<?PHP
# v25.7		20200328	EML		Ajout saut du choix des bases pour UGA
# v25.8			200411	PhD		Ajout custom_css
# v25.13	20220409	PhD		Ajout test dir_dbgalerie, correction chgt langue
#
##################################################################### XML_select ###
function XML_select ($loop, $attr, $Xaction) {
# boucle sur l'affichage des bases de données.
# Le compteur de boucle va servir de numéro de base (à partir de 0)
	global $Xvars;
	if ($loop === null) return;		// tag final : </item>

	$Bases = $Xvars['Bases'];
	$Xvars['nompublic'] = $Bases[$loop]['nompublic'];
	$Xvars['k'] = $loop;															// Numéro de la base

	if ($loop+1 < count($Bases)) return  'ACT,LOOP';  // Affichage et bouclage
	else return 'ACT' ;																// Affichage dernier item
} 


#####################################################################     
# Initialisations

require_once ('globalvars.php');	// Charger les variables globales
require_once ('globalfcts.php');	// Charger les sous-programmes communs
require_once ('xml_inc.php');					// Moteur XML, utilisé partout
require_once ('xml_inc_html5.php');		// extension pour code HTML5 correct (non XHTML)
global 	$dblink,									// Lien base de données ouverte
				$Xvars;										// Déclaré global pour tous les traitements XML

#### Ouverture de session et  OUVERTURE DE LA PAGE HTML ###
  session_start ();
  $local_titre = isset ($_SESSION['db']) ? $Bases[$_SESSION['db']]['nompublic'] : '';		// Pour titre html									
	$custom_css = "index.css";
	require_once ('inc_tete.php');

# Traiter un éventuel retour sur index.php pour clic sur changement de langue
  if (@$_GET['langue']) {
		if (in_array($_GET['langue'], $langues)) {		// langues : variable globale
			$_SESSION['langue'] = $_GET['langue'];
		} else DIE ("*** Paramètre 'langue' faux ! ***");
  } 


# Ensuite on affiche un formulaire de choix de la base (si multiple) 
# et d'identification de l'utilisateur

	// Préparation de l'espace de données à afficher	
	$Xvars['Bases'] = $Bases;														// Tableau des bases pris dans config
	$Xvars['dba_retour'] = $dba_retour;									// URL retour site web
	$Xvars['DBAconit'] = $DBAconit;											// Indicateur de version DBAconit
	$Xvars['dblink'] = $dblink;													// Lien base de données après connexion
	
	if (isset ($dir_dbgalerie)) $Xvars['dir_dbgalerie'] = $dir_dbgalerie;		// Galerie virtuelle (si elle existe...)
	
	// Fixer la langue
  if (!@$_SESSION['langue'])  $_SESSION['langue'] = 'FR';	// FR langue par défaut
	// Sélection du basculement de langues, solution pour 2 langues
	$n = array_search($_SESSION['langue'], $langues);
	$Xvars['newlang'] = $langues[!$n];
	
	if(!empty($skip_db_choices))				// Possibilité de sauter le premier écran (UGA ?) Fixé dans dblogin.php
	{
		header('location: identification.php'); // Ne pas afficher la page de choix de base de données
		exit();
	}
	
	$liste_xml = Xopen ('./XML_modeles/index.xml') ;
	Xpose ($liste_xml);

# Le retour du formulaire est orienté vers le module "identification".
# En tête de ce module, l'appel à "init.inc" pourra cette fois-ci faire une initilaisation complète :
# connexion base, lecture du profil de l'utilisateur, gestion des langues

?>