<?php
###
# v21.2		180213	PhD		Ajouté une validation générale du mode récolement
# v23.4		181218	PhD		Changé les noms des compteurs
# v24			190225	PhD		Corrigé traitement maint_recol
# v25			190707	PhD		importexport devient sauv_sql, ajout traitement paramètre mode_recolement
# v25.3		191101	PhD		Ajouté stats sur objets et iconographie
# v25.4		191204	PhD		Si mode récolement désactivé, effacer les variables session associées
# v25.5		191228	PhD		Traité parametres mode_recolement et bloquer_desherbage
# v25.7		200328	EML		Ajout pour custom style UGA
# v25.8		200411	PhD		Reprise custom style
# v25.10	210130	PhD		Remplacé SQL PASSWORD par PHP Password_sha1
# v25.14	220717	PhD		Corrigé args htmlentities, explode et pregsplit (PHP 8.1)
####

/* Protection des entrées : ----------------------------------------------------
"action"	- POST non accessible - Comparé à valeurs connues
"login"		* POST - Filtré
"passe"		* POST hidden - uniquement codé et comparé, 
----------------------------------------------------------------------------- */
	$custom_css="identification.css";
	require_once ('init.inc.php');

#### Traitement des entrées
	
	$action = @$_POST['action'];					// provient soit de "index", soit d'ici "identification"
	$login =  (isset ($_POST['login'])) ? htmlentities ($_POST['login'], ENT_QUOTES, 'UTF-8') : '';
  $passe =  @$_POST['passe'];

#### Mode VISITEUR #########################################
		if ($action == 'visiteur') {				// en provenance d'index, pas de traitement
			$_SESSION['statut'] = 'visiteur';
			Compter ('nbr_acces_visiteurs');
			// Créer le tableau  droits d'accès élémentaires à partir du profil "statut"
			$_SESSION['droits'] = $droits = $tab_droits [$_SESSION['statut']] ; 
			
			// Fixer les paramètres d'affichage à partir du profil par défaut)
			Profil ('R');
			
			// Se brancher sur l'écran "Rechercher"			  
			$_SERVER['PHP_SELF'] = 'rechercher.php';
			include ($_SERVER['PHP_SELF']);
			exit;			// >>>>>>>>>>>>>>>>>> Branchement vers "Rechercher"
		}

#### Mode IDENTIFIÉ ########################################
   	if ($action == 'identification') {		// retour du formulaire "identification"

			 // Vérification du login et du mot de passe
			if (!$login) erreurMsg ("Vous n'avez pas entré votre login");
			else {
				$password = Password_sha1 ($passe);
				$result = requete ("SELECT * FROM Rapporteurs WHERE login='$login' AND passe = '$password'");
				$nb = mysqli_num_rows ($result);
					 
				if ($nb != 0) {
					$ligne = mysqli_fetch_assoc ($result);
					$_SERVER['QUERY_STRING'] = '';
					$_SESSION['idrapporteur'] = $ligne['idrapporteur'];
					$_SESSION['statut'] = $ligne['statut']; 
					$_SESSION['rmel'] = $ligne['rmel']; 
					$_SESSION['rapporteur'] = $ligne['rprenom'] . ' ' . $ligne['rnom'];
					$_SESSION['droitlocal'] = ($ligne['droitlocal'] !== NULL) ? explode (',', $ligne['droitlocal']) : '';				
			
					Compter ('nbr_acces_inscrits');

					// Créer le tableau  droits d'accès élémentaires à partir du profil "statut"
					// (init.inc ne connaissait pas encore le résultat de l'identification) 
					$droits = $tab_droits [$_SESSION['statut']];

					// neutraliser le droit récolement si le mode n'est pas activé dans les parametres
					if (@!$dbase['mode_recolement']) 
						if ($key = array_search ('recol_aff', $droits)) unset ($droits[$key]);
		
					// neutraliser le droit désherbage si paramétre demande blocage (si l'utilisateur n'est pas senior_admin)
					if (($_SESSION['statut'] != 'senior_admin') AND @$dbase['bloquer_desherbage'])
						if ($key = array_search ('recol_desh', $droits)) unset ($droits[$key]);

					// ajouter les droits personnels (preg_split préféré à explode pour éliminer les espaces superflus...)
					if ($ligne['droitperso'] === NULL) $ligne['droitperso'] = '';
					$droits = array_merge ($droits, preg_split("/[\s,]+/", $ligne['droitperso']));
					$_SESSION['droits'] = $droits;
					
					// Lancer le processus "sauvegarde automatique" pour les rédacteurs et administrateurs
					if ($dba_sauv_auto && in_array ('sauv_auto', $droits)) require_once ('sauv_sql_auto.php');

					Profil ('R');				// Fixer les paramètres d'affichage à partir du profil enregistré				
					Deverrouiller ();		// Lever un éventuel verrou modifications résiduel
				
					// Si mode récolement désactivé, effacer les variables session associées 
					if (@!$dbase['mode_recolement']) {
							Pb_set (P_RECOL, 0);
							Pb_set (P_SELECT, 0);
						}
					// Et se brancher sur l'écran "Rechercher"			  
					$_SERVER['PHP_SELF'] = 'rechercher.php';
					include ($_SERVER['PHP_SELF']);
					exit;			// >>>>>>>>>>>>>>>>>> Branchement vers "Rechercher"
				}

				erreurMsg ("Vous avez fait une erreur dans votre login/passe.");
			}
    }

    if (($erreur == 0) && ($action == 'indice')) {
       $result = requete ("SELECT indice FROM Rapporteurs WHERE login='$login'");
       $nb = mysqli_num_rows ($result);
       $ligne = mysqli_fetch_assoc ($result);
       $indice = ($nb == 0) ? Tr ('(non disponible)', '(no available)') : $ligne['indice'];
   }


############################################################# Statistiques ###

	$result = requete ("SELECT MAX(datemod) FROM Collections");
	$ligne = mysqli_fetch_array($result);
	$stat['maj'] = $ligne[0];
	
	$result = requete ("SELECT idmachine FROM  Machines");
	$stat['nbmac'] = mysqli_num_rows($result);

	$result = requete ("SELECT idcollection FROM  Collections WHERE type='objet'");
	$stat['nbobj'] = mysqli_num_rows($result);

	$result = requete ("SELECT iddocument FROM  Documents");
	$stat['nbdoc'] = mysqli_num_rows($result);
		
	$result = requete ("SELECT idcollection FROM  Collections WHERE type='iconographie'");
	$stat['nbicon'] = mysqli_num_rows($result);

	$result = requete ("SELECT idlogiciel FROM  Logiciels");
	$stat['nblog'] = mysqli_num_rows($result);
		
	$result = requete ("SELECT idmedia FROM  Medias");
	$stat['nbpho'] = mysqli_num_rows($result);


#--------------------------------#
# Affichage
#--------------------------------#
	$Xvars['DBAconit'] = $DBAconit;											// Indicateur de version DBAconit
	$Xvars['dba_retour'] = $dba_retour;									// URL retour site web
	$Xvars ['dbase'] = $dbase;
	$Xvars ['erreur'] = $erreur;
	$Xvars ['indice'] = @$indice;	
	$Xvars ['login'] = $login;
 	$Xvars ['msg'] = $dbase[$_SESSION['langue'].'_msg'];
	$Xvars ['stat'] = $stat;
	
	// Sélection du basculement de langues, solution pour 2 langues
	$n = array_search($_SESSION['langue'], $langues);
	$Xvars['newlang'] = $langues[!$n];
	
	$liste_xml = Xopen ('./XML_modeles/identification.xml') ;
	Xpose ($liste_xml);
	$Xvars = array ();						// Purger la table des variables

	Fin ();
?>

