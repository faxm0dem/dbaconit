<?php 
# v25				190708	PhD		Ajouté des entrées, corrigé l_cols, préparé comptage
# v25.1			190717	PhD		Supprimé f_aff_galevirt
# v25.3			191030	PhD		Complété set _galevirt
# v25.4			191205	PhD		Traitement des appels récolement, désherbage et sélection
#													Erreur si pas de requête enregistrée 
# v25.5			191224	PhD		Revu test récolement
# v25.8			200411	PhD		Ajout custom_css
###
#		Ce module est appelé en retour par les 3 écrans d'affichage de résultats de recherche :
# 			'rech_resultat.xml', 'rech_resultat_recol', 'rech_resultat_desh'
#
# Suivant l'état des indicateurs de mode 'Placement' (P_RECOL) et 'Désherbage' (P_SELECT), 
# il appelle les écrans corespondants : 'rech_resultat.xml', 'rech_resultat_recol' ou 'rech_resultat_desh'
#
#	En mode standard, il permet de modifier les drapeaux des colonnes et de rappeler l'afichage standard
#
# Il ne traite pas les appels spécifiques des écrans récolement et déherbage (voir 'rappel_rech_recol.php')
######

/* Protection des entrées -------------------------------------------------------
cols[]			- POST 	- comparé par valeur 
mode				- POST - uniquement testé switch
------------------------------------------------------------------------------ */

$custom_css = "rappel_rech.css";
require_once ('global.js.php');  // avant init
require_once ('init.inc.php');
require_once ('rechercher.inc.php');
require_once ('consulter.inc.php');
if (@$dbase['mode_recolement']) require_once ('aff_recol.inc.php');

## Traitement des entrées :
###########################
$l_cols = (isset ($_POST['cols'])) ? $_POST['cols'] : array ();
$mode = @$_POST['mode'];

#################################################################################
	
	Debut ();
	
###
	$req_rechercher = @$_SESSION['req_rechercher'];
	
	// Si pas de requête enregistrée, revenir à l'écran rRechercher
	if (!$req_rechercher) {
		erreurMsg (Tr ("Pas de requête enregistrée", 'No recorded search command'));
		// Et se brancher sur l'écran "Rechercher"			  
		$_SERVER['PHP_SELF'] = 'rechercher.php';
		include ($_SERVER['PHP_SELF']);
		exit;			// >>>>>>>>>>>>>>>>>> Branchement vers "Rechercher"

	}
	// sinon réexecuter la recherche
	$SQLresult = requete ($req_rechercher);

	// Préparer le comptage machines/documents/logiciels/objets/icongraphie
	$Xvars['compt_c'] = $Xvars['compt_d'] = $Xvars['compt_l']= $Xvars['compt_o']= $Xvars['compt_i'] = 0;

	// Charger le résultat de la recherche
	$Xvars['SQLresult'] = $SQLresult;
	$Xvars['db'] =$db;					// Pour pouvoir ajouter le nr de la base dans l'URL

						// Afficher le code de la recherche
						$Xvars['textreq'] = $_SESSION['reaffichage'][0];
						$Xvars['codereq'] = $_SESSION['reaffichage'][1];
						$Xvars['clausereq'] = $_SESSION['reaffichage'][2];
						$Xvars['typs']		= $_SESSION['reaffichage'][3];
						
### Aiguillage suivant le bouton utilisé
	switch ($mode) {
	
		case 'std' : 		Pb_set (P_RECOL,0) ; Pb_set (P_SELECT, 0); break;
		case 'place' : 	Pb_set (P_RECOL,1) ; Pb_set (P_SELECT, 0); break;
		case 'desherb': Pb_set (P_RECOL,0) ; Pb_set (P_SELECT, 1); break;
		case 'select': Pb_set (P_RECOL,1) ; Pb_set (P_SELECT, 1); break;
		
		case 're_aff' :
		// Les drapeaux de mode ne changent pas,
		// mais il faut traiter les drapeaux des colonnes
			Pb_set (P_AFF_GALEVIRT, in_array ('museevirtuel', $l_cols));
			Pb_set (P_AFF_AUTRNO, in_array ('autrno', $l_cols));
			Pb_set (P_AFF_DESIGN, in_array ('design', $l_cols));
			Pb_set (P_AFF_MEDIAS, in_array ('medias', $l_cols));
			Pb_set (P_AFF_VIGNETTE, in_array ('vignette', $l_cols));
			Pb_set (P_AFF_DIMENSIONS, in_array ('dimensions', $l_cols));
			Pb_set (P_AFF_FONCTION, in_array ('fonction', $l_cols));
			Pb_set (P_AFF_ETAT, in_array ('etat', $l_cols));
			Pb_set (P_AFF_DONATEUR, in_array ('donateur', $l_cols));
			Pb_set (P_AFF_BATIMENT, in_array ('batiment', $l_cols));
			Pb_set (P_AFF_COTERANGE, in_array ('coterange', $l_cols));
			Pb_set (P_AFF_LIEURANGE, in_array ('lieurange', $l_cols));
			Pb_set (P_AFF_DATE_VALID, in_array ('datevalid', $l_cols));
			Pb_set (P_AFF_DATEMOD, in_array ('datemod', $l_cols));
			Pb_set (P_AFF_CORCOL, in_array ('corcol', $l_cols));
			Pb_set (P_AFF_RESPINV, in_array ('respinv', $l_cols));
			Pb_set (P_AFF_ETATFICH, in_array ('etatfich', $l_cols));
	}

### et réaffichage de la table localisation/récolement, dans le bon mode
	$Xvars['droits'] = $droits;
	Aiguil_rech_resultat ();
	
	Fin ();
?>
