<?php
# v15				160222	PhD		Création pour affichage par moteur Xpose. 
# v25				190615	PhD		Supprimé inst debug 8
###
	
########################################################################## debug ###
function debug ($code, $msg, $var="", $mix="") {
// Affiche un message et (éventuellement) le contenu d'une variable 
// si un bit positionné de la  variable $debug correspond à un bit de $code.
// Traite les variables simples et les tableaux.
	global $debug;
	if ($debug & $code) {
		echo "<pre>[[[".$msg ;
		if ($var) {
			echo " : ";
			if (is_array ($var) || is_numeric ($var) || empty ($mix) ) {			
				print_r ($var); 
			} else  {
				for ($i=0; $i<strlen($var); $i+=1) {
					echo ".".substr ($var, $i, 1);
				}
			}			
		} 
		echo "]]]</pre>";
	}
	return;
}

          
##################################################################### XML_resume_err ###
function XML_resume_err ($loop, $attr, $Xaction) {
# Boucle sur les objets     
	global $Xvars;

	if ($loop === null) return;		// tag de fin	

	if ($loop <count ($Xvars['erreurs'])) {
		$Xvars['ligne'] = $Xvars['erreurs'][$loop];
	 		return 'ACT,LOOP';
		    
	} else return  'EXIT' ;
}
	
##################################################################### XML_resume_ref ###
function XML_resume_ref ($loop, $attr, $Xaction) {
# Boucle sur les objets     
	global $Xvars, $dblink, $kfiche;

	if ($loop === null) return;		// tag de fin	

	if ($loop <count ($Xvars['refus'])) {
			$Xvars['ligne'] = $v = $Xvars['refus'][$loop];
		$req = "UPDATE Collections 
						SET dateTransfert='".$Xvars['date_tr']."', idetatfiche='".$kfiche['refus'].
						"' WHERE idcollection='$v[0]';";
		if (mysqli_query ($dblink, $req)) {
	 		return 'ACT,LOOP';
	 	
	 	} else {
	 		$Xvars['erreurs'] =array($v[0], $v[1], $v[2], 'Marquage refusé impossible : '. mysqli_error ($dblink));
	 		return 'LOOP';
	 	}
		    
	} else return  'EXIT' ;
}
	
##################################################################### XML_resume_val ###
function XML_resume_val ($loop, $attr, $Xaction) {
# Boucle sur les objets     
	global $Xvars, $dblink, $kfiche;

	if ($loop === null) return;		// tag de fin	

	if ($loop <count ($Xvars['valide'])) {
			$Xvars['ligne'] = $v = $Xvars['valide'][$loop];
		$req = "UPDATE Collections 
						SET dateTransfert=".$Xvars['date_tr'].", idetatfiche=".$kfiche['transfert'].
						" WHERE idcollection='$v[0]';";
		if (mysqli_query ($dblink, $req)) {
	 		return 'ACT,LOOP';
	 	
	 	} else {
	 		$Xvars['erreurs'] =array($v[0], $v[1], $v[2], 'Marquage transféré impossible : '. mysqli_error ($dblink));
	 		return 'LOOP';
	 	}
   
	} else return  'EXIT' ;
}
	
##################################################################### XML_result ###
function XML_result ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>
	
	global $Xvars;

	if ($loop === 0) 	$Xvars['index'] = 0;

	$index = $Xvars['index'];
	while ($index < count ($Xvars['tab_files'])) {
		$file = $Xvars['tab_files'][$index];
		$Xvars['index'] = $index +=1;

		// on sélectionne les fichiers HTM qui commencent par 'R_'
		if ((substr($file, -4) == '.htm') AND (substr($file, 0, 2) == 'R_')) {
			$Xvars['file']= $file;
			$Xvars['date'] = date("d F Y", filectime ($Xvars['dir_export'].$file));
			$Xvars['name']= substr (substr ($file,0,-4), 2);
			return "ACT,LOOP";
		}
	}
	
	return 'EXIT';
}	

###################################################################### XML_valid ###
function XML_valid ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>
	
	global $Xvars;

	if ($loop === 0) 	$Xvars['index'] = 0;

	$index = $Xvars['index'];
	while ($index < count ($Xvars['tab_files'])) {
		$file = $Xvars['tab_files'][$index];
		$Xvars['index'] = $index +=1;

		// on sélectionne les fichiers HTM qui ne commencent pas par 'R_'
		// et pour lesquels existe un fichier ZIP de même nom (donc sans 'T_')
		if ((substr($file, -4) == '.htm') AND (substr($file, 0, 2) != 'R_')) {
			$name = substr ($file,0,-4);
			if (file_exists ($Xvars['dir_export'].$name.'.zip')) {
				$Xvars['file']= $file;
				$Xvars['date'] = date("d F Y", filectime ($Xvars['dir_export'].$file));
				$Xvars['name']= $name;
				return "ACT,LOOP";
			}
		}
	}
	
	return 'EXIT';
}	

###################################################################### XML_zip ###
function XML_zip ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>
	
	global $Xvars;

	if ($loop === 0) 	$Xvars['index'] = 0;

	$index = $Xvars['index'];
	while ($index < count ($Xvars['tab_files'])) {
		$file = $Xvars['tab_files'][$index];
		$Xvars['index'] = $index +=1;

		// on sélectionne les fichiers ZIP qui commencent par 'T_'
		if ((substr($file, -4) == '.zip') AND (substr($file, 0, 2) == 'T_')) {
			$Xvars['date'] = date("d F Y", filectime ($Xvars['dir_export'].$file));
			$Xvars['file']= $file;
			$Xvars['name']= substr (substr ($file,0,-4), 2);
			return "ACT,LOOP";
		}
	}
	
	return 'EXIT';
}	

#####################################################################
?>