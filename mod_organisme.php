<?php
# v22			180425	PhD		Traitement de 'osigle', 'f_secable'
# v23			180818	PhD		Ajout champ 'onotes' en création
# v23.1		180901	PhD		Retouche commentaires
# v24			190225	PhD		Reprise du traitement des organismes existants à attacher
# v24.1		190318	PhD		Suppression initialisation nrinv, non utilisé
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST non accessible - uniquement testé switch
'osigle'				- POST  - Filtré NormIN
'f_secable'			- POST  - Testé isset
'onom'					- POST  - Filtré NormIN
'onotes'				- POST  - Filtré NormIN
'idorganisme'		- POST  - filtré numérique
'idcollection'	* REQ  transmis par URL (suivant/précédent) - filtré numérique
'lettres'				- POST  - Filtré NormIN
'menu'					* REQ  transmis par URL (suivant/précédent) - uniquement testé switch
'relations[]'		- POST
'supprs[]'			- POST 
------------------------------------------------------------------------------ */

require_once ('init.inc.php');
require_once ('consulter.inc.php');
require_once ('mod_org_per.inc.php');

## Traitement des entrées :
###########################
$action = @$_POST['action'];
$osigle = NormIN ('osigle');
$f_secable = isset ($_REQUEST['f_secable']);
$onom = NormIN ('onom');
$onotes = NormIN ('onotes');
	
$idcollection = @$_REQUEST['idcollection'];
if (!is_numeric($idcollection)) 	DIE ("*** Paramètre 'idcollection' faux ! ***"); 
$idorganisme = @$_REQUEST['idorganisme'];

$lettres = NormIN ('lettres');
$menu = @$_REQUEST['menu'];
$relations = @$_POST['relations'];
$supprs = @$_POST['supprs'];


# Initialisations ##############################

// Debut () et MenuConsulter () sont déjà traités dans "consulter.php"

$mode = "liste";			// Valeur par défaut pour affichage simple de la liste
$Xvars = array();
$Xvars['cle'] = 'cleorganisme';														// Traitement organismes											
$Xvars['nbrrelations'] = Nbrlignes ('Relations');					// pour dimensionner le Select

# Vider les éléments préslectionnés
$Xvars['psel_sigle'] = '';
$Xvars['psel_secable'] = FALSE;
$Xvars['psel_nom'] = '';


# EXECUTION pour modification
#############################

// Vérification de l'identité
if (!in_array ("mod_objet", $droits)) {
	 erreurMsg ("Vous ne vous êtes pas identifié..."); 
	 include ('identification.php');
	 exit;
 }
 
# Aiguillage suivant action demandée ############################################
 switch ($action) {

	### Modifications de la table des liens ########################################################
	######################### Aiguillage selon le bouton choisi à l'intérieur du bloc "Modifier" ###

	#================================================================================== Ajouter ===
	# Afficher le formulaire d'ajout
	case 'ajouter' :
		$mode = 'ajout';	
		break;
		
	#================================================================================== Supprimer ===
	case 'supprimer' :
	   case 'supprimer' :
    // Vérification
		if (!$supprs)  {
			erreurMsg ("Vous n'avez sélectionné aucun nom");
			break;
		}
		
    $sc = 0;	 
		foreach ($supprs as $idents) {
	 		$t_ident = explode ('|', $idents);
	    if (requete ("DELETE FROM Col_Org WHERE idcollection=$idcollection AND idrelation=".$t_ident[0]." AND idorganisme=".$t_ident[1])) {
        Message ("- Le lien entre le nom %0 et l'élément %1 a été supprimé - ", $t_ident[2], $idcollection);
	      $sc++;
	    }
		}  
		
	    if ($sc) miseaJour ($idcollection);
      break;

	#================================================================================== Enregistrer ===
  case 'enregistrer' :
   		
    // Vérification des erreurs
		if (empty ($osigle) && empty ($onom)) {
			erreurMsg ("Vous n'avez indiqué ni nouveau sigle, ni nouveau nom");
			break;
		}
		
		if (!isset ($relations)  || $relations == 0) {
			erreurMsg ("Vous n'avez pas indiqué la nouvelle relation");
			break;
		}
		
	  // Si l'auteur est déjà dans la table, noter idorganisme sinon l'ajouter
		$SQLosigle = NormSQL ($osigle);		
		$SQLonom = NormSQL ($onom);		
		$SQLonotes = NormSQL ($onotes);		
		$SQLresult = requete ( "SELECT idorganisme FROM Organismes WHERE (osigle ='$SQLosigle') AND (onom ='$SQLonom')");

		if (0 == mysqli_num_rows ($SQLresult)) { 
			if (requete ("INSERT INTO Organismes VALUES (NULL, '$SQLosigle', '$f_secable', '$SQLonom', '$SQLonotes')")) {
				Message ("Le nom « %0 %1» a été ajouté", $osigle, $onom);
				$idorganisme = mysqli_insert_id ($dblink);
			} else {
			erreurMsg ("Impossible d'enregistrer le nouveau nom");
			break;
			}
    } else {
        $ligne = mysqli_fetch_assoc ($SQLresult);
   	 		$idorganisme = $ligne['idorganisme'];
 				Message ("Le nom « %0 %1» existe déjà, il est attaché sans modifications", $osigle, $onom);
   }
      
    // et on peut enchainer sur le lien entre collection et organisme
    Attacher_organisme ($idcollection, $idorganisme, $relations);
			 
	break;


#=================================================================================== Inconnu ===
# Préselectionner le nom (inconnu), afficher le formulaire d'ajout
	case 'inconnu' :
		$Xvars['psel_nom'] = '(inconnu)';
		$mode = "ajout";
		break;
		
#================================================================================== Utiliser ===
# Afficher le formulaire demandant les premiers caractères recherchés
	case 'utiliser' :
		$mode = "rechercher";
		break;
		
#================================================================================== Rechercher ===
# Lancer la recherche à partir des caractères demandés, 
# Puis afficher le formulaire de sélection d'un nom dans la liste
	case 'rechercher' :
		// L'appel à la base est fait ici pour récupérer le nombre de lignes du select
		$result_nom = requete ("SELECT idorganisme, osigle, onom FROM Organismes 
			WHERE (osigle LIKE '$lettres%') OR (onom LIKE '$lettres%') ORDER BY osigle, onom");
		$Xvars['result_nom'] = $result_nom;
		$Xvars['nb_noms'] = mysqli_num_rows ($result_nom);		// L'appel à la base est fait ici pour récupérer le nombre
		$mode = "selecter";
		break;
		
#================================================================================== Preselect ===
# Un nom a été pré-sélectionné dans le bloc, afficher le formulaire réduit d'ajout d'un organisme existant
# avec uniquement affichage sigle+nom et choix de la relation. 
	case 'preselect' :
		$t = explode ("|", $onom);
		$Xvars['psel_sigle'] = $t[0];
		$Xvars['psel_onom'] = $t[1];
		$Xvars['psel_idorganisme'] = $t[2];
		$mode = "attacher";
		break;
		
#================================================================================== Attacher ===
# Attacher l'organisme existant, avec la relation demandée
	case 'attacher' :
		if (!isset ($relations)  || $relations == 0) {
			erreurMsg ("Vous n'avez pas indiqué la nouvelle relation");
		} else {
			Attacher_organisme ($idcollection, $idorganisme, $relations);
		}	
 		$mode = "liste";
		break;
		
#================================================================================== Annuler ===
# Le cas "Annuler"  ne demande aucun traitement spécifique.
 	case 'annuler' :
 		$mode = "liste";
 		break;
 		
 	}	// end_switch

	   
 
########################################################################################################################
# Affichage écran principal et sur demande "Ajouter organisme", "Rechercher organisme existant" 
########################################################################################################################

 	$Xvars['idcollection'] = $idcollection;
 	$Xvars['menu'] = $menu;
 	$Xvars['mode'] = $mode;

	// Afficher à partir du modèle XML	
	$liste_xml = Xopen ('./XML_modeles/mod_organisme.xml') ;
	Xpose ($liste_xml);

  Fin ();
?>  