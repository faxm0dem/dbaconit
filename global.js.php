<?php
# v16.2			161212	PhD		Réorganisé les fonctions javascript
#
################################################################################## 
# Fonctions Javascript chargées par init.inc - Attention à l'ordre des Require ###
# 
$jscript = "
//-------------------------------------------------------------
	function valid_sup () {
		var langue, nb_liens, result=false;
		langue = document.consult.langue.value;
		nb_liens = document.consult.nb_liens.value;
	
		if (langue == 'FR') 
			msg_confirm = 'Confirmez vous vraiment la suppression de cette fiche et de tous les liens associés ?';
		else msg_confirm = 'Do you really want to delete this file and all associated links?';
	
		if (nb_liens > 0) {
			if (langue == 'FR') 
				msg_liens = ' Bien noter que '+nb_liens+' liens avec d\'autres objets vont être supprimés';
			else msg_liens = ' Please note that '+nb_liens+' links with other objects shall be deleted';
		} else msg_liens = '';
	
		result = confirm (msg_confirm + msg_liens);
		if (!result) {
			document.consult.action.value ='menu';
			document.consult.submit('menu');
		}
	}

//-------------------------------------------------------------

	// Fonction afficher/cacher des lignes pour la liste de résultats récolement
	function afficher_cacher(id) {
		if(document.getElementById('loc'+id).style.display=='none')
		{
				document.getElementById('lig'+id).style.display='none';
				document.getElementById('loc'+id).style.display='inline-block';
		}
		else
		{
				document.getElementById('lig'+id).style.display='inline-block';
				document.getElementById('loc'+id).style.display='none';
		}
		return true;
	}
//-------------------------------------------------------------
 ";
?>
