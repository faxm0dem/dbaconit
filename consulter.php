<?php 
# v10.7			130412	PhD		Refonte pour appel des modèles XML, simplification des flags multiples...
# v12.0			140113	MSA		Remplacement Photos par Médias
# v12.1			140306	PhD		Ajout contrôle des droits de modification
# v12.5			140608	PhD		Appel de "rechercher" au lieu de "Obtenit_id" si l'idcollection n'est pas connu
# v12.5.01	140610	PhD		Correction oubli traitement idcollectionS
# v12.7			140713	PhD		Supprimé idselect de la liste des entrées (le select fournit direct idcollection)
# v12.8.02	150311	PhD		Supprimé unset $format inutile ?
# v14.1			151007	PhD		Remplacé apple mod_liens avec !modif  par appel à aff_liens
# v14.2			151214	PhD		Changé interface Debut
# v14.2.02	160105	PhD		Corrigé position appel Debut
#	v15.5			161101	PhD		Ajouté fonction valid_sup, appel SupObjet et aiguillages au retour
# v16.0			161117	PhD		Ajouté require consulter_recol.inc
# v16.2			161214	PhD		Réorganisé les .inc et les .js
# v16.3			161217	PhD		Forcé l'entrée "modification" en cas de récolement
# v16.4			170126	PhD		Remplacé session aff_complet par le profil P_AFF_COMPLET
# v16.5			170202	PhD		Remplacé session p_recol par le profil P_RECOL
# v17.1			170506	PhD		Ajouté modification type de fiche, centralisé ici l'affichage du menu objet
# v17.4			170601	PhD		Réorganisé la suppresion d'objet
# v19				170903	PhD		Ajouté Verrouiller/Déverouiller, ajouté comptages
# v19.0.02	170910	PhD		Rétabli include disparu vers Matériaux
# v19.1			170927	PhD		Suppression affichage mode modif si recol
# v22.0			180410	PhD		Affichage noms d'organismes complets, déplacé 'Compose_tl_orga'
# v23.4			181203	PhD		Changé les noms des compteurs, transféré comptage modifs de ce module vers fonction MiseaJour
# v25.5			191224	PhD		Remplacé test maint_recol par mode_recolement
# v25.8			200411	PhD		Ajout custom_css
###

/* Protection des entrées -------------------------------------------------------
'idcollection'		* REQUEST : transmis en GET par menu, appel possible Web - Filtré
'idcollections'		- POST non accessible
'menu'				* REQ  transmis par URL (suiv/précéd) - Comparé à table valeurs
'Modif'				* REQUEST : transmis en GET par menu - Uniquement testé "isset" 
'nouveau'			* REQUEST : transmis en GET par menu - Uniquement testé "isset"
'nrinv'				* REQUEST : Appel possible Web - Filtré
'Sauvegarder'		- POST non accessible -  Uniquement testé "isset"

L'ensemble des données du formulaire est normalisé par NormINPUT
------------------------------------------------------------------------------ */

$custom_css = "consulter.css";
require_once ('global.js.php');  // avant init
require_once ('init.inc.php');
require_once ('consulter.inc.php');
if (@$dbase['mode_recolement']) require_once ('aff_recol.inc.php');

## Traitement des entrées :
###########################
	$INPUT = array();
		
	$f_modif = isset ($_REQUEST['Modif']);
	#		if (Pb (P_RECOL)) $f_modif = TRUE;		// en cas de récolement, entrée directe en "modification"
	$f_nouveau = isset ($_REQUEST['nouveau']);
	$f_sauvegarder = isset ($_POST['Sauvegarder']);

	$g_menu = @$_REQUEST['menu'];
	if (isset ($g_menu) && !in_array ($g_menu, array ('A_resume', 'A_complet', 'A_liens', 'M_objet', 'M_orga', 
		'M_perso', 'M_medias', 'M_mots clés', 'M_matériaux', 'M_liens', 'M_mouvements', 'M_type', 'SUP_objet'))) 
		DIE ("*** Paramètre 'menu' faux ! ***");

   if ($f_nouveau) {
      unset ($idcollection, $_SESSION['idcollection'], $_SESSION['idcollections']);
      Deverrouiller ();		// lever le verrou Modifications
   } elseif (isset ($_POST['idcollections'])) {
	  $_SESSION['idcollections'] = array_values(array_unique ($_POST['idcollections']));
   }

	if (isset ($_REQUEST['idcollection'])) {
		// Test de validité
		if (is_numeric($_REQUEST['idcollection']) ) {
			$idcollection = $_REQUEST['idcollection'];
      Deverrouiller ();		// lever le verrou Modifications
			// si une liste existe et si l'id est en dehors, effacer la liste
			if (isset ($_SESSION['idcollections'])) 
				if (!in_array($idcollection, $_SESSION['idcollections'])) unset ($_SESSION['idcollections']);
		// Arrêt si entrée incorrecte
		} else if ($_REQUEST['idcollection']) 
			DIE ("*** Paramètre 'idcollection' faux ! ***"); 
	}

  if (isset ($_REQUEST['nrinv'])) $nrinv = htmlentities($_REQUEST['nrinv']);
   
	// Paramètre affichage des liens non orientés (famille, même lot)
	$nor = @$_GET['nor'];
  	if (isset($nor) && !is_numeric($nor)) 	DIE ("*** Paramètre 'nor' faux ! ***"); 
  	if ($nor != '') Pb_set (P_AFF_LIENS_NOR, $nor);

## Formulaire normalisé en bloc dans le tableau INPUT
# Il s'agit de toutes les valeurs modifiées dans un article de la base
	if ($f_modif && $g_menu=='M_objet') NormINPUT ();
	
#################################################################################

# Vérifier la disponibilité d'un idcollection, sinon exécuter "rechercher"
	if (empty ($idcollection) && isset ($_SESSION['idcollections'])) 
		$idcollection = $_SESSION['idcollections'][0]; 
	elseif (empty ($idcollection)) require_once ('rechercher.php'); 	//EXIT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	// Mémoriser l'idcollection pour les opérations suivantes (et identifier la table d'objets correspondantes)
	$_SESSION['idcollection'] = $idcollection;	
	$table = Section ($idcollection);
   
// Choix du menu à afficher
	if (isset ($g_menu)) $menu = $g_menu;		// menu déjà choisi
	else if ($f_modif) $menu = 'M_objet'; 	// modification demandée
	else if (Pb (P_AFF_COMPLET)) $menu = 'A_complet'; // sinon affichage dans le mode mémorisé
	else $menu = 'A_resume';
	
### Demande de sauvegarde des mofifications en cours
# on reste sur la même page 
#  
      if ($f_sauvegarder) {		
         Sauvobjet ($table, $idcollection);
      }
#
###
			// Pour assurer le changement de langue sans changer de menu	
			$_SERVER['QUERY_STRING'] = "&menu=$menu";  
			// Le procédé est pour le moins douteux ???
			
	if (substr ($menu, 0, 1) == 'M')  {					
		// si les droits de modification sont insuffisants, transformer en consultation
		if (!Autor_modif($idcollection)) $menu = 'A_resume';	
	}

### Ouverture du traitement  ############  
	Debut ();
	
# Traiter le verrouillage qui protège des modifications par autre rapporteur
	if (substr($menu, 0, 1) == 'M') {
		if (!Verrouiller ($idcollection)) $menu = 'A_complet'; // en cas de collision, se rabattre sur affichage
	}
	
# Affichage menu objet
  MenuConsulter (Design_titre ($idcollection)); 

# Composer les tables de listes de noms pour cet objet
	$tl_orga = Compose_tl_orga ($idcollection, TRUE);	// nomcomplet=TRUE
	$tl_perso = Compose_tl_perso ($idcollection);


# Aiguiller vers le traitement de l'objet
##########################################
	switch ($menu) {  
	case 'A_resume' : 
		Pb_set (P_AFF_COMPLET, 0);				// mémorisation pour les consultations suivantes
		if (in_array ('aff_complet', $droits)) Compter ('nbr_lectures_inscrits');
		else Compter ('nbr_lectures_visiteurs');
		Affobjet ('Résumé', $table, $idcollection);
  	break;

	case 'A_complet' :  
		Pb_set (P_AFF_COMPLET, 1);				// mémorisation pour les consultations suivantes
		if ($_SESSION['idrapporteur']) Compter ('nbr_lectures_inscrits');
		else Compter ('nbr_lectures_visiteurs');
		Affobjet ('Complet', $table, $idcollection);
  break;

	case 'A_liens' :
		include ('aff_liens.php');
		break;

	case 'M_objet' : 
		Modobjet ($table, $idcollection);
		break;

	case 'M_orga' :
		include ('mod_organisme.php');
		break;
  
	case 'M_perso' :
 		include ('mod_personne.php');
		break;
  
	case 'M_medias' :
		include ('mod_medias.php');
		break;
	
	case 'M_mots clés' :
		include ('mod_mots_mats.php');
		break;

	case 'M_matériaux' :
		include ('mod_mots_mats.php');
		break;

	case 'M_mouvements' :
		include ('mod_mouvts.php');
		break;

	case 'M_liens' :
		include ('mod_liens.php');
		break;
		
	case 'M_type' :
		include ('mod_type.php');
		break;

	case 'SUP_objet' :
		include ('sup_objet.php');
		break;

default : 
		DIE ('*** Erreur codage menu ***');
}

Fin ();
?>
