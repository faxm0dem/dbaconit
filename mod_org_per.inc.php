<?php
# v24			190225	PhD		Ajouté Attacher_organisme, Attacher_personne
# v25			190615	PhD		Supprimé inst debug 8
###

############################################################################################### Attacher_organisme ###
function Attacher_organisme ($idcollection, $idorganisme, $relations){
# Écrire en base le lien entre collection et organisme
# On a déjà contrôlé qu'il existe une ou plusieurs relations demandées

	// Balayer toutes les relations demandées	  
	foreach ($relations as $idrelation) {
		// Vérifier que l'on ne l'attache pas en doublon
		$result = requete ("SELECT * FROM Col_Org 
							WHERE  idorganisme = $idorganisme AND  idcollection = $idcollection AND idrelation = $idrelation");
		if (mysqli_num_rows ($result))
			erreurMsg ("Le nom %0 est déjà attaché pour la même relation", $onom);
	 
		// Enregistrer
		else {
			if (requete ("INSERT INTO Col_Org VALUES ($idcollection, $idorganisme, $idrelation)"))
				MiseaJour ($idcollection);
			else erreurMsg ("Impossible d'enregistrer");
		}
	}
}


############################################################################################### Attacher_personne ###
function Attacher_personne ($idcollection, $idpersonne, $relations){
# Écrire en base le lien entre collection et personne
# On a déjà contrôlé qu'il existe une ou plusieurs relations demandées

	// Balayer toutes les relations demandées	  
	foreach ($relations as $idrelation) {    
		// Vérifier que l'on ne l'attache pas en doublon
		$result = requete ("SELECT * FROM Col_Per 
							WHERE  idpersonne = $idpersonne AND  idcollection = $idcollection AND idrelation = $idrelation");
		if (mysqli_num_rows ($result))
			erreurMsg ("Le nom %0 est déjà attaché pour la même relation", $pnom);
	 
		// Enregistrement 
		else {
			if (requete ("INSERT INTO Col_Per VALUES ($idcollection, $idpersonne, $idrelation)"))
				MiseaJour ($idcollection);
			else erreurMsg ("Impossible d'enregistrer");
		}
	}
}

############################################################################################### XML_liste_organismes ###
function XML_liste_organismes ($loop, $attr, $Xaction) {
	global $Xvars;

	if ($loop === null) return;		// tag de fin	

	if ($loop == 0) {							// Premier appel, lire la table
			$idcollection = $Xvars['idcollection'];
			$Xvars['result_org'] = requete ("SELECT Col_Org.idorganisme, osigle, onom, Relations.idrelation, relation 
				FROM Organismes, Col_Org, Relations
				WHERE Organismes.idorganisme=Col_Org.idorganisme 
				AND Relations.idrelation=Col_Org.idrelation
				AND Col_Org.idcollection= $idcollection ORDER BY idrelation, onom");	}
	
	//  Appel de la ligne courante
	$ligne = mysqli_fetch_assoc ($Xvars['result_org']); 
	$Xvars['idorganisme'] = $ligne['idorganisme'];
	$Xvars['osigle'] = $ligne['osigle'];
	$Xvars['onom'] = $ligne['onom'];
	$Xvars['idrelation'] = $ligne['idrelation'];
	$Xvars['relation'] = $ligne['relation'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 


################################################################################################ XML_liste_personnes ###
function XML_liste_personnes ($loop, $attr, $Xaction) {
	global $Xvars;

	if ($loop === null) return;		// tag de fin	

	if ($loop == 0) {							// Premier appel, lire la table
			$idcollection = $Xvars['idcollection'];
			$Xvars['result_per'] = requete ("SELECT Col_Per.idpersonne, pcivil, pnom, pprenom, Relations.idrelation, relation
				FROM Personnes, Col_Per, Relations
				WHERE Personnes.idpersonne=Col_Per.idpersonne AND Relations.idrelation=Col_Per.idrelation
				AND Col_Per.idcollection= $idcollection ORDER BY idrelation, pnom");	}
	
	//  Appel de la ligne courante
	$ligne = mysqli_fetch_assoc ($Xvars['result_per']); 
	$Xvars['idpersonne'] = $ligne['idpersonne'];
	$Xvars['pcivil'] = $ligne['pcivil'];
	$Xvars['pprenom'] = $ligne['pprenom'];
	$Xvars['pnom'] = $ligne['pnom'];
	$Xvars['idrelation'] = $ligne['idrelation'];
	$Xvars['relation'] = $ligne['relation'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 


#################################################################################################### XML_liste_radio ###
function XML_liste_radio ($loop, $attr, $Xaction) {
# Une génération directe par echo pour éviter de multiples lignes en format xml
# ou une (élégante) boucle appelant 7 fois cette fonction 
	global $Xvars, $T_civil;

	if ($loop === null) return;		// tag de fin	

	foreach ($T_civil as $cv) {
		$checked = ($cv == $Xvars['psel_civil']) ? "checked='TRUE'" : "";
		echo "<input name='pcivil' value='$cv' type='radio' $checked /><span>$cv&nbsp;&nbsp;&nbsp;</span>";
	}

	return 'EXIT' ;
} 


################################################################################################ XML_select_relation ###
function XML_select_relation ($loop, $attr, $Xaction) {
	global $Xvars;

	if ($loop === null) return;		// tag de fin	

	if ($loop == 0) {							// Premier appel, lire la table
		$cle = $Xvars['cle'];
		$Xvars['result_rel'] = requete ("SELECT * FROM Relations WHERE $cle='oui'");
	}

//  Appel de la ligne courante
	$ligne = mysqli_fetch_assoc ($Xvars['result_rel']); 
	$Xvars['idrelation'] = $ligne['idrelation'];
	if ($_SESSION['langue']=='EN' AND $ligne['relation']!='') $Xvars['relation'] = $ligne['relation_en'];
	else 	$Xvars['relation'] = $ligne['relation'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 


#################################################################################################### XML_select_onom ###
function XML_select_onom ($loop, $attr, $Xaction) {
	global $Xvars;

	if ($loop === null) return;		// tag de fin	

// Premier appel : la table est déjà lue dans le prog principal pour récupérer le nb de lignes pour le select

//  Appel de la ligne courante
	$ligne = mysqli_fetch_assoc ($Xvars['result_nom']); 
	$Xvars['idorganisme'] = $ligne['idorganisme'];
	$Xvars['osigle'] = $ligne['osigle'];
	$Xvars['onom'] = $ligne['onom'];
		
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 


#################################################################################################### XML_select_pnom ###
function XML_select_pnom ($loop, $attr, $Xaction) {
	global $Xvars;

	if ($loop === null) return;		// tag de fin	

// Premier appel : la table est déjà lue dans le prog principal pour récupérer le nb de lignes pour le select

//  Appel de la ligne courante
	$ligne = mysqli_fetch_assoc ($Xvars['result_nom']); 
	$Xvars['idpersonne'] = $ligne['idpersonne'];
	$Xvars['pcivil'] = $ligne['pcivil'];
	$Xvars['pnom'] = $ligne['pnom'];
	$Xvars['pprenom'] = $ligne['pprenom'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

?>