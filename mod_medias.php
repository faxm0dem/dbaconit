<?php
# v12.0			140304	MSA		Refonte de mod_photos pour le nouveau traitement des Médias
# v12.2			140308	PhD		Remplacement AdPhoto par AdMedia
# v12.2.01	140311	PhD		Passer les extensions en minuscule
# v13.0			150707	PhD		Introduction Mysqli
# v13.1			150717	PhD		Nouvel interface Xpose
# v14.2			151211	PhD		Correction idcollection
# v15				160111	PhD		Ajout param Xvars dans interface fonctions XML_
# v17.1			170502	PhD		Renvoyé MenuConsulter dans 'consulter'
# v18				170801	PhD		Réécriture, ajax limité à l'affichage vignettes, introduction 'ordre_media',
#														ajouté test dans 'Ecrire_col_med'
# v23.1.02	180906	PhD		Ajouté Verif_mediacle manquant dans supprimer
# v25.9			200721	PhD		Ajout du type IMG
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
'idcollection'	* REQ  - transmis par URL (suivant/précédent) - filtré numérique
'idmedia'				- POST - filtré numérique
'descrimedia'		- POST - filtré NormIN 
'lieumedia' 		- POST - filtré NormIN
'menu'					* REQ	 - transmis par URL (suivant/précédent) 	// ??? Controle
'selectm[]'			- POST																					// ???

L'ensemble des données des formulaires est normalisé par NormINPUT

------------------------------------------------------------------------------ */

require_once ('init.inc.php');
require_once ('consulter.inc.php');
require_once ('mod_medias.inc.php');

## Traitement des entrées :
###########################
	$action = @$_POST['action'];

	$idcollection = @$_REQUEST['idcollection'];	
	if (!is_numeric($idcollection)) 	DIE ("*** Paramètre 'idcollection' faux ! ***"); 

	$idmedia = @$_POST['idmedia'];				// Formulaire Attacher
	if ($idmedia AND !is_numeric($idmedia)) 	DIE ("*** Paramètre 'idmedia' faux ! ***"); 
		
	$descrimedia = NormIN ('descri');			// Formulaire Ajouter
	$lieumedia = NormIN ('lieu');					//   "           "
		
	$selectm = @$_POST['selectm'];
			
	if ($action == 'modifier2')  NormINPUT ();				// Normalisation en bloc formulaire Modifier
	
	
# Initialisations ##############################

	// Vérification de l'identité (des fois que...)
	if (!in_array ("mod_objet", $droits)) {
		 erreurMsg ("Vous ne vous êtes pas identifié...");
		 include ('identification.php');
		 exit;
	}
	
	$t_extensions = array ("jpg", "png", "pdf", "mp3", "mp4", "mov", "img");
	
	$mode = "principal";
	
// Debut () et MenuConsulter () sont déjà traités dans "consulter.php"

# EXECUTION pour modification
#############################

if ($action) {

### Traitement de l'action demandée
	switch ($action) {

	### Actions à partir des 4 boutons associés à la liste des médias ##############################
	#================================================================================== Attacher ===	
		case 'attacher1' :
			$mode = 'attacher';						// Changer le mode d'affichage et ré-afficher
			break;

	#================================================================================== Modifier ===
		case 'modifier1' :	
			// Vérification 
			if (isset ($selectm)) {
				$idmedia = $selectm[0];			// Prendre le premier élément si sélection multiple
			} else {
				erreurMsg ("Vous n'avez sélectionné aucun media"); 
				break;
			}
			// Appel des champs médias
			$result = requete ("SELECT * FROM Medias WHERE idmedia='$idmedia'");
			$Xvars['ligne'] = mysqli_fetch_assoc ($result);
			
			// et appel du numéro d'ordre
			$result_ordre = requete ("SELECT ordre_media FROM Col_Med 
																WHERE idcollection='$idcollection' AND idmedia='$idmedia'");
			$ligne = mysqli_fetch_assoc ($result_ordre);
			$Xvars['ordre_media'] = $ligne['ordre_media'];	
		
			$mode = 'modifier';						// Changer le mode d'affichage et ré-afficher
			break;
		
	#================================================================================= Supprimer ===
		case 'supprimer' :
	
		if (isset ($selectm)) {
			foreach ($selectm as $lvalue) {
				$t = explode(',',$lvalue);
				$idmedia = $t[0];
				$result = requete ("DELETE FROM Col_Med WHERE idcollection='$idcollection' AND idmedia='$idmedia'");
				if ($result) Message ("- Le lien entre le média %0 et l'élément %1 a été supprimé - ", $idmedia, $idcollection);
			}
			// Vérifier qu'un média est toujours "mediacle"
			Verif_mediacle ($idcollection);
			miseaJour ($idcollection);
			
		} else erreurMsg ("Vous n'avez sélectionné aucun média");
		break;
	 
	#=============================================================================== Télécharger ===
		case 'télécharger1' :
			$mode = 'télécharger';						// Changer le mode d'affichage et ré-afficher
			break;
		
		
	### Actions à partir des boutons associés aux formulaires annexes ##############################
	#================================================================================== Attacher ===	
		case 'attacher2' :

		if ($idmedia) Ecrire_col_med ($idcollection, $idmedia, 'test');
		else erreurMsg ("Vous n'avez sélectionné aucun media");
		break;


	#================================================================================== Modifier ===
	case 'modifier2' :	
	
		// Normaliser les entrées, importer les variables du tableau des entrées
		array_walk ($INPUT, 'NormSQL');
		extract ($INPUT);

		// Écrire tous les champs Medias susceptibles d'avoir été modifiés
		$requete = "UPDATE Medias SET descrimedia='$descrimedia', cotemedia='$cotemedia', lieumedia='$lieumedia', 
								dimmedia='$dimmedia', pubreserv='$pubreserv', dureemedia='$dureemedia'
							WHERE idmedia = $idmedia";
								
		if (Requete ($requete)) {
			// Si écriture réussie, écrire No d'ordre(mode update)
				$result = requete ("UPDATE Col_Med SET ordre_media='$ordre_media' 
													WHERE idcollection=$idcollection AND idmedia=$idmedia");

				if ($result) {
					// Vérifier qu'un média est toujours "mediacle"
					Verif_mediacle ($idcollection);
					miseaJour ($idcollection);
					Message ("Modification enregistrée");
				}
		} else erreurMsg ("Erreur d'écriture");
		
		break;
		
	#=============================================================================== Télécharger ===
		case 'télécharger2' :

		//  Vérification du transfert de l'image
			if ($_FILES['fichier']['error'] !=0) {
				 erreurMsg ("Erreur de transfert du fichier (# %0)",$_FILES['fichier']['error']);
				 AfficheMessages ();
				 break;   // >>>>>>>>>>>>>>>>>>>>>>>>> Sortie du switch télécharger2
			}

			#---- Variables systèmes ----
			$nrmedia = maximum ("Medias") + 1 ;
			$cotemedia = $nom_fichier = $_FILES['fichier']['name'];	// Nom du fichier uploadé
			$dimmedia = round ($_FILES['fichier']['size'] /1000) . ' ko';
			$duree = 0;

			$extension = strtolower (substr (strrchr ($nom_fichier,'.'),1));
			if ($extension == 'jpeg') $extension = 'jpg';
			//  Vérifier que le format est accepté
			if (!in_array ($extension, $t_extensions)) {
				 erreurMsg ("Ce format de média n'est pas reconnu");
				 AfficheMessages ();
				 break;   // >>>>>>>>>>>>>>>>>>>>>>>>> Sortie du switch télécharger2
			}
		
			#---- traitement pdf, mp3, mp4, mov, img
			if($extension=='pdf' || $extension=='mp3' || $extension=='mp4' || $extension=='mov' || $extension=='img'){
				if(move_uploaded_file($_FILES['fichier']['tmp_name'], AdMedia($nrmedia, $db, $extension, 'p', 'rel', 'w')))
				{
					//Vignette : Elle est copiée de "commun" au bon endroit et avec le bon nom
					$ad_new_file = AdMedia($nrmedia, $db, $extension, 'v', 'rel', 'w'); 
					copy("./commun/v_".$extension.".png",$ad_new_file); 
				
					//  Actions sur la Base MySQL
					$pubreserv = ($extension == 'img') ? 'oui' : 'non';
					requete ("INSERT INTO Medias 
						VALUES (NULL, '$descrimedia', '$cotemedia', '$lieumedia', '$dimmedia', '$pubreserv', '$extension', '$duree')");

					if ($result) {
						$idmedia = mysqli_insert_id ($dblink);
						Ecrire_col_med ($idcollection, $idmedia);
					}
				}
				break;		// >>>>>>>>>>>>>>>>>>>>>>>>> Sortie du switch télécharger2
			}

			#---- Redimensionnement de l'image si jpg ou png
			list ($l, $h) = GetImageSize ($_FILES['fichier']['tmp_name'], $info);
			$dimphoto = "$l x $h";
		
			$largeur = min ($l, $plmax);
			$hauteur = floor ($largeur * $h / $l);
			if ($hauteur > $phmax) {
				$largeur = floor ($largeur * $phmax / $hauteur);
				$hauteur = $phmax;
			}
		
			$pi = ImageCreateTrueColor ($largeur, $hauteur);
			if($extension=='jpg')
				$im = ImageCreateFromJPEG ($_FILES['fichier']['tmp_name']);
			
			if($extension=='png'){
				$im = ImageCreateFromPNG($_FILES['fichier']['tmp_name']);
			}

			if($extension=='png')
			{
				imagecolortransparent($pi, imagecolorallocatealpha($pi, 0, 0, 0, 127));
				imagealphablending($pi, false);
				imagesavealpha($pi, true);
			}
		
			ImageCopyResampled ($pi, $im, 0, 0, 0, 0, $largeur, $hauteur, $l, $h);
		
			if($extension=='jpg')
				ImageJPEG ($pi, AdMedia($nrmedia, $db, 'jpg', 'p', 'rel', 'w'));
		
			if($extension=='png')
			{
				ImagePNG ($pi, AdMedia($nrmedia, $db, 'png', 'p', 'rel', 'w'));
			}

			ImageDestroy ($pi);

			#---- Création de la vignette
			$largeur = min ($l, $vlmax);
			$hauteur = floor ($largeur * $h / $l);
			if ($hauteur > $vhmax) {
				 $largeur = floor ($largeur * $vhmax / $hauteur);
				 $hauteur = $vhmax;
			}
			$vi = ImageCreateTrueColor ($largeur, $hauteur);
		
			if($extension=='png')
			{
				imagecolortransparent($vi, imagecolorallocatealpha($vi, 0, 0, 0, 127));
				imagealphablending($vi, false);
				imagesavealpha($vi, true);
			}
		
			ImageCopyResampled ($vi, $im, 0, 0, 0, 0, $largeur, $hauteur, $l, $h);
			//ImageJPEG ($vi, AdMedia($nrmedia, $db, 'jpg', 'v', 'rel', 'w')); 
			if($extension=='jpg')
			{	$test = AdMedia($nrmedia, $db, 'jpg', 'v', 'rel', 'w');
				ImageJPEG ($vi, AdMedia($nrmedia, $db, 'jpg', 'v', 'rel', 'w'));
			}
			if($extension=='png')
				ImagePNG ($vi, AdMedia($nrmedia, $db, 'png', 'v', 'rel', 'w'));
			ImageDestroy ($vi);
		
			if ($descrimedia == '')
				$descrimedia = " ( Aucune info disponible ) ";
		
			$cotemedia = $nom_fichier;
	
		//  Actions sur la Base MySQL
		requete ("INSERT INTO Medias 
			VALUES (NULL, '$descrimedia', '$cotemedia', '$lieumedia', '$dimphoto', 'non', '$extension', '$duree')");

		if ($result) {
			$idmedia = mysqli_insert_id ($dblink);
			Ecrire_col_med ($idcollection, $idmedia);
		}

		break;		// >>>>>>>>>>>>>>>>>>>>>>>>> Sortie du switch télécharger2	

	}
			 
}
	// Dans tous les cas : Vérifier qu'un média est toujours "mediacle"
	Verif_mediacle ($idcollection);

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;

$Xvars['idcollection'] = $idcollection;
$Xvars['mode'] = $mode;

$Xvars['menu'] = $menu;
$Xvars['vignette_init'] = './commun/medias.png';	// Vignette affichée par défaut, remplacée à la volée

$Xvars['view_vignette'] = "javascript:traite_view('".$dir_media."_".$db."/');";
$Xvars['chemin_dossier'] = $dir_media.'_'.$db.'/';

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/mod_medias.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>