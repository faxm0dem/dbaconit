<?PHP 
# v25				190708	PhD		Table Categories devient Domaines, 'tout_affich' devient 'visiteur_visualisation_etat', 
#														modifié liste_champs, ajouté P_OBJET & P_ICONO, retouché init comptage, ajout rech lieurange
# v25.1			190717	PhD		Supprimé f_aff_galevirt, corrigé strlen
# v25.2			190906	PhD		Cryptage du code SQL transmis en URL par les modules listes
# v25.3			191026	PhD		Paramètre 'visiteur_visualisation_etat' devient 'etat_public', corrigé et complété (2 cas)
# v25.4			191125	PhD		L'affichage de résultat est aiguillé mode standard/récolement/désherbage
# v25.5			200104	PhD		Forcer l'affichage résultat en mode standard, revu test récolement
#													Ajout appel de fonctions add-on, ajout des filtres récolement (caractérisé, verrouillé)
# v25.6			200125	PhD		Ajout tri auto sigle/nrinv dans premier mode de recherche, ajouté choix tri idcol
# v25.8			200412	PhD		Le fichier requete_plus est maintenant dans dossier plugin, ajouté custom_css
#	v25.10		210208	Phd		Traitement de l'indice idetablissement courant, 
#													traitement champ mixte idetablissement+nrinv
# v25.14		220713	PhD		Remplacé accolades par crochets d'index (PHP 8)
# v26			221216	PhD		Ajouté 'constatetat', 'modif' dans recherches texte intégral
############
#	Module central de recherche dans la base.
# Il est appelé normalement par le menu_tête, 
#	mais aussi par les listes : liens sur les nombre d'objets, dans ce cas la requète SQL where est fournie dans l'appel.
# 
# Il appelle l'écran de recherche 'rech_form.xml' 
# Au retour, si réponse simple, elle est transmise au module 'consulter.php'
# sinon rechercher apelle l'écran 'rech_resultat.xml', ou 'rech_resultat_recol.xml' ou 'rech_resultat_desh.xml'
# suivant l'état des indicateurs de mode
############

/* Protection des entrées -------------------------------------------------------
'nom'					* POST - Filtré NormIN
'NomRequete' 	* POST - Filtré NormIN
'orderby[]'		- POST - comparé par valeur
'order[]'			- POST - comparé par valeur
'Rech_cle'		- REQ  - Testé isset
'Rech_simple'	- REQ  - Testé isset
'Rech_txt'		- REQ  - Testé isset
'recol_car'		– POST - comparé par valeur
'recol_cver'	– POST - comparé par valeur
'Reset'				- REQ  - Testé isset
's_nrinv'			* POST - Filtré NormIN
's_idcollection'    * POST - Filtré NormIN
's_idetablissement' * POST - Filtré NormIN
'Selection'		- POST - Testé isset
'code_sql'		* POST - Filtré NormIN
'bouton_sql'	* POST - Filtré NormIN
'lien'		* POST - Filtré NormIN
'text'				* POST - Filtré NormIN
'text2'				* POST - Filtré NormIN
'typs[]'			- POST - comparé par valeur
------------------------------------------------------------------------------ */

$custom_css = "rechercher.css";
require_once ('global.js.php');  // avant init
require_once ('init.inc.php');
require_once ('rechercher.inc.php');
require_once ('consulter.inc.php');        		// pour affichage des listes récolement
if (@$dbase['mode_recolement']) require_once ('aff_recol.inc.php');

### Traitement des entrées :
###########################
// Sélection par numéro
$f_selection = @isset ($_POST['Selection']);
$s_idcollection = NormIN ('s_idcollection');
$s_idetablissement = NormIN ('idetablissement');
$s_nrinv  = NormIN ('s_nrinv');
$s_nrinv = str_replace ('§', '-', $s_nrinv);		//// Remplacer les codes § par -  (lecture codes barres clavier Mac)

// Traitement du numéro d'inventaire mixte : idetablissement.K_SEP.nrinv
	$n = strpos ($s_nrinv, K_SEP);
	if ($n != 0) {
		$s_idetablissement = substr ($s_nrinv, 0, $n);
		$s_nrinv = substr ($s_nrinv, $n+1);
	}
// conserver l'indice établissement pour les recherches suivantes
if ($s_idetablissement != 0) $_SESSION['idetab_c'] = $s_idetablissement;

### Recherche par texte
$text = NormIN ('text');
	if ($text != '') $_SESSION['p_text'] = $text;
$text2 = NormIN ('text2');
	if ($text2 != '') $_SESSION['p_text2'] = $text2;
$f_reset = @isset ($_REQUEST['Reset']);
$f_rech_simple = @isset ($_REQUEST['Rech_simple']);	
$f_rech_txt = @isset ($_REQUEST['Rech_txt']);	
$f_recherche = $f_rech_simple + $f_rech_txt;	// f_recherche vrai pour toutes recherches textes

### Appel par un lien HTML qui fournit le WHERE de recherche SQL (liste org, per, mouv,…)
// le code de recherche est codé pour éviter une attaque par injection
// Noter que l'id session est utilisé comme clé de cryptage...
if ($f_lien = @isset ($_REQUEST['lien']))	{
	$key = session_id ();
	$code = NormIN ('lien', 'R');
	$SQL = Phd_decode ($code, $key);
}


### Appel par un include, le code de recherche SQL est fourni
$f_include = (isset ($f_include)) ? $f_include : FALSE; 

### Reqetes prédéfinies
if ($f_bouton = @isset ($_REQUEST['bouton_sql']))	$SQL = NormIN ('bouton_sql'); // bouton pour visisteurs
if ($f_requete = @isset ($_REQUEST['Requete'])) $SQL = NormIN ('code_sql');			// texte area cas général
$nom = NormIN ('nom');	
$NomRequete = NormIN ('NomRequete');

### Paramètres 
$typs = @$_POST['typs'];	
	if ($typs != '') {
		Pb_set (P_MACHINE, in_array ('machine', $typs));
		Pb_set (P_DOCUMENT, in_array ('document', $typs));
		Pb_set (P_LOGICIEL, in_array ('logiciel', $typs));
		Pb_set (P_OBJET, in_array ('objet', $typs));
		Pb_set (P_ICONO, in_array ('iconographie', $typs));
	}
$orderby = @$_POST['orderby'];	
	if ($orderby != '') {
	Pb_set (P_ORD_NRINV, in_array ('nrinv', $orderby));
	Pb_set (P_ORD_IDCOL, in_array ('idcol', $orderby));
	Pb_set (P_ORD_DATE, in_array ('date', $orderby));
	Pb_set (P_ORD_ETAT, in_array ('etat', $orderby));
	Pb_set (P_ORD_COTE, in_array ('coterange', $orderby));
	}
$order = @$_POST['order'];	
	if ($order != '') Pb_set (P_ORDER_ASC, in_array ('asc', $order));
	
$recol_clause = "";
if (isset($_POST['recol_car'])) {					// filtrage des fiches caractérisées 
	switch ($_POST['recol_car']) {
		case 0 : $recol_clause = " AND idtrecol=1"; Break;
		case 1 : Break;
		case 2 : $recol_clause = " AND idtrecol!=1";
	}
}

if (isset($_POST['recol_ver'])) {					// filtrage des fiches verrouillées
	switch ($_POST['recol_ver']) {
		case 0 : $recol_clause .= " AND m10=0"; Break;
		case 1 : Break;
		case 2 : $recol_clause .= " AND m10=1";
	}
}

Pb_set (P_RECOL,0) ; Pb_set (P_SELECT, 0);	// le premier affichage d'un résultat sera toujours en mode standard.

### Initialisations :
#####################
 
// Pour ne pas empiler des ordres de changement de langue (seule la langue est transmise par GET vers "rechercher"
$_SERVER['QUERY_STRING'] = '';  
	
unset ($_SESSION['idcollections']);
unset ($_SESSION['idcollection']);
Deverrouiller (); 			// et oter un verrou éventuel !

// Pour ne pas avoir 2 emplacements où corriger la liste des champs nécessaires pour l'affichage des résultats...
$liste_champs = "Collections.idcollection, type, nrinv, nrancien, description, etat, datemod, idetatfiche, m10, notes,
			Collections.idetablissement, dimlong, dimlarg, dimhaut, dimdiam, dimpoids, couleur, comdim, coterange,
			lieurange, constatetat, modif,
			daterange, idtrecol, idtnposition, Collections.idmachine, Collections.iddocument, Collections.idlogiciel, 
			prefinv, etablissement,localisation, nb_segments, numeration, designation, designation_en, nom, 
			nom_en, museevirtuel, service, typedoc, typedoc_en, titredoc, titrelog, version";

	
### Régler d'abord le cas d'un retour chariot à la place d'un clic  Recherche texte
if ($f_selection AND $s_idcollection=='' AND $s_nrinv=='' AND ($text !='' OR $text2 !='')) {
	$f_selection = FALSE;
	$f_recherche = $f_rech_simple = TRUE;
	Message (Tr ('Commande «Recherche» par nom supposée…', 'Command "Text Search" assumed…'));	
};

# (La limitation de sélection aux fiches complètes est placéee juste après le Where ...$req)
	$etat_public = @$dbase['etat_public'];
	
### Simple effacement des préselections de texte
#################################################################################
if ($f_reset) unset ( $_SESSION['p_text'], $_SESSION['p_text2']);


### Traitement des sélections par numéro, ou par lien HTML, ou par include
#################################################################################
elseif ($f_selection OR $f_lien OR $f_include) {
	$req = '';
	if ($f_lien) $req = $SQL;
	if ($f_include) $req = $SQL_include;
	else {
		if (!empty ($s_nrinv)) {
			if (isset ($dbase['num_commune'])) $req = "nrinv LIKE '$s_nrinv%'";
			else $req = "Collections.idetablissement=$s_idetablissement AND nrinv LIKE '$s_nrinv%'";
		}
		elseif (!empty ($s_idcollection)) $req = "Collections.idcollection = '$s_idcollection'";
	}
	
	if ($req != '') {
		$req_rechercher = 
			"SELECT DISTINCT 	$liste_champs
			FROM Collections
			left JOIN Machines on Machines.idmachine=Collections.idmachine 
			left JOIN Documents on Documents.iddocument=Collections.iddocument     
			left JOIN Logiciels on Logiciels.idlogiciel=Collections.idlogiciel
			";
		if ($f_lien)
			$req_rechercher .=
				"left JOIN Col_Mouv on Col_Mouv.idcollection = Collections.idcollection 
				left JOIN Col_Org on Col_Org.idcollection = Collections.idcollection 
				left JOIN Col_Per on Col_Per.idcollection = Collections.idcollection 
				LEFT JOIN Col_Mate ON Col_Mate.idcollection=Collections.idcollection
				LEFT JOIN Col_Mot ON Col_Mot.idcollection=Collections.idcollection
			";
		$req_rechercher .=
			"left JOIN Designations on Designations.iddesignation=Machines.iddesignation  
			left JOIN Etablissements ON Etablissements.idetablissement=Collections.idetablissement
			LEFT  JOIN Etats ON Etats.idetat=Collections.idetat
			left JOIN Services on Services.idservice=Machines.idservice
			left JOIN Typedocs on Typedocs.idtypedoc=Documents.idtypedoc
			WHERE $req" 
			.(($_SESSION['statut']=='visiteur' AND $etat_public!='') 
											? ' AND (idetatfiche IN ('.$etat_public.')) ' : '')
			." ORDER BY prefinv, Collections.nrinv"
			; 

		$_SESSION['req_rechercher']	= $req_rechercher;			// Mémoriser pour permettre le rappel de cette liste
		$SQLresult = requete ($req_rechercher);
			
											$textreq = Tr ('Sélection', 'Select');		// pour affichage
											$codereq = $req;
											$clausereq = " ORDER BY prefinv, Collections.nrinv";
											$typs = array ('machine', 'document', 'logiciel'); 
	}

	
### Traitement des recherches par texte ou requête prédéfinie
#################################################################################
} elseif ($f_recherche OR $f_requete OR $f_bouton) {

//------------------------------------------------------- Paramètres -------------------	
	$typs = empty ($typs) ? array () : $typs;
	
	$rclause = $recol_clause; 		// Placer déjà les filtrages du récolement 
	

	if (Pb (P_ORD_NRINV))	$rclause .= ' ORDER BY prefinv, Collections.nrinv ';
	elseif (Pb (P_ORD_IDCOL))	$rclause .= ' ORDER BY Collections.idcollection ';
	elseif (Pb (P_ORD_DATE)) 	$rclause .= ' ORDER BY Collections.datemod ';
	elseif (Pb (P_ORD_ETAT))	$rclause .= ' ORDER BY Collections.idetatfiche ';
	elseif (Pb (P_ORD_COTE))	$rclause .= ' ORDER BY Collections.coterange ';

	if (Pb (P_ORDER_ASC)) $rclause .= ' ASC ';
	else $rclause .= ' DESC ';

//------------------------------------------------------- Recherche full text -------------	
	if ($f_recherche) {
		$req_text = empty ($text) ? '' : "'%".$text."%'";		// mis en forme pour request SQL
		$req_text2 = empty ($text2) ? '' : "'%".$text2."%'";	// mis en forme pour request SQL

												$sep = ($req_text2 != '') ? '&nbsp;ET&nbsp;' : '&nbsp;&nbsp;&nbsp;';
												$codereq = $req_text. $sep .$req_text2;
												$clausereq = $rclause;
											
		if (!empty ($req_text)) {
			if ($f_rech_simple) {
				$req = '(
				nom LIKE '.$req_text.	 
				'or nom_en LIKE '.$req_text.	 
				'or nomsecond LIKE '.$req_text.
				'or nomsecond_en LIKE '.$req_text.
				'or onom LIKE '.$req_text.
				'or osigle LIKE '.$req_text.
				'or pnom LIKE '.$req_text.
				'or pprenom LIKE '.$req_text.
				'or modele LIKE '.$req_text.
				'or '.Tr ('designation', 'designation_en').' LIKE '.$req_text.
				'or titrelog LIKE '.$req_text.
				'or titredoc LIKE '.$req_text;
													$textreq = Tr ('Recherche simple', 'Simple search');
			
			} elseif ($f_rech_txt) {
				$req = '(
				description LIKE '.$req_text.
				'or descutilisation LIKE '.$req_text.
				'or descrimach LIKE '.$req_text;
				if (in_array ('aff_complet', $droits)) {
					$req.= 'or notes LIKE '.$req_text;	
					$req.= 'or constatetat LIKE '.$req_text;	
					$req.= 'or modif LIKE '.$req_text;	
					$req.= 'or lieurange LIKE '.$req_text;	
				}	
													$textreq = Tr ('Recherche texte intégral', 'Full text search');
			}
		
				// dans les 2 cas, ajouter recherche matériaux et mots 
				$req = $req.
				'or materiau LIKE '.$req_text.	 
				'or materiau_en LIKE '.$req_text.	 
				'or motcle LIKE '.$req_text.
				'or motcle_en LIKE '.$req_text.
				')';

			if (!empty ($req_text2)) {		
				if ($f_rech_simple) {		
					$req = $req.' AND (
					nom LIKE '.$req_text2.	 
					'or nom_en LIKE '.$req_text2.	 
					'or nomsecond LIKE '.$req_text2.
					'or nomsecond_en LIKE '.$req_text2.
					'or onom LIKE '.$req_text2.
					'or osigle LIKE '.$req_text2.
					'or pnom LIKE '.$req_text2.
					'or pprenom LIKE '.$req_text2.
					'or modele LIKE '.$req_text2.
					'or '.Tr ('designation', 'designation_en').' LIKE '.$req_text2.
					'or titrelog LIKE '.$req_text2.
					'or description LIKE '.$req_text2.
					'or descutilisation LIKE '.$req_text2.
					'or titredoc LIKE '.$req_text2;
			
				} elseif ($f_rech_txt) {
					$req = $req.' AND (
					description LIKE '.$req_text2.
					'or descutilisation LIKE '.$req_text2.
					'or descrimach LIKE '.$req_text2;
					if (in_array ('aff_complet', $droits)) {
						$req.= 'or notes LIKE '.$req_text2;	
						$req.= 'or constatetat LIKE '.$req_text2;	
						$req.= 'or lieurange LIKE '.$req_text2;	
					}	
				}
				
				// dans les 2 cas, ajouter recherche matériaux et mots 
				$req = $req.
				'or materiau LIKE '.$req_text2.	 
				'or materiau_en LIKE '.$req_text2.	 
				'or motcle LIKE '.$req_text2.
				'or motcle_en LIKE '.$req_text2.
				')';
			}

		} else  erreurMsg ("Vous n'avez pas entré de nom recherché");

//------------------------------------------------------- Requête prédéfinie -----------
	} elseif ($f_requete OR $f_bouton) {	
	
		if ($f_requete)  { 
			MajRequetes ($SQL, $NomRequete);		// Mise à jour (éventuelle) d'une requête prédéfinie
		}		

		// Oter la protection htmlspecialchars pour fournir des caractères traitables par SQL...
		$SQL = htmlspecialchars_decode($SQL, ENT_QUOTES);
		
		// Si la requête appelle une fonction du fichier requete_plus (add-on), la traiter...
		if ($SQL[0] == '#') {
			require_once ($dir_plugin.$requete_plus);		// Charger le fichier des add-on
			$st = substr ($SQL, 1);									// Oter le caractère # d'en-tête
			$SQL =  eval ("return $st;");
		}

		// Si la requête contient 2 parties (code SQL et clause de tri), les séparer
		$tr = explode (';',$SQL);
		// Noter que seules les clauses de filtrage du récolement sont maintenues
		// mais que les clauses des requêtes viennent écraser les clauses des paramêtres permanents
		$req = $tr[0];
		if (isset ($tr[1])) $rclause = $recol_clause.$tr[1];

											$textreq = Tr ('Requête [ ', 'Query [ ').$NomRequete.' ]';		// pour affichage
											$codereq = @$req;		// (non défini si effacement de requête)
											$clausereq = $rclause;
	}
	

#-------------------------------------------------- requête SQL principale ---	
	
	// Dresser le tableau des types d'item à rechercher
	$typ_req = "";
		$typ_req .= Pb (P_MACHINE) ? "'machine'," : "";
		$typ_req .= Pb (P_DOCUMENT) ? "'document'," : "";
		$typ_req .= Pb (P_LOGICIEL) ? "'logiciel'," : "";
		$typ_req .= Pb (P_OBJET) ? "'objet'," : "";
		$typ_req .= Pb (P_ICONO) ? "'iconographie'," : "";
		$typ_req = rtrim ($typ_req, ',');

	if (!empty ($req) && strlen ($typ_req)) {
		$req_rechercher = "SELECT DISTINCT $liste_champs		
		FROM Collections
		LEFT  JOIN Col_Org ON Col_Org.idcollection = Collections.idcollection 
		LEFT  JOIN Organismes ON Col_Org.idorganisme = Organismes.idorganisme
		LEFT  JOIN Col_Per ON Col_Per.idcollection = Collections.idcollection 
		LEFT  JOIN Personnes ON Col_Per.idpersonne = Personnes.idpersonne
		
		LEFT  JOIN Col_Mate ON Col_Mate.idcollection=Collections.idcollection
		LEFT  JOIN Materiaux ON Col_Mate.idmateriau=Materiaux.idmateriau
		LEFT  JOIN Col_Mot ON Col_Mot.idcollection=Collections.idcollection
		LEFT  JOIN Motscles ON Col_Mot.idmotcle=Motscles.idmotcle
		
		LEFT  JOIN Machines ON Machines.idmachine=Collections.idmachine 
		LEFT  JOIN Documents ON Documents.iddocument=Collections.iddocument     
		LEFT  JOIN Logiciels ON Logiciels.idlogiciel=Collections.idlogiciel    
		
		LEFT  JOIN Acquisitions ON Acquisitions.idacquisition=Collections.idacquisition
		LEFT  JOIN Domaines ON Domaines.iddomaine=Collections.iddomaine
		LEFT  JOIN Codeprogs ON Codeprogs.idcodeprog=Logiciels.idcodeprog    
		LEFT  JOIN Designations ON Designations.iddesignation=Machines.iddesignation  
		LEFT  JOIN Etats ON Etats.idetat=Collections.idetat
		LEFT  JOIN Langprogs ON Langprogs.idlangprog=Logiciels.idlangprog    
		LEFT  JOIN Langues ON Langues.idlangue=Documents.idlangue
		LEFT  JOIN Etablissements ON Etablissements.idetablissement=Collections.idetablissement
		LEFT  JOIN Productions ON Productions.idproduction=Collections.idproduction
		LEFT  JOIN Services ON Services.idservice=Machines.idservice
		LEFT  JOIN Supports ON Supports.idsupport=Logiciels.idsupport    
		LEFT  JOIN Systemes ON Systemes.idsysteme=Logiciels.idsysteme
		LEFT  JOIN Typedocs ON Typedocs.idtypedoc=Documents.idtypedoc
		WHERE " .( $req )
		.(($_SESSION['statut']=='visiteur' AND $etat_public!='') 
											? ' AND (idetatfiche IN ('.$etat_public.')) ' : '')
		." AND type IN ($typ_req) "
		. $rclause ;

		$_SESSION['req_rechercher']	= $req_rechercher;			// Mémoriser pour permettre le rappel de cette liste
		$SQLresult = requete ($req_rechercher);
		
   $mem = memory_get_usage ();				// Déterminer la taille mémoire utilisée
   debugMsg ('Mémoire utilisée après requête : '.$mem);		
	}
}


### Traitement des résultats des requêtes ###
#################################################################################
if ($f_recherche OR $f_selection OR $f_requete OR $f_bouton OR $f_lien OR $f_include) {

	if ($nb = @mysqli_num_rows ($SQLresult)) {
		
		if ($nb == 1) {				// Un seul résultat : l'afficher ----------
			$ligne =  mysqli_fetch_assoc ($SQLresult);
			$idcollection = $_SESSION['idcollection'] = $ligne['idcollection'];

			// Se brancher sur l'écran "Consulter"			  
			$_SERVER['PHP_SELF'] = 'consulter.php';
			include ($_SERVER['PHP_SELF']);							// >>>>>>>>>>>>>>>>>> Branchement vers "Consulter"
		}

 		Message ('Nombre de résultats : %0', $nb);

#-------------- Affichage des tableaux de résultat à partir d'un modèle XML ---	
		Debut ();

						// Afficher le code de la recherche
						$Xvars['textreq'] = $textreq;
						$Xvars['codereq'] = $codereq;
						$Xvars['clausereq'] = $clausereq;
						$Xvars['typs']		= array_merge ($typs, array (' ',' ', ' ')); // pour avoir toujours au moins 4 éléments						
						// Conserver ce code pour un réaffichage en cas de modif récolement
						$_SESSION['reaffichage'] = array ( $textreq, $codereq, $clausereq, $Xvars['typs']);
	
		// Préparer le comptage machines/documents/logiciels/objets/icongraphie
		$Xvars['compt_c'] = $Xvars['compt_d'] = $Xvars['compt_l']= $Xvars['compt_o']= $Xvars['compt_i'] = 0;

		// Charger le résultat de la recherche
		$Xvars['SQLresult'] = $SQLresult;
		$Xvars['db'] = $db;					// Pour pouvoir ajouter le nr de la base dans l'URL
	
		// Afficher le résultat avant le tableau
		AfficheMessages ();
	 
		// Affichage standard ou affichage récolement ou affichage désherbage
		$Xvars['droits'] = $droits;
		$_SESSION['ids'] = array ();		// effacement des marques de sélection récolement
		
		Aiguil_rech_resultat ();		// AFFICHAGE DES RÉSULTATS
	
		Fin ();
		exit;						// >>>>>>>> EXIT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	}
	
	else 
		erreurMsg ("Aucun objet n'a été trouvé");
		Message (Tr ("Pensez à utiliser l'affichage 'Consultation'- 'Liste des établissements'",
							"Think of command 'Searchjng' - 'Organization list'"));
}



### Affichage des formulaires de recherche
#################################################################################
	Debut ();

	// Préparer le tableau des variables
	$Xvars['droits'] = $droits;

	$Xvars['f_modif'] = @$f_modif;	// fourni par "consulter"
	$Xvars['idetab_c'] = $_SESSION['idetab_c'];
	$Xvars['nom'] = @$nom;
	$Xvars['num_commune'] = isset ($dbase['num_commune']);		// Cas particulier numération commune à tous établissements
	$Xvars['p_text'] = (isset ($_SESSION['p_text'])) ? $_SESSION['p_text'] : '';
	$Xvars['p_text2'] = (isset ($_SESSION['p_text2'])) ? $_SESSION['p_text2'] : '';
	$Xvars['void'] = '';
	
	$liste_xml = Xopen ('./XML_modeles/rech_form.xml') ;
	Xpose ($liste_xml);
	
#------------------------------------------------- Sortie
Fin ();
?>
