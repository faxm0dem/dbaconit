<?php
# v18			170801	PhD		Création extrait de mod_medias et de medias.ajax, ajouté test dans 'Ecrire_col_med'
# v25			190615	PhD		Supprimé inst debug 8
# v25.4		191208	PhD		Simplifié calcul d'adresse pour accepter https
# v25.9		200721	PhD		Ajout format img
###
?>

<script type="text/javascript">

var format = 'init';

function vignette(chemin_dossier) {

	var medias_value = document.getElementById('liste_medias').value;
	var id 		= medias_value.split(",")[0];

	var format 	= medias_value.split(',')[1];

	//Calcul le numero de dossier de l'image avec son numéro
	var dossier = parseInt(parseInt(id) / 1000);
	
	//Initialise l'extension de fichier
	var extension=format;
	if( (format=='mp3') || (format=='mp4') || (format=='mov') || (format=='pdf') || (format=='img') )
		extension='png';
		
	var chemin_image = chemin_dossier + format + '_v' + dossier + '/' + id + '.' + extension;

	//Modifie la source de l'image de previsualisation du media
	document.getElementById('img_preview_image').setAttribute('src',chemin_image);
	
	var newImg = new Image();
	newImg.src = chemin_image;
	
}


function traite_view(chemin_dossier){
	
	var medias_value = document.getElementById('liste_medias').value;
	var id 		= medias_value.split(",")[0];
	
	if(id!='') {
		var format 	= medias_value.split(',')[1];

		//Calcul le numero de dossier de l'image avec son numéro
		var dossier = parseInt(parseInt(id) / 1000);
		var chemin_image = chemin_dossier + format + '_' + dossier + '/' + id + '.' + format;
		var enc_legende    = medias_value.split(",")[2];		
		var page = 	"medias.view.php?"+
					"media="+ chemin_image +
					"&legende=" + enc_legende +
					"&format=" + format;

		window.open(page, 'Aconit', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, width=50%,height=50%');
	}
}
</script>


<?php
############################################################## Ecrire_col_med ###
function Ecrire_col_med ($idcollection, $idmedia, $test='notest') {

	if ($test=='test') {
		// vérifier l'existence de l'enregistrement média
		$res = requete ("SELECT idmedia FROM Medias WHERE idmedia=$idmedia");
		if (0 == mysqli_num_rows ($res)) {
			erreurMsg (Tr ("Cet enregistrement media n'existe pas", "This media	 record doesn't exist"));
			return;						// >>>>>>>>>>>>>>>>>>>>>>>> EXIT
		}
	}

	// Noter le plus haut numéro d'ordre_media utilisé
	$result_ordre = requete ("SELECT MAX(ordre_media) AS ordre_max FROM Col_Med WHERE idcollection=$idcollection");
	$ligne_ordre = mysqli_fetch_array ($result_ordre);
	$ordre_max = $ligne_ordre['ordre_max'];

	// assurer l'écriture dans Col_Med et la date de mise à jour de Collections
	$ordre_media = round($ordre_max, -1) + 10;
	$mediacle = ($ordre_media == 10) ? 'oui' : 'non';  // si c'est le premier, il est toujours mediacle
	$result = requete ("INSERT INTO Col_Med VALUES ('$idcollection','$idmedia', '$ordre_media', '$mediacle')");

	if ($result) {
		miseaJour ($idcollection);
		Message ("Média %0 attaché", $idmedia);
	}
}

############################################################ Verif_mediacle ###
function Verif_mediacle ($idcollection) {
# Positionner un lien et un seul en mediacle=oui

# Sortir la liste des liens-médias triés par ordre
	$result = requete ("SELECT idmedia, ordre_media, mediacle FROM Col_Med WHERE idcollection=$idcollection 
											ORDER BY ordre_media");
	if (mysqli_num_rows ($result)) {										// pas de traitement si pas de média
		$prime = TRUE;
		while ($ligne = mysqli_fetch_array ($result)) {
			$idmedia = $ligne['idmedia'];

			// Le premier doit être média clé
			if ($prime) {
				if ($ligne['mediacle'] != 'oui') {					 // si non, imposer "mediacle" à 'oui' 
					requete ("UPDATE Col_Med SET mediacle='oui' WHERE idcollection=$idcollection and idmedia=$idmedia");		
				}
				$prime = false;
			} 
			
			else {
				// Tous les suivants ne doivent pas être media clé !
				if ($ligne['mediacle'] == 'oui') {					 // si oui, imposer "mediacle" à 'non' 
					requete ("UPDATE Col_Med SET mediacle='non' WHERE idcollection=$idcollection and idmedia=$idmedia");		
				}	
			}
		}
	}
}	

############################################################ XML_liste_attach ###
function XML_liste_attach ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;

	// Si tag de début, appeler la liste des médias attachés
	if ($loop === 0) {
		$idcollection = $Xvars['idcollection'];
	 	$Xvars['SQLresult_attach'] = requete ("SELECT Medias.idmedia, descrimedia, formatmedia, ordre_media, mediacle 
	 		FROM Col_Med, Medias WHERE idcollection=$idcollection AND Col_Med.idmedia=Medias.idmedia
	 		ORDER BY ordre_media");
 	}
	
	//  Appel du media courant
	$ligne = mysqli_fetch_assoc ($Xvars['SQLresult_attach']); 
	$idmedia = $ligne ['idmedia'];
	$Xvars['media']   = '('.$ligne['ordre_media'].') -- '.$idmedia.' : '.$ligne ['descrimedia'];
	
	// 'id_type' reçoit une liste de 3 items, pour appel ultérieur de la fenêtre d'affichage 
	$nrinventaire = Nrinventaire ($Xvars['idcollection'], FALSE, '_');
	$Xvars['id_type'] = $idmedia.','.$ligne ['formatmedia'].','.base64_encode($nrinventaire.'_'.$ligne['descrimedia']);
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 


