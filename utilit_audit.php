<?php
# v25.1			190717	PhD		Corrigé MYSQLI_BOTH
# v25.4			191210	PhD		Simplifié calcul d'adresse pour accepter https, corrige doublons nrinv
# v25.8			200411	PhD		Ajout custom_css
# v25.9.02	201008	PhD		Ajout audit des liens matériaux et mots clés
###

$custom_css = "utilit_audit.css";
require_once ('init.inc.php');		// Lancer l'initialisation avant d'envoyer Javascript

############################################################################### Audit_col ####
function Audit_col ($table, $table_col) {
# Recherche des éléments orphelns, manquants et des fiches collections manquantes dans la table $tab_col
# Note : les orphelins ne sont pas affichés dans le cas des tables mérariaux et mots clés : 
# 				il est normal que certains matériaux ou mots clés ne soient pas utilisés.
#
	global $arr_id_collection;
	
	// Liste des éléments secondaires 
	$result = requete ("SELECT ".nomid ($table)." FROM  ".$table);
	echo "<p>$table - Nombre d'éléments : ".mysqli_num_rows($result)."</p>\n";
	$arr_id = array();
	while ($ligne = mysqli_fetch_array($result)) {
		$arr_id[] = $ligne[0];
	}	
	
	// Liste des appels de ces éléments dans fichier Col_xxx
	$result = requete ("SELECT DISTINCT ".nomid ($table)." FROM  $table_col");		
	$arr_id_col = array();
	while ($ligne = mysqli_fetch_array($result)) {
		$arr_id_col[] = $ligne[0];
	}	

	if ($table_col != 'Col_Mate' AND $table_col != 'Col_Mot') {
		$orphelins =  array_diff ($arr_id, $arr_id_col);
		echo "<h4> $table orphelins : ".count ($orphelins)."</h4>\n";
		echo "<p style='color:red'>";
		foreach ($orphelins as $id) {
			echo nomid ($table)." = $id <br />\n";
		}
		echo "</p>";
	}	
	
	$manquants =  array_diff ($arr_id_col, $arr_id);
	echo "<h4>$table manquants : ".count ($manquants)."</h4>\n";
	echo "<p style='color:red'>";
	foreach ($manquants as $id) {
		echo nomid ($table)." = $id <br />\n";
	}
	echo "</p>";

	// Liste des idcollections dans fichier Col_xxx
	$result = requete ("SELECT DISTINCT idcollection FROM  $table_col");		
	$arr_idcolcol = array();
	while ($ligne = mysqli_fetch_array($result)) {
		$arr_idcolcol[] = $ligne[0];
	}	
	$perdus =  array_diff ($arr_idcolcol, $arr_id_collection);
	echo "<h4>Fiches Collections manquantes : ".count ($perdus)."</h4>\n";
	echo "<p style='color:red'>";
	foreach ($perdus as $id) {
		echo "idcollection = $id <br />\n";
	}
	echo "</p><hr/><br/>";	
	return;
}	

############################################################################### Audit_lien ####
function Audit_lien ($table) {

	// Tableau des id dans la table secondaire Machines...
	$result = requete ("SELECT ".nomid ($table)." FROM  ".$table);
	echo "<p>Nombre d'&eacute;l&eacute;ments trait&eacute;s : ".mysqli_num_rows($result)."</p>\n";
	
	$arr_id = array();
	while ($ligne = mysqli_fetch_array($result)) {
		$arr_id[] = $ligne[0];
	}	
	
	// Tableau des id dans la table Collections
	$result = requete ("SELECT ".nomid ($table)." FROM  Collections");		
	$arr_id_col = array();
	while ($ligne = mysqli_fetch_array($result)) {
		$arr_id_col[] = $ligne[0];
	}	

	$orphelins =  array_diff ($arr_id, $arr_id_col);
	echo "<h4> $table orphelins : ".count ($orphelins)."</h4>\n";
	echo "<p style='color:red'>";
	foreach ($orphelins as $id) {
		echo nomid ($table)." = $id <br />\n";
	}
	echo "</p>";
		
	$manquants =  array_diff ($arr_id_col, $arr_id);
	echo "<h4>$table manquants</h4>\n";
	echo "<p style='color:red'>";
	foreach ($manquants as $id) {
		if ($id !=0) echo nomid ($table)." = $id <br />\n";
	}
	echo "</p><hr/><br/>";
	return;
}


##############################################################################################
?>

<script type="text/javascript">

var format = 'init';

function createXhrObject()
{
    if (window.XMLHttpRequest)
        return new XMLHttpRequest();
 
    if (window.ActiveXObject)
    {
        var names = [
            "Msxml2.XMLHTTP.6.0",
            "Msxml2.XMLHTTP.3.0",
            "Msxml2.XMLHTTP",
            "Microsoft.XMLHTTP"
        ];
        for(var i in names)
        {
            try{ return new ActiveXObject(names[i]); }
            catch(e){}
        }
    }
    window.alert("Votre navigateur ne prend pas en charge l'objet XMLHTTPRequest.");
    return null; // non supporté
}

function AJAX_suppression_medias(id) {

	//Recupere le dossier du fichier php actuellement 'actif'
	chemin_dossier = '';
	newURL = '';
	decoupURL = location.href.split('/');

	for (i=0, n = decoupURL.length-1; i<n; i++)
	{
	newURL += decoupURL[i]+'/'; 
	}
	chemin_dossier = newURL;
	chaine = chemin_dossier+'utilit_audit.ajax.php';
	
	var xhr=null;
	xhr = createXhrObject();

	//true pour asynchrone .... si on veut que ce soit bloquant mettre false
	xhr.open("POST",chaine,true);
	xhr.Charset = "ISO8859-1"
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
	xhr.onreadystatechange=function() 
	{    
		if (xhr.readyState==4) 
		{ 
			if(xhr.status == 200)
			{
				var reponse = xhr.responseText;
				//"Alimente" la réponse à l'écran
				document.getElementById('val_retour_'+id).innerHTML = reponse;
			} 
		}
	}
	
	xhr.send("param1="+id);

}
</script>

<?php
########################################################################

/* Protection des entrées -------------------------------------------------------
'audit'			- POST submit - uniquement testé issset
'x_xxxx'		- POST checkbox - uniquement testé isset
------------------------------------------------------------------------------ */

Debut();   

### Traitement des entrées
####################################

$f_audit = isset ($_POST['audit']);
$t_keys = array_keys ($_POST);

### Affichage du formulaire :
####################################

	$liste_xml = Xopen ('./XML_modeles/utilit_audit_form.xml') ;
	Xpose ($liste_xml);
	 
### Traitement audit :
######################

if ($f_audit) echo "<h1>Audit de la base de données </h1>\n";
	
	// Table des idcollection _ Toujours utile !
	$result = requete ("SELECT idcollection FROM  Collections");
	$arr_id_collection = array();
	while ($ligne = mysqli_fetch_array($result)) {
		$arr_id_collection[] = $ligne[0];
	}	

### Audit des liens machines #########################################################################################
	if (array_search('o_machine',$t_keys)) {
		echo "<h3>Audit des liens machines</h3>\n";
				
		Audit_lien ("Machines");
	}

### Audit des liens documents	########################################################################################
	if (array_search('o_document',$t_keys)) {
		echo "<h3>Audit des liens documents</h3>\n";
				
		Audit_lien ("Documents");
	}

### Audit des liens logiciels	########################################################################################
	if (array_search('o_logiciel',$t_keys)) {
		echo "<h3>Audit des liens logiciels</h3>\n";
				
		Audit_lien ("Logiciels");
	}

### Liens machines / documents / logiciels manquants	################################################################
	if (array_search('l_liens',$t_keys)) {
		echo "<h3>Objets ni 'Machines' ni Documents' ni 'Logiciels'</h3>\n";
		
		$result = requete ("SELECT idcollection, idmachine, iddocument, idlogiciel FROM  Collections");
		echo "<p>Nombre d'&eacute;l&eacute;ments trait&eacute;s : ".mysqli_num_rows($result)."</p>\n";
		
		echo "<p style='color:red'>";
		while ($ligne = mysqli_fetch_array($result)) {
			if ((!$ligne['idmachine'])&&(!$ligne['iddocument'])&&(!$ligne['idlogiciel'])) {
				echo "idcollection = " . $ligne['idcollection'] . "<br />\n";
			}
		}
		echo "</p><hr/><br/>";
	}
	
### Doublons "idetablissement-nrinv"	########################################################################################
	if (array_search('d_nrinv',$t_keys))  {
		echo "<h3>Recherche des doublons 'idetatblissement-nrinv'</h3>\n";
		
		$result = requete ("SELECT idcollection, idetablissement, nrinv FROM  Collections ORDER BY idcollection");
		echo "<p>Nombre d'&eacute;l&eacute;ments trait&eacute;s : ".mysqli_num_rows($result)."</p>\n";
		
		$tnrinv = array ();
		// composer un tableau des couples idetablissemnet-nrinv
		while ($ligne = mysqli_fetch_array($result)) {
			$tnrinv[$ligne['idcollection']] = $ligne['idetablissement'].'#'.$ligne['nrinv'];
		}
		// trier ce tableau et le lire en recherchant les doublons
		asort ($tnrinv);
		
		echo "<p style='color:red'>";
		$prev_couple = '';
		
		foreach ($tnrinv as $idcol => $couple) {		
			if($couple==$prev_couple) {
				$c = explode ('#', $couple);
				echo "Doublon : idetablissement = $c[0] - nrinv = $c[1] - idcollection = $prev_idcol/$idcol<br />\n";
			}
			$prev_idcol=$idcol;
			$prev_couple=$couple;
		}	
		echo "</p><hr/><br/>";
	}

######################################################################################################################
### Audit des liens matériaux	######################################################################################
	if (array_search('l_mate',$t_keys)) {
		echo "<h3>Audit des liens matériaux</h3>\n";
	
		Audit_col ("Materiaux", "Col_Mate");
	}
	
### Audit des liens mots clés	######################################################################################
	if (array_search('l_mot',$t_keys)) {
		echo "<h3>Audit des liens mots clés</h3>\n";
	
		Audit_col ("Motscles", "Col_Mot");
	}
	
### Audit des liens mouvements	######################################################################################
	if (array_search('l_mouv',$t_keys)) {
		echo "<h3>Audit des liens mouvements</h3>\n";
	
		Audit_col ("Mouvements", "Col_Mouv");
	}
	
### Audit des liens organismes	######################################################################################
	if (array_search('l_organisme',$t_keys)) {
		echo "<h3>Audit des liens organismes</h3>\n";
	
		Audit_col ("Organismes", "Col_Org");
	}

### Audit des liens personnes ########################################################################################
	if (array_search('l_personne',$t_keys)) {
		echo "<h3>Audit des liens personnes</h3>\n";
	
		$err = Audit_col ("Personnes", "Col_Per");
	}



######################################################################################################################
### Medias orphelins	################################################################################################
	if (array_search('o_photo',$t_keys)) {
		echo "<h3>Médias orphelins</h3>\n";
		
		$result = requete ("SELECT idmedia FROM  Medias");
		echo "<p>Nombre d'&eacute;l&eacute;ments trait&eacute;s : ".mysqli_num_rows($result)."</p>\n";
		
		$arr_id_med = array();
		while ($ligne = mysqli_fetch_array($result)) {
			$arr_id_med[] = $ligne[0];
		}	
		
		$result = requete ("SELECT idmedia FROM  Col_Med");		
		$arr_id_colmed = array();
		while ($ligne = mysqli_fetch_array($result)) {
			$arr_id_colmed[] = $ligne[0];
		}	

		$orphelins =  array_diff ($arr_id_med, $arr_id_colmed);
		
		foreach ($orphelins as $id) 
		{
			//recupere description du media dans la table medias + format media
			$result = requete ("select descrimedia,formatmedia from Medias where idmedia=$id");
			if (!$result) die ('Erreur: ' . mysqli_error ($dblink));	
			$result = mysqli_fetch_array ($result, MYSQLI_BOTH);
			$descri_media = $result['descrimedia'];
			$format_media = $result['formatmedia'];
			
			$chemin_vignette_media = AdMedia ($id, $db, $format_media, 'v');
			
			$id_retour = "val_retour_".$id;
			$ligne_infos_media = "<table><tr>
						<td style='color:red' width=80 align='left'> idmedia = ".$id."</td>
						<td width=300 align='left'>".$descri_media."</td>
						<td width=200 align='left'>"."<img alt='Pas de vignette' src='$chemin_vignette_media'>"."</td>
						<td width=150 align='right'>
							<input type='button' value='Supprimer' onClick='AJAX_suppression_medias($id)'>"."</td>
						<td width=150 align='right' id=$id_retour></td>
						</tr></table><br />";
			echo $ligne_infos_media;
		}
		echo "<hr/><br/>";
	}
	
### Fiches Collections manquantes dans les liens médias ########################################################################################
	if (array_search('l_media',$t_keys)) {
		echo "<h3>Fiches collections manquantes dans les liens médias</h3>\n";
		
		$result = requete ("SELECT DISTINCT idcollection FROM  Col_Med");		
		echo "<p>Nombre d'éléments traités : ".mysqli_num_rows($result)."</p>\n";
		
		$arr_id_colmed = array();
		while ($ligne = mysqli_fetch_array($result)) {
			$arr_id_colmed[] = $ligne[0];
		}	

		$orphelins =  array_diff ($arr_id_colmed, $arr_id_collection);
		echo "<p style='color:red'>";
		foreach ($orphelins as $id) {
			echo "idcollection = $id <br />\n";
		}
		echo "</p><hr/><br/>";
	}

### Fichiers médias manquants ########################################################################################
	if (array_search('f_media',$t_keys)) {
		echo "<h3>Fichiers médias manquants</h3>\n";
		
		$result = requete ("SELECT idmedia, formatmedia FROM  Medias");
		echo "<p>Nombre d'&eacute;l&eacute;ments trait&eacute;s : ".mysqli_num_rows($result)."</p>\n";
	
		echo "<p style='color:red'>";
		while ($ligne = mysqli_fetch_array($result)) {
			$idmedia = $ligne['idmedia'];
			$formatmedia = $ligne['formatmedia'];
			if (!file_exists(AdMedia ($idmedia, $db, $formatmedia, ''))) 
			echo "Media = $idmedia.$formatmedia<br />\n";			
			if (!file_exists(AdMedia ($idmedia, $db, $formatmedia, 'v'))) 
			echo "Vignette = $idmedia $formatmedia<br />\n";			
		}	
		echo "</p><hr/><br/>";
	}

### Fichiers médias en excès ########################################################################################
	if (array_search('f_mediax',$t_keys)) {
		echo "<h3>Fichiers médias en excès</h3>\n";

		// Table des médias connus -------------------------------------------------------------------------------------
		$result = requete ("SELECT idmedia, formatmedia FROM  Medias");
		echo "<p>Nombre d'&eacute;l&eacute;ments trait&eacute;s : ".mysqli_num_rows($result)."</p>\n";	
		$arr_id_med = array();
		//on construit un tableau des medias avec leur format en '.'.format
		while ($ligne = mysqli_fetch_array($result)) {
			$arr_id_med[] = $ligne['idmedia'].'.'.$ligne['formatmedia'];
		}	

		// Table des fichiers médias existants
		$arr_file_id_med = array();
		// Table des fichiers médias-vignettes existantes
		$arr_vignette_id_med = array();
		
		$nb_files = 0;
		$le_dossier = $dir_media.'_'.$db.'/';

		// Lecture du dossier média principal -----------------------------------------------------------------------------
		if($dossier = opendir($le_dossier))
		{
			while(false !== ($fichier = readdir($dossier)))
			{
				// Recherche des sous-dossiers valides
				if(($fichier[0]!='.') && is_dir($le_dossier.'/'.$fichier))							//si on est en présence d'un dossier
				{
					$file_type = explode("_",$fichier);
					if( substr($file_type[1], 0, 1) != "v")					// Traiter d'abord dossiers medias --------------------------
					{
						// On traite les dossiers des type de medias connus 
						// (pour ne pas traiter le dossier vignettes_communes ou autre)
						if( ($file_type[0]=="jpg")||($file_type[0]=="png")||($file_type[0]=="pdf")||
							($file_type[0]=="mov")||($file_type[0]=="avi")||($file_type[0]=="mp3")||
							($file_type[0]=="mp4"))
						{
							// Lecture d'un dossier de medias ---------------------------------------------------------------------
							$le_sous_dossier = $le_dossier.'/'.$fichier;
							if($sous_dossier = opendir($le_sous_dossier))
								while(false !== ($fichier2 = readdir($sous_dossier))) {
									if(!is_dir($le_sous_dossier.'/'.$fichier2))
										$arr_file_id_med[$nb_files++] = $fichier2;
								}
						}
					}
					else		
					{	
						// Lecture d'un dossier de vignettes ---------------------------------------------------------------------
						$le_sous_dossier = $le_dossier.'/'.$fichier;
							if($sous_dossier = opendir($le_sous_dossier))
								while(false !== ($fichier2 = readdir($sous_dossier))) {
									if(!is_dir($le_sous_dossier.'/'.$fichier2))
										$arr_vignette_id_med[$nb_files++] = $fichier2;
								}
					
					}
				}
			}
		}

		// Rapports -------------------------------------------------------------------------------------------------------

			echo sizeof($arr_file_id_med)." Fichiers médias physique et ".sizeof($arr_id_med)." Id médias dans la base";
			$intersection = array_intersect ($arr_file_id_med ,$arr_id_med);
			echo "<br/> [Fichiers médias présents et utilisés dans la base : ".sizeof($intersection)."]";
			$exces =  array_diff ($arr_file_id_med, $arr_id_med);

			echo "<br/><br/><u>"." Fichiers médias en exces</u> : ".sizeof($exces);
			echo "<p style='color:red'>";
			foreach ($exces as $id) {
				echo "$id <br/>\n";
			}
			
			// Pour les vignettes :
			//Remplacer les extensions 'pdf' , 'mp3' , 'mp4' , 'mov' par 'png' dans le tableau des medias de la base
			$nb=0;
			foreach ($arr_id_med as $id) {
				$typ = strrchr($arr_id_med[$nb],".");
				if((($typ=='.pdf') || ($typ=='.mp3') || ($typ=='.mp4') || ($typ=='.mov')))
					$arr_id_med[$nb] = substr($arr_id_med[$nb],0,strlen($arr_id_med[$nb])-strlen($typ)).".png";
				$nb++;
			}
			
			//récupérer la différence entre les 2 tables correspondant à des vignettes
			$exces_vignette =  array_diff ($arr_vignette_id_med, $arr_id_med);
			echo "<p style='color:black'>";
			echo "<br/><u>"." Fichiers vignettes en exces</u> : ".sizeof($exces_vignette);
			echo "<p style='color:red'>";
			foreach ($exces_vignette as $id) {
				echo "$id <br/>\n";
			}
			echo "</p><hr/><br/>";
	}

Fin ();
?>