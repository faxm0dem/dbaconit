<?php 
# v21.2			180210	PhD		Corrigé écriture de nrinv dans la base (entre apostrophes)
# v22				180524	PhD		Ajouté création de nrinv pour format MGSM
# v22.1			180605	PhD		numéros MGSM sur 3 chiffres
# v24				190225	PhD		Remplacé ici 'mode_mgsm'_ par 'mode_MdF', localisation=>etablissement, ajouté Nrunique
# v25				190615	PhD		Correction commentaire, ajouter traitement champ type
# v25.3.01	191107	PhD		Corriger sélection nouvel nrinv à partir No série
# v25.8			200411	PhD		Ajout custom_css
# v25.13		220401	PhD		Ajout traitement de nrinv en longueur fixe
###

		

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
'tabtyp'				* REQUEST - Filtré NormIN	(regroupe les 2 valeurs dans l'URL d'appel création)
'table'					- POST		- filtré NormIN
'type'					- POST		- filtré NormIN
'codeserie'			- POST		- filtré NormIN
'comserie'			- POST		- filtré NormIN
'idetablissement' -POST		- vérifié numérique
Toutes les autres entrées proviennent du formulaire en POST et sont filtrées NormIn
------------------------------------------------------------------------------ */

$custom_css = "creer.css";
require_once ('init.inc.php');
require_once ('consulter.inc.php');


############################################################ XML_etabl ###
function XML_etabl ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_etabl;

	// Si tag de début, appeler la liste des établissements
	if ($loop === 0) {
		
		$SQLresult_etabl = requete (
			"SELECT *	FROM Etablissements ORDER BY prefinv");
 	}
			
	//  Appel de l'élément courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult_etabl)) { 
		$Xvars['ligne'] = $ligne;
	
		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

########################################################################################################################
############################################################ XML_serie ###
function XML_serie ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_serie;

	// Si tag de début, appeler la liste des établissements
	if ($loop === 0) {
		$SQLresult_serie = requete (
			"SELECT *	FROM Series 
			LEFT JOIN Etablissements ON Series.idetablissement = Etablissements.idetablissement
			WHERE Series.idetablissement = ".$Xvars['idetablissement'].
			" ORDER BY codeserie DESC");
 	}
			
	//  Appel de l'élément courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult_serie)) { 
		$Xvars['ligne'] = $ligne;
	
		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

####################################################################################################################
####################################################################################################################

## Traitement des entrées 
#########################

	// Choix de la table transmis dans l'URL lors de l'appel de cette commande

	$tabtyp = NormIN('tabtyp', 'R');
	if ($tabtyp) {															// Premier appel, par menu_tete
		list ($table, $type) = explode ("|", $tabtyp);
		if (!in_array ($table, array ('Machines','Documents','Logiciels')))  DIE ("Nom de table '".$table."' non admis !");
	} else {																		// Appels suivants qui gardent l'info
		$table = Normin ('table');
		$type = Normin ('type');
	}
	
	// Retour du formulaire
	$action = @$_POST['action'];
	if (!$action) $action = 'empty';

	$idetablissement = @$_POST['idetablissement'];
	if (isset ($idetablissement) AND  !is_numeric($idetablissement)) 	DIE ("*** Paramètre 'idetablissement' faux ! ***"); 
	$codeserie = NormIN ('codeserie');
	$comserie = NormIN ('comserie');
	
	// Variables session
	$nomrapcreat = $_SESSION['rapporteur'];
	$datecreat = date ('Ymd'); 
   
   
##########################################################

Debut ();		// Affichage menu principal

	// Création d'une table de structure simplifiée, pour obtenir les valeurs des champs par défaut
	$tstruct = Tstructure (array ('Collections', $table));
	
	// Par défaut, les drapeaux d'affichage sont faux...
	$Xvars['f_creer_serie'] = $Xvars['f_etabl']	= $Xvars['f_serie'] = FALSE;
	// ainsi que le drapeau d'écriture
	$f_create = FALSE;
	
	switch ($action) {
		// Premier passage dans Créer ##########################################################################
		case 'empty' : 		
			$Xvars['f_etabl']	= TRUE;			// Préparer l'affichage du premier écran, saisie du numéro d'établissement
			break;
		
		// Retour après saisie de l'établissement ##############################################################
		case 'v_etabl' : 		

			// si l'utilisateur n'est pas autorisé à travailler pour cet etablissement : renvoyer l'écran pour correction 
			if (!Autor_modif (0, $idetablissement)) {	
				$Xvars['f_etabl']	= TRUE;
				break;											// >>>> on réaffiche
			}
		
			// Chercher le sigle et le type de numération
			$requete = requete ("SELECT prefinv, numeration FROM Etablissements WHERE idetablissement=$idetablissement");
			$ligne = mysqli_fetch_assoc ($requete);
			mysqli_free_result ($result);
		
			if ($ligne['numeration'] == 'MdF') {				// ------- Numération MdF -----------------
				$Xvars['idetablissement'] = $idetablissement;
				$Xvars['f_serie'] = TRUE;		// => on affiche le 2e écran, saisie du code série

			} else {																		// ------- Numération simple -----------------
				// Recherche du prochain numéro
				// L'opération "nrinv+1" oblige MySQL à prendre la partie numérique 
				// des chaines de caractère nrinv... Ensuite l'opérateur MAX fonctionne correctement.
				// Sans cette astuce, MAX (nrinv) sort le dernier en tri alphanumérique, 
			
				// Distinguer le cas standard du cas spécial avec numérotation commune à tous les établissements
				$where = isset ($dbase['num_commune']) ? "" : "WHERE idetablissement=$idetablissement";
				$result = requete ("SELECT MAX(nrinv+1) AS nr FROM Collections ".$where);
				$ligne = mysqli_fetch_assoc ($result);
				mysqli_free_result ($result);
				if (!($nrinv = $ligne['nr'])) $nrinv = 1;		
				
				// Traitement de la longueur imposée, fixée par nbr_cars_nrinv
					// si absent : longueur variable
					// sinon : longueur imposée
				if (isset ($dbase['nbr_cars_nrinv'])) {
					$nbcars = $dbase['nbr_cars_nrinv'];
					$nrinv = (string)$nrinv; 		// repasser en string pour pouvoir tester le nbr de caractères utilisés
					if ($nbcars >= strlen ($nrinv)) $nrinv = substr ('000000000'.$nrinv, -$nbcars, $nbcars);
					else die (Tr ("Le numéro d'inventaire <$nrinv> ne peut pas être affiché en <$nbcars> caractères",
												"The inventory number <$nrinv> cannot be expressed in <$nbcars> caracters"));
				}
				
				// Demander la création de la fiche
				$f_create = TRUE;				// =>  on lance les écritures
			}			
			break;
		
		
		// Retour avec demande de création d'une nouvelle série ##################################################
		case 'v_demande_serie' :
			$Xvars['idetablissement'] = $idetablissement;
			$Xvars['f_creer_serie'] = TRUE;		// => affichage formulaire nouvelle série
			$Xvars['f_serie'] = TRUE;					// maintenir le 2e écran, saisie du code série
			break;
	

	// Retour du formulaire nouvelle série ##################################################
		case 'v_creer_serie' :

			// Vérifier la valdité des infos
			if (strlen ($codeserie)<7) {
				erreurMsg (Tr ('Le code année-série %0 semble incorrect', 'Code "year-range" %0 seems improper'), $codeserie);
				$Xvars['idetablissement'] = $idetablissement;
				$Xvars['f_creer_serie'] = TRUE;		// => relancer le formulaire nouvelle série
				$Xvars['f_serie'] = TRUE;					// maintenir le 2e écran, saisie du code série
				break;
			}
			
		$sql_code_serie = NormSQL ($codeserie);
		$sql_com_serie = NormSQL ($comserie);
		
		$requete=	"INSERT INTO Series SET idetablissement=$idetablissement, codeserie='$codeserie', comserie='$comserie'";
		$result = requete ($requete);
		if (!$result) erreurMsg ("Pas d'inscription dans Series"); 	
		else {
			// Réafficher la table des séries pour contrôle et sélection
			$Xvars['idetablissement'] = $idetablissement;
			$Xvars['f_serie'] = TRUE;		// => on affiche le 2e écran, saisie du code série
			break;
		}
		
		
	// Retour avec annulation de création d'une nouvelle série ##################################################
		case 'v_annuler' :
			$Xvars['f_etabl']	= TRUE;			// Retour à affichage du premier écran, saisie du numéro d'établissement
			break;
		
		
	// Retour après saisie du code série #####################################################################
	case 'v_serie' : 		
		// Recherche du prochain numéro
		// Sélectionner le dernier numéro de la série
		$result = requete (
			"SELECT nrinv FROM Collections
				WHERE idetablissement=$idetablissement AND nrinv LIKE '".$codeserie."%'
				ORDER BY nrinv DESC LIMIT 1");
		$ligne = mysqli_fetch_assoc ($result);
		$nrinv = $ligne['nrinv'];
		mysqli_free_result ($result);
	
		// cas d'une nouvelle série : réponse nulle 
		if (!$nrinv) $nrinv = $codeserie.'001';
		
		else {
			$tn = explode ("-", $ligne['nrinv']);

			// ne garder que les chiffres en tête du dernier numéro (hors indices lettres)
			$n = preg_split("#\D#", $tn[2], -1);
			// Incrémenter et formater sur 3 caractères, composer le nouveau numéro
			$n = (string)$n[0]+1;
			$n = substr ('00'.$n, -3, 3);
			$nrinv = $tn[0].'-'.$tn[1].'-'.$n;
		}
	
		// Demander la création de la fiche
		$f_create = TRUE;				// =>  on lance les écritures
		break;		
	}

  
# Si le numéro est complet : Insérer dans la base #########################################################
############################################################################################################

	if ($f_create) {
			
### Première écriture en table Collections pour réserver le Nr inventaire
		// Chercher les domaines assignés par défaut à l'etablissement choisi
		$array = array ('m'=>'Machines', 'd'=>'Documents', 'l'=>'Logiciels');
		$iddomdef = 'iddomdef_'.array_search ($table, $array);

		if ($result = requete (" SELECT $iddomdef FROM Etablissements WHERE idetablissement = $idetablissement")) {
			$ligne = mysqli_fetch_assoc ($result);
			if ($ligne[$iddomdef] > 0) $iddomaine = $ligne[$iddomdef];
			else $iddomaine = $tstruct['iddomaine_d'];
		}

		$result = requete ("INSERT INTO Collections SET type='$type', nrinv='$nrinv', idetablissement=$idetablissement, 
												iddomaine=$iddomaine, nomrapcreat='$nomrapcreat', datecreat=$datecreat");

		if (!$result) erreurMsg ("Pas d'inscription dans Collections"); 		
		else {
			// Récupérer l'index
			$idcollection = mysqli_insert_id ($dblink);				
			
### Maintenant qu'on a vérifié qu'on peut créer la fiche 
			// on crée un enregistrement en table Machines, Documents, Logiciels
			// mais il faut bien écrire quelque chose dedans !
			$req = ($table == 'Machines') ? "nom=''" : (($table == 'Documents') ? "titredoc=''" : "titrelog=''");
			if (!($result = requete ($r = "INSERT INTO $table SET $req"))) 
				erreurMsg ("Pas d'inscription dans %0", $table); 	
			else {	
				// Récupérer l'index de cette table
				$idtable = mysqli_insert_id ($dblink);


				// Et on reporte dans la fiche Collections 
				$id_table = NomId ($table);
				$result = requete ("UPDATE Collections SET $id_table = $idtable WHERE idcollection = $idcollection");

### ON APPELLE L'ÉCRAN MODIFICATIONS :
				$menu = $_REQUEST['menu'] = 'M_objet';
				$_REQUEST['idcollection'] = $idcollection;
				$_REQUEST['Modif'] = 1;

				include ('consulter.php'); // >>>>>>>>>>>>>>>>>>>>>> Passer à l'écran MODIFIER objet
				debug (255, 'RETOUR >>>>>>'); // Pour piéger un retour intempestif
				exit ();											// ... par sécurité
			}
		}
	}

###  AFFICHAGES ############################################################################################
############################################################################################################

### Charger le tableau de contexte
	$Xvars['table'] = $table;
	$Xvars['type'] = $type;
	
### Afficher
$liste_xml = Xopen ('./XML_modeles/creer.xml') ;
Xpose ($liste_xml);

#################################### Fin de traitement
Fin ();
?>