<?php
# v15				130220	PhD		Ajout XML_bouclexml
# v15.3			160729	PhD		Reprise fonction FormatZip pour nouveau test de validité, nouvelle fonction Valid_xml
# v25				190615	PhD		Fonction 'Format_Zip devient 'Format_PATSTEC', ajout nouveau 'Format_ZIP' et 'Format_file'
# v25.1			190726	PhD		Réécriture de 'Format_ZIP' et 'Format_PATSTEC' en utilisant la classe ZipArchive
#														->le module 'crerzip.php' disparait
# v25.11.01	211021	PhD		Ajout des extensions zip et xml
###

#================================================================ Date_clair ===
function Date_clair($tname) {
  $t = substr ($tname,-12,8);    
  $annee=(int)substr($t,0,4);
  $mois=(int)substr($t,4,2);
  $jour=(int)substr($t,6,2);
  return $jour.'/'.$mois.'/'.$annee;
}

#=============================================================== Format_file ===
function Format_file () {       
  global $dir_tempo, $dbase, $tname, $file_ok, $ext; 
 
  // nom de fichier
  $tname = $dbase['nompublic'].'_' . date('Ymd_B');
  
# Test de validité du XML généré
	if (!Valid_xml ($xml = ob_get_clean ())) return;  // >>>>>>>>>>>>>> Sortie si code incorrect

# Créer le répertoire temporaire si non existant
	if (!is_dir($dir_tempo)) mkdir ($dir_tempo);

# Écriture du fichier temporaire
  $Zf = fopen($dir_tempo.$tname.'.xml','w');
  fwrite($Zf,$xml);
  fclose ($Zf);
   
  $file_ok = true;
  $ext = '.xml';
  return;
      
}  



#=============================================================== Format_PATSTEC ===
function Format_PATSTEC () {       
    global $fichierListe, $objetListe, $dir_export, $dbase, $kfiche, $tname, $file_ok; 
  
  // nom de fichier
  $tname = $dbase['nompublic'].'_' . date('Ymd_B');
  
# Test de validité du XML généré
	if (!Valid_xml ($xml = ob_get_clean ())) return;  // >>>>>>>>>>>>>> Sortie si code incorrect

# Ouverture du package ZIP
	$zip = new ZipArchive();
	if ($zip->open($dir_export.'T_'.$tname.'.zip', ZipArchive::CREATE)!==TRUE) {
 		ErreurMsg( Tr("Impossible de créer le dossier ZIP", 'Unable to create ZIP package'));
		}
	// Dossier interne
	$zip -> addEmptyDir($tname);
	
  // Ajout du ficheir XML
	$zip->addFromString($tname.'/'.$tname.'xml', $xml);
    
	// Ajout des fichiers médias
  if (isset ($fichierListe))
    foreach ($fichierListe as $name => $path)             
			$zip->addFile($path, $tname.'/'.$name);
            
  // Ajout du fichier "liste_envoi"
  if (isset ($objetListe) && sizeof ($objetListe)) {          

    $html = liste_envoi($objetListe, $tname);
		$zip->addFromString('/L_'.$tname.'.htm', $html);

		// Fermeture du fichier ZIP
		$zip->close();

# Écriture du fichier de validation
		$html = validation_envoi($objetListe, $tname);
		$Zf = fopen($dir_export.$tname.'.htm','w');
		fwrite($Zf,$html);
		fclose ($Zf);
   
# Marquage "attente" des objets en base de données
		foreach ($objetListe as $o) {
			 $o = explode (' : ', $o);
			 requete ("UPDATE Collections SET idetatfiche=".$kfiche['attente']." WHERE idcollection=$o[0]");     
		}

		$file_ok = true;
		return;
  }
   
  ErreurMsg ('Aucun objet trouvé');
}  


#=============================================================== Format_ZIP ===
function Format_ZIP () {       
  global $fichierListe, $objetListe, $dir_tempo, $dbase, $tname, $file_ok, $ext; 

  
  // nom de fichier
  $tname = $dbase['nompublic'].'_' . date('Ymd_B');
  
# Test de validité du XML généré
	if (!Valid_xml ($xml = ob_get_clean ())) return;  // >>>>>>>>>>>>>> Sortie si code incorrect

# Créer le répertoire temporaire si non existant
	if (!is_dir($dir_tempo)) mkdir ($dir_tempo);

# Ouverture du package ZIP
	$zip = new ZipArchive();
	if ($zip->open($dir_tempo.$tname.'.zip', ZipArchive::CREATE)!==TRUE) {
 		ErreurMsg( Tr("Impossible de créer le dossier ZIP", 'Unable to create ZIP package'));
		}
    
  // Ajout du fichier XML
	$zip->addFromString($tname.'.xml', $xml);
  
	// Ajout des fichiers médias
  if (isset ($fichierListe))
    foreach ($fichierListe as $name => $path)             
			$zip->addFile($path, $name);
         
# Controle final
  if (isset ($objetListe) && sizeof ($objetListe)) {          

	//Fermeture du fichier ZIP
		$zip->close();

    $file_ok = true;
    $ext = '.zip';
    return;
    
	} else  ErreurMsg ('Aucun objet trouvé');
}  


#=============================================================== Liste_envoi ===
function Liste_envoi($objet, $tname) {
#	Création du code d'un page HTML permettant la validation du transfert, avant enregistrement dans un fichier.htm
# $objet :	tableau des objets transférés
# $tname :	nom de base des fichiers créés
###
	global $Xvars;
  ob_start ();					// bufferisation
   
	$Xvars['date_clair'] = date_clair($tname);
	$Xvars['objet'] = $objet;
	$Xvars['tname'] = $tname;

	$liste_xml = Xopen ('./XML_modeles/transfertxml_liste.xml') ;
	Xpose ($liste_xml);

  return ob_get_clean();
}


#========================================================== Validation_envoi ===
function Validation_envoi ($objet, $tname) {
#	Création du code d'un page HTML permettant la validation du transfert, avant enregistrement dans un fichier.htm
# $objet :	tableau des objets transférés
# $tname :	nom de base des fichiers créés
###
	global $Xvars;
  ob_start ();					// bufferisation
   
	$Xvars['date_clair'] = date_clair($tname);
	$Xvars['objet'] = $objet;
	$Xvars['tname'] = $tname;

	$liste_xml = Xopen ('./XML_modeles/transfertxml_valid.xml') ;
	Xpose ($liste_xml);
  return ob_get_clean();
}

#================================================================= Valid_xml ===
function Valid_xml ($xml) {
# Test de validité du XML généré

	global $dir_tempo;
	libxml_use_internal_errors(true);
	if (false === ($simplexml = @simplexml_load_string($xml))) {
		require_once ('inc_tete.php');  // Ouverture retardée de la page HTML 
		Debut ();
		echo "<h2 style='text-align:center; color:red;'>"
				.Tr('Erreurs dans le XML généré','Errors in XML object'). "</h2>";
		foreach(libxml_get_errors() as $error) {
			echo "<p> $error->message </p>";
		}
			//Créer le répertoire temporaire si non existant
			if (!is_dir($dir_tempo)) mkdir ($dir_tempo);
			
			$fname = $dir_tempo.'code_' . date('Ymd_B').'xml';
      $Zf = fopen($fname,'w');
      fwrite($Zf,$xml);
      fclose ($Zf);
		echo "<p style='font-weight:bold; color:red;'>"
				.Tr ('Le code XML incorrect est enregistré dans ', 'XML wrong code is recorded in ')
				.$fname." </p>";
		return false;
  }
  
  return true;
}

#============================================================= XML_bouclexml ===
function XML_bouclexml ($loop, $attr, $Xaction) {
# Appelle les fichiers du répertoire 
# mentionné dans $dir qui ont l'extension '.xml'     

	global $Xvars;
	$dir = "XML_transfert/";

	if ($loop === null) return;		// tag de fin	

	if ($loop == 0) {							// Premier appel, lire la table
   	$dh = opendir($dir);
		while (($file = readdir($dh)) !== false) {
			if (strtolower (substr ($file, -4)) == '.xml')  //  XML ou xml ou....
            $tab_fxml[]= $file;
      }
			$Xvars['tab_fxml'] =  $tab_fxml;
      closedir($dh);
  }
	
	// Puis sortir un élément à chaque tour
	if ($loop < count ($Xvars['tab_fxml'])) {
	$file = $Xvars['tab_fxml'][$loop];
	
  $Xvars['xmlfile'] = $dir . $file;
  $Xvars['xmlname'] = substr($file, 0, strlen ($file)-4);   // Supprime .XML dans l'affichage
  return 'ACT,LOOP';
		    
	} else return  'EXIT' ;
}

#=============================================================== XML_liste_obj ===
function XML_liste_obj ($loop, $attr, $Xaction) {
# Boucle sur les objets     

	global $Xvars;

	if ($loop === null) return;		// tag de fin	

	if ($loop <count ($Xvars['objet'])) {			
		$Xvars['o'] = $Xvars['objet'][$loop];
	  return 'ACT,LOOP';
		    
	} else return  'EXIT' ;
}
	
?>