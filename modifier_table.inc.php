<?php
# v10.7		130514	PhD		Transféré ici "Couleur"
# v10.8		130801	PhD		Replacé ancienne fonction Input allégée,  ajouté style table
#													Ajouté condition sur effacement élément
# v12.0			140224	PhD		Déplacé fonction Ligne dans globalfcts
# v12.8			141220	PhD		Afficher la table triée par id
# v13.0			150708	PhD		Introduction Mysqli
# v14.1			151003	PhD		Reprise des traductions
# v14.2			160102	PhD		Réécrit pour interface Xpose, suprimé fonct 'couleur'
# v15				160111	PhD		Ajout param Xvars dans interface fonctions XML_
# v19				170903	PhD		Cas particulier pour table Verrous 
# v24.1			190320	PhD		Réécriture Ajouter_elt et Modifier_elt pour limiter chgt senior_admin
# v25				190615	PhD		Interdire modification Version base
# v25.1			190717	PhD		Corrigé MYSQLI_NUM
# V25.4			191206	PHD		Corrigé test affectation 'senior_admin'
# v25.10		210130	PhD		Remplacé SQL PASSWORD par PHP Password_sha1, modifié Ajouter_elt (auto incrément avec MySQL8)
# v25.12.01	220208	PhD		Corrigé test création senior_admin
# v25.14.01	220804	PhD		Sumplifié fonction Modifier_elt : traitement password n'avait pas été mis à jour
###
#
# ATTENTION : Ces fonctions sont générales pour toutes les tables...
# SAUF QUE les champs de nom 'passe', 'statut', 'parametre' ont un traitement spécial, sans que la table soit précisée
#
####################################################################################################### Ajouter_elt ###

function Ajouter_elt ($table) {
      // Ajoute un élément dans la table $table en affichant un formulaire
      // Si un des champs a pour nom 'passe' (pour les rapporteurs),
      // elle utilise la fonction password pour crypter le mot de passe et masque 
      // ce que tape l'utilisateur

	$query = "INSERT INTO $table VALUES (";
	$colonnes = colonnes ($table);

	for ($i=0; $i < count ($colonnes);$i++) {
		$new =  NormIN($colonnes[$i]) ;
		
		switch ($colonnes[$i]) {		
			case 'passe' :
				$password = Password_sha1 ($new);
				$query .= "'".$password."'";
				break;
			
			case 'statut' :
				if ($new=='senior_admin' AND $_SESSION['statut']!='senior_admin') {
					$new = 'administrateur';
					erreurMsg ("Seul un 'senior-admin' peut déclarer un 'senior-admin'...");
				}
        $query .= "'" .$new  . "'";
				break;
				
			default :						// cas général
				// MySQL8 impose une valeur 0 ou NULL pour un indice auto-incrémenté
				$sqlval = (($new == '') AND (substr ($colonnes[$i], 0, 2) == 'id')) ? 0 : NormSQL($new);
        $query .= "'" . $sqlval  . "'";
		}
    if ($i != count ($colonnes) - 1) $query .= ", ";
	}
	
	$query .= ")";

	$result = requete ($query);
	$nomid = nomid ($table);
	$result = requete ("ALTER TABLE $table ORDER BY $nomid");
	if ($result) Message ("Modification réussie");
}


####################################################################################################### Modifier_elt ###

function Modifier_elt ($table, $id) {
# Gère la modification d'un élement, sans autoriser le chgt d'indice !
# Si un des champs a pour nom 'passe' (Table "Rapporteur"")
# 	on utilise la fonction password pour crypter le mot de passe
# Si un des champs a pour nom 'statut' 
#  on vérifie que seul un senior-admin a le droit de spécifier un senior-admin
# Note : la première ligne de la table Parametres est modifié uniquement par PHPmyAdmin

	// Le formulaire a déjà été rempli
	$colonnes = Colonnes ($table);
	$nomid = Nomid ($table);

# Ne pas modifier la ligne 1 de la table Paramètres
	if ($table=='Parametres' AND $id==1) {
		erreurMsg ("La version de la base est modifiable uniquement par PHPmyAdmin...");
		return;
	}
	
	$set = '';

	for ($i = 1; $i < count ($colonnes);$i++) {	// On saute la colonne indice id...
		$new =  NormIN($colonnes[$i]) ;
		
		switch ($colonnes[$i]) {		
			case 'passe' :
				if ($new)	{
					$password = Password_sha1 ($new);
					$set .= " $colonnes[$i] = '".$password."' ,";			// passe n'est pas ré-écrit si inchangé		
				}	
				break;
			
			case 'statut' :
				if ($new=='senior_admin' AND $_SESSION['statut']!='senior_admin') {
					$new = 'administrateur';
					erreurMsg ("Seul un 'senior_admin' peut déclarer un 'senior_admin'...");
				}
				$set .= " $colonnes[$i] = '$new' ,";
				break;
				
			default :						// cas général
				$set .= " $colonnes[$i] = '" . NormSQL ($new) . "' ,";
		}
	}
	$set = substr($set, 0, -1);		// éliminer la dernière virgule

	if ($result = Requete ("UPDATE $table SET $set WHERE $nomid = '$id'"))
		Message ("Modification réussie");
	$result = Requete ("ALTER TABLE $table ORDER BY $nomid");

}

################################################################################################## Nbenregistrements ###

function Nbenregistrements ($table, $id, $tables) {
   // renvoie le nombre d'enregistrements ayant un champ lié à la table $table
   // de même valeur que l'identifiant $id dans toute les $tables

   $nomid = nomid ($table);
   
   $from = '';
   $where = '';
   for ($i = 0; $i < count ($tables); $i++) {
      $from .= ", $tables[$i] ";
      $where .= " OR $tables[$i].$nomid = '$id'";
   }
   
   $result = Requete ("SELECT count(*) as nb FROM " . substr ($from, 2) .
      'WHERE' . substr ($where, 3));
   $ligne = mysqli_fetch_assoc ($result);
   return $ligne['nb'];
}


####################################################################################################### XML_colonnes ###  
function XML_colonnes ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Détection de la balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>

	global $Xvars;
	if ($loop == 0) $Xvars['colonnes'] = Colonnes ($Xvars['table']);

	if ($loop < count ($Xvars['colonnes'])) {
		$Xvars['colonne'] = $Xvars['colonnes'][$loop];
		return 'ACT,LOOP';
	} else
		return  'EXIT';
}


######################################################################################################## XML_element ###  
function XML_elements ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Détection de la balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>

	global $Xvars;
	if ($loop < count ($Xvars['ligne'])) {
		$Xvars['val'] = $Xvars['ligne'][$loop];
		return 'ACT,LOOP';
	} else
		return  'EXIT';
}


##################################################################################################### XML_item_enum2 ###
### Cette fonction est dérivée mais différente de la fonction présente dans consulter.inc
function XML_item_enum2 ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag final : </item>
	
	global $Xvars;
	static $t_enum;
	$item = $attr['item'];

	// tag de début, mettre en tableau la liste des enum
	if ($loop === 0) 
		$t_enum = explode (',', $Xvars['tstruct'][$item]['Length']);

	//  Appel de la valeur courante
	if ($loop < count ($t_enum)) {
		$Xvars['item_val'] = $t_enum[$loop];
	
		// Préparer la pré-sélection
		if (isset ($Xvars['ligne'])) $Xvars['item_select'] = $Xvars['item_val'] == $Xvars['ligne'][$item];
		else $Xvars['item_select'] = '';
			
		
		return 'ACT,LOOP';
	} else return 'EXIT' ;
} 

####################################################################################################### XML_item_tab ###  
function XML_item_tab ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Détection de la balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>

	global $Xvars;
	$table = $Xvars['table'];
	
	if ($loop == 0 ) {
    // Création de la table de structure améliorée
		$tstruct = Tablestruct ($table);	 
		
		// Le premier élément (index) est uniquement affiché -> mettre un type spécial
		$keys = array_keys($tstruct);
		$tstruct[$keys[0]]['Type'] = 'nomodif';
		
		$Xvars['tstruct'] = $tstruct;
    $Xvars['items'] = array_column($tstruct, 'Field');
		$Xvars['nbitem'] = count ($tstruct);
	}	
	
	// Pour chaque item, afficher un champ	
	  $Xvars['item'] = array_shift ($Xvars['items']);

	return ($loop >= $Xvars['nbitem']) ? 'EXIT' : 'ACT,LOOP' ;
}


######################################################################################################### XML_lignes ###  
function XML_lignes ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Détection de la balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>

	global $Xvars;
	if ($loop == 0 ) {
		$idtable = $Xvars['idtable'];
		$table = $Xvars['table'];
		
		// Préparer les bornes du bloc, si demandé
		if ($Xvars['f_bloc']) 
			$conditions = "WHERE $idtable >= ".$Xvars['borne1']." ORDER BY $idtable LIMIT ".$Xvars['pas'];
		else 
			$conditions ="ORDER BY $idtable";
			
		// cas particulier...
		if ($table == 'Verrous') $conditions = "";
	
		$Xvars['SQL_lignes'] = Requete ("SELECT * FROM $table $conditions");
	}
	
	// Alternance des couleurs de ligne
	$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';

	if ($Xvars['ligne'] = mysqli_fetch_array ($Xvars['SQL_lignes'], MYSQLI_NUM))
		return 'ACT,LOOP';
	else
		return  'EXIT';
}

?>