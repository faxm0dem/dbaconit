<?php
# v10.5			121229	PhD		Chgt interface fonction buffer_export
# v12.8.03	150323	PhD		Chargé globalfacts, Nomid() nécessaire pour tri depuis v12.8 
# v12.8.04	150408	PhD		Supprimé deuxième sauvegarde, inutile sous OVH
# v13.0			150707	PhD	 Introduction Mysqli
# v13.0.01	150717	PhD 	corrigé message erreur connexion
# v14.02		151214	PhD		Commentaires
# v15				160224	PhD		Commentaires, corrigé erreur depuis v13.0 : link devient dblink
# v15.2			160715	PhD		Réorganisé pour fonctionner sans CRON	
# v18				170725	PhD		Corrections msg d'erreur
# v25				190520	PhD		importexport devient sauv_sql, dir_save contient slash final
###

#################################################################################
# Ce module assure les sauvegardes automatiques du contenu de la base de données.
# Il était prévu pour être appelé par un timer du type CRON.
# Il a été modifié (v15.2) pour être déclenché lors du login d'un rédacteur ou d'un administrateur.
# Il teste si la sauvegarde a déjà été faite ce même jour pour ne pas répéter.
#
# Il n'a pas de paramètre en entrée (la base à sauvegarder est déjà connectée)
# La sauvegarde est assurée dans le dossier désigné par $dir_save
#################################################################################


##   La sauvegarde journalière est-elle déjà faite ?
####################################################
	// Definir le nom complet du fichier
$file =  date('Ymd_').$dbase['nom'].'.sql';
$filename =  $dir_save.$file;

	// Sauter la sauvegarde si un fichier de ce nom existe
if (!is_file($filename)) {

	## Sinon créer le fichier
	####################################################
	echo "<p class=message>".Tr ('Sauvegarde journalière, patientez', 'Daily backup, please wait')."</p>\n";
	flush ();
	
	@set_time_limit (10000); // On augmente le timeout
	require_once ('sauv_sql.inc.php');	// Fonctions de création du buffer texte

	$f = fopen ( $filename, "w");
	if (!$f) erreurMsg ("*** Refus ouverture fichier %0 ***", $filename);

	##  Export 
	####################################################
	$num_tables = buffer_export ('');
		if ($num_tables == 0) erreurMsg ("Aucune table n'a été trouvée...");

	// Le résultat dump_buffer est disponible en global
	fwrite ($f,$dump_buffer);
	fclose($f);

	echo "<p class=message>".Tr ("Sauvegarde terminée", "Backup done")."</p>\n";
}
?>

