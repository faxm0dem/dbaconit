<?php
# V5.02			20050623  DU/HBP  Premier implementation du transfert XML
# V8.3  	 	111101	PhD		Corrections normes W3C
# V8.4			111122	PhD		Le dossier /XML devient /XML_transfert
# v10.3			120626	PhD		Ajout du fichier transfertxml.inc
# v10.4			121107	PhD		Ajout codage accent sur Prenom
# v10.8			130804	PhD		Corrigé position tableau
# v11.4			131223	PhD		Modification pour export table par table
# v12.4.02	140504	PhD		Déplacé ici lien rapide vers Transferts en cours
# v14.1			151003	PhD	 Reprise des traductions
# v14.2			160103	PhD		Nouvel interface Debut, affichage par Xpose, appel retardé de inc_tete.php
# v15				160219	PhD		Validation HTML5
# v15.3			160729	PhD		Remplacement du moteur XML de Hans par moteur Xpose, nouveau traitement validation XML
#														Suppression du mode HTML
# v15.4			160913	PhD		Rebaptisé f_modexml->$f_non_head_html
# v25				190612	PhD		Utilisation de liste idcollections préselectionnée, et de l'attribut "format"
# v25.8			200410	PhD		Ajout Collections devant idetatfiche, ajout custom_css
# v25.11.01	211021	PhD		Ajout des extensions zip et xml
###

$f_non_head_html = TRUE; 							// Ceci va bloquer l'ouverture de la page HTML dans init.inc
require_once ('init.inc.php');
require_once ('consulter.inc.php');
require_once ('xml_inc.php');
require_once ('transfertxml.inc.php');
   
#****************************************************************************
#****                        Programme principal                         ****
#****************************************************************************

/* Protection des entrées -------------------------------------------------------
'action'			- POST non accessible - uniquement testé isset
'choix'				- POST non accessible (radio) - uniquement testé sur valeurs connues
'format'			- POST non accessible (radio) - uniquement testé sur valeurs connues
'requete'			* POST  - Filtré NormIN
'xml_file'		- POST non accessible (select) - filtré NormIN
------------------------------------------------------------------------------ */


## Traitement des entrées :
###########################

	$f_action = isset ($_POST['action']);
	$choix = @$_POST['choix'];
	$formatsortie = @$_POST['format'];
	$requete = NormIN ('requete');
	$xml_file = NormIN ('xml_file');

###########################
	
	set_time_limit (60); // Maximum temps de traitement : 60 secondes
	$traitement = NULL;

if ($f_action) {

  // Vérifier que le fichier XML est sélectionné et qu'il existe.
  if ( !$xml_file OR !file_exists($xml_file)) {
  	$f_erreur = TRUE;
		erreurMsg ('Fichier XML non sélectionné');
	} else {
		// Vérifier l'existence du fichier PHP associé (sinon erreur grave - arrêt)
		if (! file_exists ($fn = substr($xml_file, 0, strlen($xml_file)-3).'php')) exit ('Fichier PHP absent');
		require ($fn);  
		
	// Préparer la requête WHERE de recherche en base
	
		switch ($choix[0]) {
			case ('et5') :		// transfert PATSTEC standard
				$requete = 'Collections.idetatfiche=5';
				break;
				
			case ('req') : 		// la requete est déjà transmise par $requete
				break;
				
			case ('fiche') :	// fiche courante
				$requete = 'Collections.idcollection='.$_SESSION['idcollection'];
				break;
				
			case ('list') :		// cas d'une liste à mettre en forme
				$requete ="(";
				foreach ($_SESSION['idcollections'] as $idcollection){ 
					$requete .= "Collections.idcollection=$idcollection OR ";
				}	
				$requete = rtrim ($requete, 'OR ').")";
				break;
		}
		
		// Dans tous les cas, on sort les donnees XML dans un 
		// tampon, ce qui permettra à la fonction de format de faire les traitements complémentaires
		ob_start ();

#--- 	Préparer le premier tableau de variables et lancer le traitement  

		$Xvars['DBAconit'] = $DBAconit;
		$Xvars['dbase'] =	$dbase;
		$Xvars['xml_file'] = $xml_file;
		$Xvars['dir_export'] = $dir_export;
		// Nom, la date et l'heure du fichier PHP chargé.
		$Xvars['phpfile'] = $fn ." @ ".date ('Y.m.d H:i', filemtime($fn));       

		// Déclare le format XML
		echo "<?xml version='1.0' encoding='utf-8' standalone='yes' ?>\n";  

		// Vérification, puis traitement du fichier modèle XML
		$liste_xml = Xopen ($xml_file) ;
			// au passage, noter le format de sortie, si ce n'est pas simple affichage XML
			if ($formatsortie != 'AFFICH') $formatsortie = $liste_xml->attributes()->format;
		Xpose ($liste_xml);

#--- À la fin du traitement, on fait le nécesaire pour terminer le travail
		switch ($formatsortie) {

		case 'AFFICH':
			// Dans le cas XML, on vérifie le code avant l'affichage écran par le navigateur.
			// De cette façon, on garde la maîtrise de l'affichage des erreurs et on stocke le code pour examen.
			$xml = ob_get_clean ();
			if (Valid_xml ($xml)) {
				header("content-type: text/XML");
				echo $xml;
				exit;		// >>>>>>>>>>>>>>>>>
				
			} else break;
	
		default:
			// Dans tout autre cas, il faut une fontion de nom "format_" suivi par
			// le nom du format. Si elle existe, l'appeler, si non afficher un message
			// d'erreur pour l'utilisateur
			if (function_exists ('format_'.$formatsortie))
				 call_user_func ('format_'.$formatsortie);
			else {     
				 erreurMsg ("Transfert format %0 inconnu", $formatsortie );
			}
			break;
		}
	}
}

###############################################################
# Affichage du formulaire de commande
###############################################################
global $erreur;

$custom_css = "transfertxml.css";
require_once ('inc_tete.php');  // Ouverture retardée de la page HTML
	Debut ();
   
	 	//Pour afficher le Nrinventaire de la fiche en cours :
	 	$Xvars['nrinv'] = (isset ($_SESSION['idcollection'])) ?
	 		nrinv ('Collections', $_SESSION['idcollection'], FALSE) : "";
	 	 	
	 	$Xvars['dir_export'] = $dir_export;
	 	$Xvars['dir_tempo'] = $dir_tempo;
	 	$Xvars['formatsortie'] = $formatsortie;
 	
	 	// Si la préparation d'un transfert a réussi : l'afficher
		if (isset ($file_ok)) {
			$Xvars['file_ok'] = $file_ok;
			$Xvars ['objet'] = $objetListe;
			$Xvars['tname'] = $tname;
			$Xvars['ext'] = $ext;
		}
		
		$liste_xml = Xopen ('./XML_modeles/transfertxml.xml') ;
		Xpose ($liste_xml);
//}
?>
