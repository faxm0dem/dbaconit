<?php
# v10.7		130308	PhD		Remplacé Design_complete par Design_titre
#					130615	PhD		Transféré ici Tablestruct, Selection, ValParDefaut
# v10.8		130805	PhD		Transféré Tablestruct dans golbalfcts...
# v13.0		150707	PhD		Introduction Mysqli
# v14.1		151012	PhD		Sorti la partie affichage dans aff_liens.php; ajouté les fonctions xmlEncoding
# v14.2		151230	PhD		Chgt nom XML_selection -> XML_select_famille, supprimé fonction Légende, modifié valpardefaut
# v15			160111	PhD		Ajout param Xvars dans interface fonctions XML_
# v25			190615	PhD		Supprimé inst debug 8, ajouté préselection dans XML_prefinv2
###

#############################################################= valpardefaut ### 

function valpardefaut ($table, $champ) {
// Cette fonction renvoie une valeur contenant le paramètre par défaut d'un champ de la table

  $result = requete (" SHOW COLUMNS FROM $table ");
	while ($ligne = mysqli_fetch_assoc ($result)) {
		if ($ligne['Field'] == $champ) $defaut = $ligne['Default'];
	}
	  
  return $defaut;
}

############################################################ XML_liste_liens ###
function XML_liste_liens ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	static $SQLresult_liste;
	
	// tag de début, lire la table
	if ($loop === 0) {
		$idcollection = $Xvars['idcollection'];

  	$SQLresult_liste = requete ("
    		SELECT idlien, famille, commentlien, idcol1, idcol2
        FROM Familles, Liens 
        WHERE Familles.idfamille = Liens.idfamille
        AND ( idcol1 = $idcollection
        OR idcol2 = $idcollection )
        ");
  }
  
	//  Appel des valeurs courantes
	$ligne = mysqli_fetch_assoc ($SQLresult_liste);
	
	// Préparer les variables
  $Xvars['nrinv1'] = nrinv ('Collections', $ligne['idcol1']);
  $Xvars['nrinv2'] = nrinv ('Collections' ,$ligne['idcol2']);
  $Xvars['idlien'] = $ligne['idlien'];
	$Xvars['commentlien'] = $ligne['commentlien'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

################################################################# XML_prefinv2 ###  

function XML_prefinv2 ($loop, $attr, $Xaction) {
	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	
	// tag de début, lire la table
	if ($loop === 0) {
  	$Xvars['SQL_prefinv'] = requete ("SELECT idetablissement, prefinv FROM Etablissements  ORDER BY prefinv");
  }
  
	//  Appel des valeurs courantes
	$ligne = mysqli_fetch_assoc ($Xvars['SQL_prefinv']);
	
	// Préparer les variables
  $Xvars['idetablissement'] = $ligne['idetablissement'];
  $Xvars['prefinv'] = $ligne['prefinv'];
  // Choisir la ligne présélectionnée
  $Xvars['fsel'] = $Xvars['idetab1'] == $Xvars['idetablissement'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

################################################################# XML_select_famille ###  

function XML_select_famille ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	static $SQLresult_fam;
	
	// tag de début, lire la table
	if ($loop === 0) {
		$idcollection = $Xvars['idcollection'];

  	$SQLresult_fam = requete ("SELECT * FROM Familles  ORDER BY famille");
  }
  
	//  Appel des valeurs courantes
	$ligne = mysqli_fetch_assoc ($SQLresult_fam);
	
	// Préparer les variables
  $Xvars['idfamille'] = $ligne['idfamille'];
  $Xvars['famille'] = $ligne['famille'];
 	$Xvars['f_select'] = $ligne['idfamille'] == $Xvars['id_select'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

?>