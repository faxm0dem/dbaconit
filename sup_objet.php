<?php
# v17.4		170605	PhD		Création
# v18			170712	PhD		Ajout suppression des mouvemenst
# v25.7		200326	PhD		Autoriser la suppression en masse des objets sélectionnés
###

/* Protection des entrées -------------------------------------------------------
'valid_suppress'				- POST  - Testé uniquement isset
'idcollection'					- POST	- Filtré NormIN
------------------------------------------------------------------------------ */
################################################################### Id_next_sup ###
function Id_next_sup ($idcollection)
# Fournit l'idcollection  suivant dans la liste $_SESSION['idcollections'] si elle existe
# Fournit 0 si pas de liste, ou si plus d'élément disponible
####
{
### Si il y a une liste d'éléments sélectionnés
	if (isset ($_SESSION['idcollections'])) {
		$cid = count ($_SESSION['idcollections']);

		// trouver l'indice de l'élément courant dans la liste
		for ($i = 0; $i < $cid ; $i++) {
			if ($_SESSION['idcollections'][$i] == $idcollection) {			
				//  Incrémenter l'indice
				$nextid = $i + 1;
				// si l'élément suivant n'existe pas, on retourne la valeur 0
				if ($nextid < $cid) $idcollection = $_SESSION['idcollections'][$nextid];
				else $idcollection = 0;
				break;
			}
		}

### S'il n'y a pas de liste d'éléments sélectionnés, retourner 0
	} else  $idcollection = 0;

	return $idcollection;
}

################################################################### Sup_un_objet ###
function Sup_un_objet ($idcollection)
### Exécuter la suppression ###########################################
# Supprimer tous les liens de cet objet, puis l'enregistrement matériel/document/logiciel, 
# puis l'enregistrement principal collection.
# Il n'y a pas de contrôle suplémentaire d'autorisation de modification à ce niveau, 
# car la suppression n'est ouverte qu'aux senior-admin, qui peuvent de toutes façons s'accorder tous les droits.
###
# Noter que :
# - seuls les enregistrements mouvements sont supprimés si plus de liens
# - les médias peuvent être ré-utilisés (sinon voir filtrage dans audit)
# - personnes et organismes sont filtrés à l'aide des listes organismes et personnes
# - Matériaux et Mots clés sont des listes d'ordre stables...
###
{	
	global $dblink;

# Matériaux
	$r = requete ("DELETE FROM Col_Mate WHERE Col_Mate.idcollection = $idcollection ");
	if ($n = mysqli_affected_rows ($dblink)) 
		Message ("%0 liens matériaux supprimés", $n);

# Mots clés
  $r = requete ("DELETE FROM Col_Mot WHERE Col_Mot.idcollection = $idcollection ");
	if ($n = mysqli_affected_rows ($dblink)) Message ("%0 liens mots clés supprimés", $n);

# Organismes
  $r = requete ("DELETE FROM Col_Org WHERE Col_Org.idcollection = $idcollection ");
	if ($n = mysqli_affected_rows ($dblink)) Message ("%0 liens organismes supprimés", $n);

# Personnes
  $r = requete ("DELETE FROM Col_Per WHERE Col_Per.idcollection = $idcollection ");
	if ($n = mysqli_affected_rows ($dblink)) Message ("%0 liens personnes supprimés", $n);

# Médias 
	$r = requete ("DELETE FROM Col_Med WHERE Col_Med.idcollection = $idcollection ");
	if ($n = mysqli_affected_rows ($dblink)) {
		Message ("%0 liens médias supprimés", $n);
		Message ('<strong>'.Tr ("Rappel : les fichiers médias ne sont pas supprimés, utiliser l'audit 'médias orphelins'",
									"Notice: medias files are not deleted, check audit 'orphans médias'").'</strong>');
	}
	
# Mouvements 
	// Lister tous les mouvements connectés
  $rmouv = requete ("SELECT idmouvement FROM Col_Mouv WHERE Col_Mouv.idcollection = $idcollection ");
	// Pour chacun, chercher à combien d'objets il est lié
	while ($ligne = mysqli_fetch_assoc ($rmouv)) {
		$idmouvement = $ligne['idmouvement'];
		// S'il n' y a qu'un seul mouvement associé, il est supprimé d'autorité
		$rlien = requete ("SELECT idcollection FROM Col_Mouv WHERE Col_Mouv.idmouvement =". $idmouvement );
		if (mysqli_num_rows ($rlien) == 1) {
	  	$r = requete ("DELETE FROM Mouvements WHERE idmouvement =". $idmouvement);
			if (mysqli_affected_rows ($dblink)) 
				Message ("Enregistrement mouvement supprimé %0", $idmouvement);
 	 	}
 	}
	// Puis supprimer tous les liens correspondants à l'objet
  $r = requete ("DELETE FROM Col_Mouv WHERE Col_Mouv.idcollection = $idcollection ");
	if ($n = mysqli_affected_rows ($dblink)) Message ("%0 liens mouvements supprimés", $n);

# Liens entre objets
  $r = requete ("DELETE FROM Liens WHERE idcol1=$idcollection OR idcol2=$idcollection");
	if ($n = mysqli_affected_rows ($dblink)) Message ("%0 liens entre objets supprimés", $n);

# Localiser et supprimer l'enregistrement matériel/document/logiciel
  $SQLresult = requete 
  	("SELECT idmachine, iddocument, idlogiciel FROM Collections WHERE Collections.idcollection = $idcollection");
  $ligne = mysqli_fetch_assoc ($SQLresult);
  if ($ligne['idmachine']) {
  	$r = requete ("DELETE FROM Machines WHERE idmachine = ".$ligne['idmachine'] );
 	  if (mysqli_affected_rows ($dblink)) Message ("Fiche machine supprimée");
  }
  if ($ligne['iddocument']) {
		$r = requete ("DELETE FROM Documents WHERE iddocument = ".$ligne['iddocument'] );
 	  if (mysqli_affected_rows ($dblink)) Message ("Fiche document supprimée");
  }
  if ($ligne['idlogiciel']) {
		$r = requete ("DELETE FROM Logiciels WHERE idlogiciel = ".$ligne['idlogiciel'] );
 	  if (mysqli_affected_rows ($dblink)) Message ("Fiche logicielle supprimée");
  }

# Et enfin supprimer l'enregistrement principal dans Collections
	$r = requete ("DELETE FROM Collections WHERE idcollection = $idcollection ");
	if (mysqli_affected_rows ($dblink)) 
 	 	Message ("Fiche principale collection supprimée : [id] = ".$idcollection);
	
# Afficher le résultat
	AfficheMessages ();
}


###################################################################
###################################################################
# Début
###################################################################
require_once ('init.inc.php');

### Traitement des entrées :
###########################
# Ce module est appelé en 'include' par 'consulter.php', il dispose de la variable 'idcollection'

// En retour de l'écran de validation, 'idcollection' est obligatoirement présent 
$idcollection = NormIn ('idcollection', 'P');
// Lire le paramêtre autorisation de suppression par liste d'objets sélecionnés
$Xvars['f_liste'] = $f_liste = ($dbase['autor_suppress_liste'] == TRUE) & isset ($_SESSION['idcollections']);

### Traitement de la réponse à la demande de validation
#######################################################################################
if  (isset ($_POST['valid_suppress'])) { 
	
	//................................Suppression premier objet 
	Sup_un_objet ($idcollection);
	
	//................................ Suppression suite de la liste (si elle existe)
	if ($f_liste) {
		while ($idcollection = Id_next_sup ($idcollection)) :
			Sup_un_objet ($idcollection); 
		endwhile;
	} 


# ==> Affichage du deuxième écran 
#...................................................................................................

	//Reste à sélectionner l'objet suivant :	
 	$Xvars['appel'] = 'second';
 	$Xvars['idcollection'] = Id_next_sup ($idcollection);
 	// Si 'idcollection' valide, l'écran va proposer affichage de l'objet
 	// sinon, le retour se fera vers 'rechercher'
 	
	$liste_xml = Xopen ('./XML_modeles/sup_objet.xml') ;
	Xpose ($liste_xml);
	
} else {

# Affichage du premier écran 
####################################################################################################

 	$Xvars['idcollection'] = $idcollection;
 	$Xvars['designation'] = Design_titre ($idcollection);
 	$Xvars['appel'] = 'premier';

	// Afficher à partir du modèle XML	
	$liste_xml = Xopen ('./XML_modeles/sup_objet.xml') ;
	Xpose ($liste_xml); 
}

Fin ();
?>