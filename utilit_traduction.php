<?php
# V8.4		111122	PhD		Création à partir de l'ancienne fonction 'audit'  
# v14.2		151214	PhD		Nouvel interface Debut
# v15			160119	PhD		Validation HTML5
###


# Fonctions
#--------------------------------#

function makeTranslations () {
   $dir = opendir ('.');
   $translates = array ();
   $txt = '';
   while ($f = readdir ($dir)) {
      $fp = pathinfo ($f);
      if ($f == 'translations.php') continue;
      if (0 == strcasecmp (@$fp['extension'], 'php')) {
	$txt .= join ('', file ($f));
      }
   }
   
   $cnt = preg_match_all ('/(translate|message|erreurmsg) \((.+)\)/im'  , $txt, $matches);

   echo "<pre>\n";   
   $tstrings = array ();
   for ($i = 0; $i < count ($matches[2]) ; $i++) {
      $t = $matches[2][$i];
      $strch = substr ($t,0,1);
      if (($strch == '"') || ($strch == "'"))
         $tstrings[] = substr ($t, 0, strpos ($t, $strch, 1)+1);
      else	 
         echo '// ? ' . $matches[1][$i] . " $t\n";
   }
   
   echo "\n// Found " . count ($tstrings = array_unique($tstrings)) . " strings\n";
   
   include_once ("EN_formats.php");
   
   foreach ($tstrings as $s)
      if (isset ($Translations[trim ($s, '"\'')])) {
         echo "   $s =&gt; '" . $Translations[trim ($s, '"\'')] . "',\n";
	 unset ($Translations[trim ($s, '"\'')]);
      } else
         echo "// $s =&gt; '',\n";
	 
   if (count (@$Translations)) {
      echo "\n\n// ".count ($Translations)." strings left in translation table :\n";	   
      foreach ($Translations as $k => $v)
         echo "   \"$k\" =&gt; ". ((false !== strpos ($v, '\'')) ? "\"$v\"" : "'$v'"). ",\n";
   }
   
   echo "</pre>\n";   

}
###############################################################################

require_once ('init.inc.php');

Debut();   
	makeTranslations ();
Fin ();						

?>
