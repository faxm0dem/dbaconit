<?php
# v25			190325	PhD		Autorisé ajouts dans table Parametres
# v25.5		191226	PhD		Ajout taille_bloc
# v25.8			200411	PhD		Ajout custom_css
###

/* Protection des entrées -------------------------------------------------------
'action'	- POST  - Filtré NormIN  - uniquement testé switch
'borne1'	- POST  - Filtré numérique
'id'			* REQ href	- filtré numérique
'pas'			- POST  - Filtré numérique
'pas_ar'	- POST  - Filtré NormIN  - uniquement testé switch
'pas_av'	- POST  - Filtré NormIN  - uniquement testé switch
'table'		* REQ appelé par menu - filtré NormIN
(champs)	* POST - tous les champs sont filtrés NormIN (sauf password codé immédiatement)
------------------------------------------------------------------------------ */


$custom_css = "modifier_table.css";
require_once ('init.inc.php');
require_once ('consulter.inc.php');
require_once ('modifier_table.inc.php');

### Traitement des entrées ###########################

$table = NormIN ('table', 'R');
if (!array_search ($table, $T_ordre))  DIE ("Nom de table non admis !");

$action = NormIN ('action', 'R');

if (in_array ($action, array('modif', 'suppr', 'exec_modif'))) {
	$id = $_REQUEST['id'];
	if (!is_numeric ($id)) DIE ("Valeur id interdite !"); 
}

if (in_array ($table, $dba_affbloc)) {				// Liste des tables à afficher par bloc
	$f_bloc = TRUE;
	$borne1 = (is_numeric (@$_POST['borne1'])) ? $_POST['borne1'] : 1;
	$pas = (is_numeric (@$_POST['pas'])) ? $_POST['pas'] : $dbase['taille_bloc'];
	$pas_ar = NormIN ('pas_ar');
	$pas_av = NormIN ('pas_av');
} else {
	$f_bloc = FALSE;
}

##### PROGRAMME PRINCIPAL ###############################################################################
global $droits, $Xvars;
Debut ();

if (!$table) {
	erreurMsg ("Vous avez oublié de sélectionner une table."); 
	Fin ();				// >>>>>>>>>>>>>>>>>>>>>>>>>> exit
}
$nomid = nomid ($table);	// Nom de l'ID de la table

// Affectation de $tables liees pour la vérification de la cohérence de la table
// $T_tablesliees est préparé dans globalvars.inc, pour rester indépendant du code.

$mode = "liste";		// Valeur par défaut pour affichage simple de la liste
$f_colonnes_mod = TRUE;

### TRAITEMENT ##################################################################
switch ($action) {

	############################################ Aiguillage selon le formulaire affichage par bloc ###
	#================================================================================== Pas arrière ===
		case 'pas_ar' :
			$borne1 -= $pas;
			if ($borne1 <1) $borne1 =1;
			break;

	#=================================================================================== Pas avant ===
		case 'pas_av' :
			$borne1 += $pas;
			break;

	#================================================================================== Réafficher ===
	case 'reafficher' :
		break;			// rien à faire

	######################### Aiguillage selon le bouton choisi à l'intérieur du tableau principal ###
	
	#==================================================================================== Modifier ===
	case 'modif' :
		$mode = 'modif';
		$f_colonnes_mod = FALSE;
		$f_ajout = FALSE;

		// Lire la ligne à modifier et la placer dans le tableau des variables
		$req = "SELECT * FROM $table where $nomid='$id'";
		$resultat = Requete ($req);
    $Xvars['ligne'] = mysqli_fetch_assoc($resultat);      
		break;
 
	#======================================================================================= ajout ===
	case 'ajout' :
		$mode = 'ajout';
		$f_colonnes_mod = FALSE;
		$f_ajout = TRUE;
		break;
 
	#======================================================================================= suppr ===
	case 'suppr' :
		// Gère la suppression d'un élement tout en préservant la cohérence de la base : 
		// si un élement utilise ce champ dans les tables liées $tables alors la fonction affiche un message d'erreur.

		$nomid = nomid ($table);
		$tablesliees = $T_tablesliees[$nomid];
		if (count ($tablesliees) && nbenregistrements ($table, $id, $tablesliees) > 0)
			 ErreurMsg ("L'enregistrement est déjà utilisé ailleurs, sa suppression nuirait à la cohérence de la table");
		else {
			 $result = requete ("DELETE FROM $table where $nomid = '$id'");
			 Message ("Élément supprimé");
		}  
		break;

	######################### Aiguillage selon le bouton choisi dans le bloc ajou/modif d'un élément ###

	#==================================================================================== exec_ajout ===
	case 'exec_ajout' :
		Ajouter_elt ($table);
		break;

	#==================================================================================== exec_modif ===
	case 'exec_modif' :
		Modifier_elt ($table, $id);
		break;
 
	#======================================================================================= annuler ===
	case 'annuler' :
		break;						// pas d'action 

} //endswitch

# Affichage écran principal "Modifier une table" et sur demande "ajouter un élément", "modifier un élément" 
############################################################################################################

   	// Préparer le tableau des variables
	$Xvars['droits'] = $droits;
	$Xvars['table'] = $table;
	$Xvars['idtable'] = Nomid ($table);
	$Xvars['id'] = @$id;
	$Xvars['mode'] = $mode;
	$Xvars['f_colonnes_mod'] = $f_colonnes_mod;
	$Xvars['f_ajout'] = @$f_ajout;
	// cas particulier : pas d'ajout possible dans certaines tables
	$Xvars['f_bouton_ajout'] = ($table == 'Verrous') ? "" : $f_colonnes_mod;

	$Xvars['f_bloc'] = $f_bloc;
	if ($f_bloc) { 
		$Xvars['borne1'] = $borne1;
		$Xvars['pas'] = $pas;
	}

	$liste_xml = Xopen ('./XML_modeles/modifier_table.xml') ;
	Xpose ($liste_xml);


   Fin ();
?>