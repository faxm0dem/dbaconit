<?php
# v25.9			200718	PhD		Création
# v25.9.01	200911	PhD 	Correction erreur sur $quest_v1
# v25.14		220716	PhD		Corrigé requête : cote et non coterange; attribut de strlen ne peut être nul (PHP 8)
###


############################################################ XML_list_cote ###
function XML_list_cote ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_cote;

	// Si tag de début, appeler la liste des cotes d'étage/zone/travée PAR ÉTABLISSEMENT
	if ($loop === 0) {		
		$SQLresult_cote = requete (
	 		"SELECT DISTINCT LEFT(coterange, 3) AS cote, prefinv, Collections.idetablissement 
	 		FROM Collections 
	 		LEFT JOIN Etablissements ON Collections.idetablissement = Etablissements.idetablissement
	 		ORDER BY prefinv, cote");

		// Préparer les totalisateurs
		$Xvars['tot_fiches'] = $Xvars['tot_machines'] = $Xvars['tot_documents'] = $Xvars['tot_logiciels'] = 0;
		$Xvars['tot_nonver'] = $Xvars['tot_ver'] = $Xvars['tot_ver1'] = $Xvars['tot_ver2'] = 0;
		
 	}
 	
	//  Pour chaque critère dominant chercher le nombre d'objets
	while ($ligne = mysqli_fetch_assoc ($SQLresult_cote)) { 
		$idetablissement = $ligne['idetablissement'];
		$Xvars['prefinv'] = $ligne['prefinv'];
		
		// Distinguer les cas 3 caractères et plus des cas de cote partielle
		$cote = $ligne['cote'];
		if ($cote === NULL) $cote= '';
		$Xvars['lib_cote'] = (strlen ($cote) ==3) ? $cote.'…' : $cote; 
		
		if ($cote == '') $test = "(coterange IS NULL)";
		elseif (strlen ($cote)<3) $test = "(Collections.idetablissement=$idetablissement) AND (coterange LIKE '$cote')";
		else $test = "(Collections.idetablissement=$idetablissement) AND (coterange LIKE '$cote%')";
		
		// Chercher le nombre global de fiches concernées	
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE $test");
		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
		$Xvars['tot_fiches'] += $Xvars['nbr_fiches'];
				// Préparer les paramètres pour l'URL de recherche
				$Xvars['quest'] = Phd_encode("$test", session_id ());

		// Chercher le nombre de machines	
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE $test AND (Collections.idmachine!=0)");
		$Xvars['nbr_machines'] = mysqli_num_rows($SQLresult2);
		$Xvars['tot_machines'] += $Xvars['nbr_machines'];
				// URL de recherche
				$Xvars['quest_m'] = Phd_encode("$test AND (Collections.idmachine!=0)", session_id ());

		// Chercher le nombre de documents
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE $test AND (Collections.iddocument!=0)");
		$Xvars['nbr_documents'] = mysqli_num_rows($SQLresult2);
		$Xvars['tot_documents'] += $Xvars['nbr_documents'];
				// URL de recherche
				$Xvars['quest_d'] = Phd_encode("$test AND (Collections.iddocument!=0)", session_id ());

		// Chercher le nombre de logiciels
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE $test AND (Collections.idlogiciel!=0)");
		$Xvars['nbr_logiciels'] = mysqli_num_rows($SQLresult2);
		$Xvars['tot_logiciels'] += $Xvars['nbr_logiciels'];
				// URL de recherche
				$Xvars['quest_l'] = Phd_encode("$test AND (Collections.idlogiciel!=0)", session_id ());

#...Si mode récolement
		if ($Xvars['recolement']) {
			
			// Chercher le nombre de fiches non verrouillées
			$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE $test AND m10=0");
			$Xvars['nbr_nonver'] = mysqli_num_rows($SQLresult2);
			$Xvars['tot_nonver'] += $Xvars['nbr_nonver'];
					// URL de recherche
					$Xvars['quest_nv'] = Phd_encode("$test AND m10=0", session_id ());

			// Chercher le nombre de fiches verrouillées
			$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE $test AND m10!=0");
			$Xvars['nbr_ver'] = mysqli_num_rows($SQLresult2);
			$Xvars['tot_ver'] += $Xvars['nbr_ver'];
					// URL de recherche
					$Xvars['quest_v'] = Phd_encode("$test AND m10!=0", session_id ());

			// Chercher le nombre de fiches verrouillées 1
			$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE $test AND m10=1");
			$Xvars['nbr_ver1'] = mysqli_num_rows($SQLresult2);
			$Xvars['tot_ver1'] += $Xvars['nbr_ver1'];
					// URL de recherche
					$Xvars['quest_v1'] = Phd_encode("$test AND m10=1", session_id ());

			// Chercher le nombre de fiches verrouillées 2
			$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE $test AND m10=2");
			$Xvars['nbr_ver2'] = mysqli_num_rows($SQLresult2);
			$Xvars['tot_ver2'] += $Xvars['nbr_ver2'];
					// URL de recherche
					$Xvars['quest_v2'] = Phd_encode("$test AND m10= 2", session_id ());
					
		}
		
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';		

		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

########################################################################################################################
########################################################################################################################

$custom_css = "list_coterange.css";
require_once ('init.inc.php');

# Initialisations ##############################

Debut ();

// Noter le mode récolement (conditionne l'affichage des colonnes "verrouillage")
$Xvars['recolement'] = @$dbase['mode_recolement']; 

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/list_coterange.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>