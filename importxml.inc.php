<?php
# v25				190711	PhD		Création
# v25.3			191031	PhD		Toilettage
# v25.4			191208	PhD		Ajouté NormSQL manquant dans Tablecheck
# v25.6			200121	PhD		Modifié Tablecheck pour permettre test champs multiples
# v25.7			200328	PhD		Ajouté UnlinkRecursive
# v25.12		211026	PhD		'Tablecheck' : traité cas $val='', ajouté forçage d'écriture
#
##################################################################### Copymedia ###
function Copymedia ($idmedia_d, $idmedia_l, $extension) 
# Copie les fichiers médias reçus dans le dossier zip et les placent dans le dossier ad-hoc.
{
	global $db, $dir_dezip;

 // copie du media principal
 copy ($dir_dezip.$idmedia_d ,AdMedia($idmedia_l, $db, $extension, '', 'rel', 'w'));
 
}

##################################################################### Fil ###
function Fil ($cod, $txt) 
# Affiche des messages "au fil de l'eau" durant le traitement
{
	switch ($cod) {
		case 'h1' : echo "<h1> $txt </h1>"; break;
		case 'h2' : echo "<h2> $txt </h2>"; break;
		case 'v'	: echo "<p class=vert> $txt </p>"; break; 
		case 'r'	: echo "<p class=rouge> $txt </p>";
	}
	return;
}


##################################################################### Id2nom ###
function Id2nom ($id) 
# renvoie le nom de la table  et le nom de l'élément principal à partir de l'index
# Traite les tables d'ordre (ni les tables de liaison Col, ni la table Verrous…)
{
  switch ($id) {
  	case 'idmotcle'		: $tn = array ('Motscles', 'motcle'); break;
  	case 'idmateriau'	: $tn = array ('Materiaux', 'materiau'); break;
  	default :  
  		$nom = substr ($id, 2) ;
  		$tn = array (ucfirst ($nom).'s', $nom);
  }
  return $tn;
}


############################################################## TableCheck ###
function TableCheck ($table, $elem, $val, $f_creat, $XMLobject="") 
# Teste l'existence d'un élément dans une table (table secondaire, table d'ordre…),
# Si l'élément n'existe pas, le crée si autorisé,
# Retourne l'index, ou 0 si refusé
# 
#	'$table'			- Table concernée
# '$elem'				- Champ testé – si vide, alors $val contient une expression 'where' complète
# '$val'				- Élément recherché
# '$f_creat'		- Droit de création de l'élément dans la table s'il n'esiste pas
#									si $f_create=='FORCE', l'écriture est forcée dans tous les cas.
# '$XMLobject'	-	Éventuellement : Tableau contenant les différents champs à copier 
#
###
{	global $dblink, $f_erreur;

### Tester l'existence en base locale
	$nomid = Nomid ($table);	// Trouver le nom de l'index
	
	// Composer la chaîne SQL de recherche	$where = '';
	if ($elem == '') $where = $val;
	else {
		if ($val == '') $where = $elem." IS NULL";
		else $where = $elem."=\"$val\"";
	}
	
	$result = requete ("SELECT $nomid FROM $table WHERE $where");

###		N'existe pas localement
###   ou forcage d'écriture (cas exceptionnel des médias mal définis par leur 'coterange')
	if (0 == mysqli_num_rows ($result) OR $f_creat === 'FORCE') {

		if (!$f_creat) {
			// Pas le droit de création : abandon pour cette table
			Fil ('r', Tr ("«".$val."» inconnu dans la table $table - Pas de droit de création", 
										"«".$val."» unknown in table $table - Creation not allowed: "));
			
		} else {
			// Droit de création, créer l'élément en table locale
			$set = "";
			if($XMLobject != "") {										// Élément complétement défini dans le fichier XML
				foreach ($XMLobject as $nchamp=>$champ) {	
				$champ = NormSQL ($champ);
					if (substr ($nchamp, 0, 2)!= 'id') $set .= "$nchamp='$champ', ";		// On ne traite pas les index
				}
				$set = rtrim ($set, " ,");
				
			} else $set = "$elem='".NormSQL($val)."'";							// Élément défini uniquement par une valeur
			
			// Créer l'élèment en table locale				
			$result = requete ("INSERT INTO $table SET $set");
			if (!$result) erreurMsg (Tr ("Erreur d'écriture dans ", "Writing error in ").$table); 		
			else {
				$idtable = mysqli_insert_id ($dblink);				
				Fil ('v', Tr ("Élément «".$val."» ajouté en table : ", "Item «".$val."» added in table: ").$table);
			}
		}
		
	} else {
### 	Existe localement : noter l'index
		$ligne = mysqli_fetch_assoc($result); 
		$idtable = $ligne [$nomid];
	}
	return (isset($idtable) ? $idtable : 0);	
}   
      

##################################################################### type2aux ###
#
# En partant du type d'un objet, cette table fournit le nom de la table auxiliaire et le nom de son index
$type2aux = array (
	'document'	=> array ('Documents', 'iddocument'),
	'iconographie' => array ('Documents', 'iddocument'),
	'machine'	=> array ('Machines', 'idmachine'),
	'objet'	=> array ('Machines', 'idmachine'),
	'logiciel'	=> array ('Logiciels', 'idlogiciel')
	);
	
##################################################################### UnlinkRecursive ###
function UnlinkRecursive($dir, $deleteRootToo) 
# Recursively delete a directory 
# 
# @param string $dir Directory name 
# @param boolean $deleteRootToo Delete specified top-level directory as well 
# 
{ 
  if(!$dh = @opendir($dir)) return; 
 
  while (false !== ($obj = readdir($dh)))  {   
    if($obj == '.' || $obj == '..') continue; 
    if (!@unlink($dir . '/' . $obj)) unlinkRecursive($dir.'/'.$obj, true); 
  } 

  closedir($dh);     
	if ($deleteRootToo) @rmdir($dir); 
    
  return; 
} 

###################################################################################################
?>
