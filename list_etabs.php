<?php
# v24				190131	PhD		Création à partir de list_bats
# v25				190615	PhD		Supprimé inst debug 8
# v25.2			190906	PhD		Le terme SQL de recherche est maintenant codé
# v25.8			200411	PhD		Ajout custom_css
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
------------------------------------------------------------------------------ */
############################################################ XML_list_bat ###
function XML_list_etab ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_bat;

	// Si tag de début, appeler la liste des établissements
	if ($loop === 0) {
		
		$SQLresult_bat = requete ("SELECT *	FROM Etablissements ORDER BY prefinv");
 	}
			
	//  Appel de létablissement courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult_bat)) { 
		$Xvars['ligne'] = $ligne;

		// Chercher le nombre de fiches concernées	
		$idetablissement = $ligne['idetablissement'];

		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idetablissement = $idetablissement");
		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
	
		// Préparer les paramètres pour l'URL de recherche
		$Xvars['quest'] =Phd_encode("Collections.idetablissement = $idetablissement", session_id ());
	
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';		
	
		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

########################################################################################################################
########################################################################################################################

$custom_css = "list_etabs.css";
require_once ('init.inc.php');

## Traitement des entrées :
###########################
	
# Initialisations ##############################

Debut ();

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/list_etabs.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>