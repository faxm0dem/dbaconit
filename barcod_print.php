<?php
# v22.1			180617	PhD		Création
# v24				190219	PhD		Modifié appel Nrinventaire
# v24.2			190410	PhD		Corrigé découpage nrinventaire
# v25				190615	PhD		Supprimé inst debug 8
# v25.8			200411	PhD		Ajout custom_css
# v25.10		210208	PhD		Nouveau format label, avec code mixte idcol+nrinv
# v25.10.01	210222	PhD		Remplacé fichier unique barcod.txt par tableau session (risque de conflits multi-utilisateurs)
# v25.11		210330	PhD		Traitement différencié sur paramètre "dymo", supp. No opérateur dans filename
###

$jscript = "
function msg_ok (msg) {
	document.getElementById('demo').innerHTML = 'Téléchargement lancé';
}";			// Français seul...

$f_non_head_html = TRUE; 					// Ceci va bloquer l'ouverture de la page HTML dans init.inc
																	// Nécessaire dans le cas export d'un fichier
$custom_css = "barcod_print.css";
require_once ('init.inc.php');		// Initialisations, identification..

############################################################ XML_liste_labels ###
function XML_liste_labels ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	
## tag de début
	if ($loop === 0) {
	}

##  Appel de la valeur courante
	if (count ($_SESSION['barcodtxt']) > $loop) {
			$a_line = $_SESSION['barcodtxt'][$loop][0];
			$b_line = $_SESSION['barcodtxt'][$loop][1];
			$Xvars['label'] = trim ($a_line). "__".trim ($b_line);
			return 'ACT,LOOP';
		
	} else {
		$Xvars['nlabels'] = $loop;
		return 'EXIT' ;
	}
} 

##################################################################################

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
'in_code'				– POST – filtré NormIn
'in_label'			– POST – filtré NormIn
------------------------------------------------------------------------------ */

## Traitement des entrées :
###########################

$action = @$_POST['action'];
if (!$action) $action = 'empty';

if (!isset ($_SESSION[('barcodtxt')])) $_SESSION['barcodtxt'] = array ();
$in_label = NormIn ('in_label');
$in_code = NormIn ('in_code');

if (isset ($dbase['dymo'])) {
	$dymo = $dbase['dymo'];
	switch ($dymo) {
		case 'text' : $filename = 'barcod.txt'; break;
		case 'csv_virg' : $filename = 'barcod.csv'; break;
		case 'csv_ptvirg' : $filename = 'barcod.csv'; break;
	}	
} else {
	$dymo = $filename = '';
}

####################################################

$mode = '';

### L'affichage des objets déjà préparés se fait automatiquement par XML_liste_labels


switch ($action) {
case 'empty':
#############
	break;
   	  
case 'ajout':
#############

	if (isset ($_SESSION['idcollections']))  $idcollections = $_SESSION['idcollections'];
	else if (isset ($_SESSION['idcollection'])) $idcollections[] = $_SESSION['idcollection'];
	
		if (isset ($idcollections)){
		foreach ($idcollections as $id) {
			$ligne = mysqli_fetch_assoc (requete ("SELECT Collections.idetablissement, nrinv, prefinv 
				FROM Collections
				LEFT JOIN Etablissements ON Collections.idetablissement=Etablissements.idetablissement
			WHERE  idcollection = $id "));
	
			$a_line = $ligne['prefinv']." ".$ligne['nrinv']; 
			$b_line = $ligne['idetablissement'].K_SEP.$ligne['nrinv'];			   // CRLF imposé par prog DYMO
			$_SESSION['barcodtxt'][] = array ($a_line, $b_line);
		}
	}else erreurMsg (Tr ("Pas d'objets sélectionnés...", "Not any items selected..."));
	break;
	
case 'annuler':
#############
	$mode = '';
	break;
	
case 'enregistrer':													// ajouter une entrée manuelle dans la liste des labels
#############
	$_SESSION['barcodtxt'][] = array ($in_label, $in_code);
		
	$mode = '';	
	break;
	
case 'export':
#############
	if ($filename == '') {
		erreurMsg (Tr ("Paramètre 'dymo' non défini", "Parameter 'dymo' undefined)"));
		break;
	}
	
# Créer le répertoire temporaire si non existant, ouvrir le fichier
	if (!is_dir($dir_tempo)) mkdir ($dir_tempo);
	$file_barcod = $dir_tempo.'/'.$filename;
	$handle = fopen ($file_barcod, 'a');			// ouvrir en écriture et et pointer en queue


	// Traitement différent suivant le paramètre "dymo"	
	switch ($dymo) {
		case 'text' :		// fichier texte avec une paire de lignes pour chaque étiquette
			foreach ($_SESSION['barcodtxt'] as $label) {
				$text = $label[0]."\r\n".$label[1]."\r\n";		// CRLF imposé par prog DYMO Mac
				fwrite ($handle, $text);
			}
		break;
			
		case 'csv_virg' :		// fichier CSV, séparateur VIRGULE
			$text = "ADRESSE,CODEBARRE\r\n";
			fwrite ($handle, $text);												// première ligne
			foreach ($_SESSION['barcodtxt'] as $label) {
				$text = $label[0].",".$label[1]."\r\n";		
				fwrite ($handle, $text);
			}
		break;
			
		case 'csv_ptvirg' :		// fichier CSV, séparateur POINT VIRGULE
			$text = "ADRESSE;CODEBARRE\r\n";
			fwrite ($handle, $text);												// première ligne
			foreach ($_SESSION['barcodtxt'] as $label) {
				$text = $label[0].";".$label[1]."\r\n";		
				fwrite ($handle, $text);
			}
		break;
	}
	// Fermeture
	fclose ($handle);
	
	// Exporter le fichier vers la machine opérateur
	if (file_exists($file_barcod)) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($file_barcod).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: '.filesize($file_barcod));
		readfile($file_barcod);
	
		unlink ($file_barcod);			// effacement du fichier
		unset ($_SESSION['barcodtxt']);
		exit;    										// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ON NE PEUT PLUS AFFICHER SUR L'ÉCRAN

	} else {
		ErreurMSG (Tr ("Fichier absent", 'File not found'));
	}
	break;
	
case 'manuel':
#############
	$mode = 'manuel';
	break;
	
case 'vider':
#############
	unset ($_SESSION['barcodtxt']);
	break;
	
}    

# AFFICHAGE ÉCRAN PRINCIPAL  
#############################

require_once ('inc_tete.php');  // Ouverture retardée de la page HTML
Debut ();

$Xvars['mode'] = $mode;
$Xvars['filename'] = $filename;

$liste_xml = Xopen ('./XML_modeles/barcod_print.xml') ;
Xpose ($liste_xml);

Fin ();
?>

