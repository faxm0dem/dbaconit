<?php
# v25			190615	PhD		Supprimé inst debug 8, ajouté distinct dans 'XML_nom'
# v25.2		190906	PhD		Le terme SQL de recherche est maintenant codé
# v25.5		191226	PhD		Ajout taille_bloc
# v25.6		200116	PhD		Ajouté protection pour ne pas créer des doublons, msg anglais
# v25.8		200411	PhD		Ajout custom_css
###


/* Protection des entrées -------------------------------------------------------
'renom'				- POST submit - uniquement testé isset
'action'			- POST submit - uniquement testé valeur connue
'modif'				- POST submit - uniquement testé isset
'idancien'		* POST - Traité NormIN
'idnouveau'		* POST - Traité NormIN
'nouvnom'			* POST - Traité NormIN
'nouvnotes'		* POST - Traité NormIN
'idsuprim[]'	* POST - testé numérique
'idpersonne'	* POST - testé numérique
--- affichage par bloc :
'affich'			- POST  - Filtré NormIN  - uniquement testé switch
'borne1'			- POST  - Filtré numérique
'pas'					- POST  - Filtré numérique
'pas_ar'			- POST  - Filtré NormIN  - uniquement testé switch
'pas_av'			- POST  - Filtré NormIN  - uniquement testé switch
------------------------------------------------------------------------------ */

############################################################# Javascript ###
# (Le traitement langue anglaise manque ici)
$jscript = "

function valid_renom () {
	var ancien, nouveau, nouvnom, result=false, tab;

	for (var i=0; i<document.renommer.ancien.length; i++) {
		if (document.renommer.ancien[i].checked) {
			tab = document.renommer.ancien[i].value.split ('#');
			ancien = tab[1];
			document.renommer.idancien.value = tab[0];
			break;
		}
	}
	
	for (var j=0; j<document.renommer.nouveau.length; j++) {
		if (document.renommer.nouveau[j].checked) {
			nouveau = document.renommer.nouveau[j].value;
			
			if (nouveau=='_creation_') {
				nouvnom = document.renommer.nouvnom.value;
				document.renommer.idnouveau.value = nouveau;
				nouveau = document.renommer.nouvcivil.value +' '+
					document.renommer.nouvprenom.value +' '+
					document.renommer.nouvnom.value;
				break;
			 } else { 			
				tab = nouveau.split ('#');
				nouveau = tab[1];
				document.renommer.idnouveau.value = tab[0];
				break;
			}
		}
	}
		
	if (ancien==null || nouveau==null || nouvnom=='') {
		alert ('Informations noms manquantes');
		result = false;
	} else 
		result = confirm ('Confirmer le remplacement : '+ancien+ ' ==> '+nouveau);

	if (result) {
		document.renommer.action.value ='renom';
		document.renommer.submit('renom');
	}
} 

function valid_suppr () {	// NON UTILISÉ - A REVOIR : IDSUPRIM [I] EST DÉCLARÉ INDÉFINI
	for (var i=0; i<document.renommer.ancien.length; i++) {
		if (document.renommer.idsuprim[i].checked) {
			document.renommer.submit();
		}
	}
 	alert ('Informations suppression manquantes');
}
";

#################################################################################################### XML_liste_civil ###
function XML_liste_civil ($loop, $attr, $Xaction) {
# >>> Analogue à 'XML_liste_radio' dans mod-org-per-inc <<<
# Une génération directe par echo pour éviter de multiples lignes en format xml
# ou une (élégante) boucle appelant 7 fois cette fonction 
	global $Xvars, $T_civil;

	if ($loop === null) return;		// tag de fin	

	foreach ($T_civil as $cv) {
		$checked = ($cv == $Xvars['psel_civil']) ? "checked='TRUE'" : "";
		echo "<input name='nouvcivil' value='$cv' type='radio' $checked /><span>$cv&nbsp;&nbsp;&nbsp;</span>";
	}

	return 'EXIT' ;
} 


################################################################ XML_nom ###
function XML_nom ($loop, $attr, $Xaction) {
	if ($loop === null) return;		// tag final : </nom>

	global $Xvars;
	$borne1 = $Xvars['borne1'];
	$pas = $Xvars['pas'];

	//  Appel du nom courant
	$ligne = mysqli_fetch_assoc ($Xvars['SQLresult']);
	if ((!empty($ligne)) && ($loop+1 >= $borne1)) {    // Traitement à partir de la borne basse
		$Xvars = array_merge ($Xvars, $ligne);
		extract($ligne);

		// Chercher le nombre de fiches concernées	
		$SQLresult2 = requete ("SELECT DISTINCT idcollection FROM Col_Per WHERE idpersonne = $idpersonne");
		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
		
		// Préparer les paramètres pour l'URL de recherche
		$Xvars['quest'] = Phd_encode("Col_Per.idpersonne=$idpersonne", session_id ());
		
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';
	} elseif ((!empty($ligne)) && ($loop+1 < $borne1)) return 'LOOP';	// avant la borne basse, boucle sans affichage

	return ((!empty($ligne)) && ($loop+1<$borne1+$pas)) ? 'ACT,LOOP' : 'EXIT';	// Affichage -> borne haute, puis sortie
} 

############################################################################
################################################################# TRAITEMENT

$custom_css = "list_per.css";
require_once ('init.inc.php');

Debut();  

### Traitement des entrées
####################################
$action = @$_POST['action'];
$id_ancien = NormIN ('idancien');
$id_nouveau = NormIN ('idnouveau');
$nouvcivil = NormIN ('nouvcivil');
$nouvprenom = NormIN ('nouvprenom');
$nouvnom = NormIN ('nouvnom');
$nouvnotes = NormIN ('nouvnotes');

if (isset ($_POST['suppr'])) $action = 'suppr';		// Javascript ne permet pas d'utiliser 2 boutons nommés action

if(isset ($_POST['modif'])) {			// les boutons 'Modifier' transmettent à la fois l'ordre et l'idpersonne
	$action = 'modif';
	$idpersonne = $_POST['modif'];
}	

if (is_numeric(@$_POST['idpersonne'])) $idpersonne = $_POST['idpersonne'];

$id_suprim = @$_POST['idsuprim'];
if (isset ($id_suprim)) {
	foreach ($id_suprim as $n) {
		if (!is_numeric($n)) DIE ("*** list_per.php - Paramètre 'suprim' faux ! ***");
	}
}


if (in_array ('list_per', $dba_affbloc)) {				// Liste des tables à afficher par bloc
	$f_bloc = TRUE;
	$affich = NormIN ('affich');
	$borne1 = (is_numeric (@$_POST['borne1'])) ? $_POST['borne1'] : 1;
	$pas = (is_numeric (@$_POST['pas'])) ? $_POST['pas'] : $dbase['taille_bloc'];
	$pas_ar = NormIN ('pas_ar');
	$pas_av = NormIN ('pas_av');
} else {
	$f_bloc = FALSE;
}

### Si affichage par bloc : calcul des bornes selon formulaire ###
if ($f_bloc) {
	switch ($affich) {
			case 'pas_ar' :				// Pas arrière
				$borne1 -= $pas;
				if ($borne1 <1) $borne1 =1;
				break;
			case 'pas_av' :				// Pas avant
				$borne1 += $pas;
				break;
		case 'reafficher' :			// Réafficher
			break;			// rien à faire
	}
} else {
	$borne1 = 1;
	$pas = 1000000;
}

### CHOIX DE L'ACTION À EXÉCUTER #############
##############################################
switch ($action) {

	### Si modification d'un nom demandé, afficher le formulaire de modification
	############################################################################
	case 'modif':
		$result = requete ("SELECT * FROM Personnes WHERE idpersonne = $idpersonne");
		$ligne = mysqli_fetch_assoc ($result);
		$Xvars['psel_civil'] = $ligne['pcivil'];
		$Xvars['psel_prenom'] = $ligne['pprenom'];
		$Xvars['psel_nom'] = $ligne['pnom'];
		$Xvars['psel_notes'] = $ligne['pnotes'];
		$Xvars['idpersonne'] = $idpersonne;
	
		break ;	// et aller afficher

	### Modification annulée : ne rien faire
	############################################################################
	case 'annuler':	
		break ;	// aller afficher

	### Modification : enregistrer le nom modifié
	############################################################################
	case 'enregistrer':
		$SQLpcivil =  NormSQL ($nouvcivil);
		$SQLpprenom = NormSQL ($nouvprenom);
		$SQLpnom = NormSQL ($nouvnom);
		$SQLpnotes = NormSQL ($nouvnotes);		
		
		$requete = "SELECT idpersonne FROM Personnes 
								WHERE (pcivil='$SQLpcivil') AND (pprenom ='$SQLpprenom') AND (pnom ='$SQLpnom')";	
		$SQLresult = requete ($requete);

 		$f_corrig = FALSE;
 		if (0 == mysqli_num_rows ($SQLresult)) {
    	// Ce nom n'existe pas sous cette forme, on peut le corriger
    	$f_corrig = TRUE;
   	} else {
			// Ce nom existe... est-ce le même id ? (il ne reste que les notes à corriger )
			$ligne = mysqli_fetch_assoc ($SQLresult);
			$idpernouv = $ligne['idpersonne'];
			if ($idpernouv == $idpersonne) $f_corrig = TRUE;
		}	
		
 		// Dans les 2 cas on peut corriger
 		if ($f_corrig) {
			$r = requete ("UPDATE Personnes SET pcivil='$SQLpcivil', pprenom='$SQLpprenom', pnom='$SQLpnom',
										pnotes='$SQLpnotes' WHERE idpersonne=$idpersonne");
			if ($r) Message (Tr ('Remplacement effectué', 'Replacement done'));
			else  erreurMSG (Tr ('Écriture du nom refusée', 'Name recording rejected'));	
			
		} else erreurMSG (Tr ("Nom déjà existant, utiliser le renommage pour changer les liens", 
												'Existing name, use rename command for updaying the links'));


		AfficheMessages();
		break ;	// et aller afficher

	### Si suppression d'un nom demandé, l'exécuter 
	###############################################
	case 'suppr' :

		if (!empty ($id_suprim)) {
			foreach ($id_suprim as $id) {
				$r = requete ("DELETE FROM Personnes WHERE idpersonne=$id");
				if ($r) Message (Tr ('Personne supprimée :', 'Deleted person:'). $id);
			}
		} else  erreurMSG (Tr ('Informations manquantes', 'Data missing'));
	
		AfficheMessages();
		break ;	// et aller afficher

	### Si remplacement de nom demandé, l'exécuter
	##############################################
	case 'renom' :
		if ($id_ancien!==''  && $id_nouveau!=='') {
			// Changer le lien dans la table de liaison
			$r = requete ("UPDATE Col_Per SET idpersonne=$id_nouveau WHERE idpersonne=$id_ancien");
			if ($r) Message (Tr ('Remplacement effectué', 'Replacement done'));
		}
		else  erreurMSG (Tr ('Informations manquantes', 'Data missing'));

		AfficheMessages();
		break ;	// et aller afficher

}

### Puis afficher la liste
####################################
// Trouver la taille des champs
$SQLtemp =  requete ( "SELECT pnom FROM Personnes LIMIT 1");
$Xvars['size_nom'] = mysqli_fetch_field_direct ($SQLtemp, 0)->length;

$SQLtemp =  requete ( "SELECT pprenom FROM Personnes LIMIT 1");
$Xvars['size_prenom'] = mysqli_fetch_field_direct ($SQLtemp, 0)->length;

// Sélectionner tous les noms demandés
$SQLresult = requete ( "SELECT * FROM Personnes ORDER BY pnom, pprenom, pcivil");
$Xvars['SQLresult'] = $SQLresult;

$Xvars['total_noms'] = mysqli_num_rows ($SQLresult);
$Xvars['server'] = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];	// adresse retour formulaire

// table de correspondance entre un nom de champ et les affichages spécifiques 
$Xvars['l_champs'] = array (
	'titre'=> Tr ('Nombre total de personnes', 'Total number of persons'),
	'civil'=>Tr ('Civilité', 'Title'),
	'prenom'=>Tr ('Prénom', 'First name'),
	'nom'=>Tr ('Nom de la personne', 'Last name'),
	'notes'=>Tr ('Commentaires', 'Comments')
);

$Xvars['droits'] = $droits;

$Xvars['f_bloc'] = $f_bloc;
$Xvars['borne1'] = $borne1;
$Xvars['pas'] = $pas;

$Xvars['action'] = $action;

#======================= Afficher partir du modèle XML
	$liste_xml = Xopen ('./XML_modeles/list_per.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin ();
?>