<?php
# v22.2			180830 	PhD		Création
# v25.8			200411	PhD		Ajout custom_css
# v25.10		210211	PhD		Ajout traitement nr inventaire mixte
###

$custom_css = "barcod_liste.css";
require_once ('init.inc.php');		// Initialisations, identification..


##################################################################################

/* Protection des entrées -------------------------------------------------------
//? 'action'				- POST - uniquement testé switch
'sinput'				– POPST – filtré NormIn
------------------------------------------------------------------------------ */

## Traitement des entrées :
###########################

$action = @$_POST['action'];
if (!$action) $action = 'empty';
$sinput = NormIn ('sinput');

#  TRAITEMENT - SAISIE DES POSITIONS
####################################

$audio ='';
$id_err = $cote_err = FALSE; 
$barcod = '';

switch ($action) {
case 'empty':
#############
	break;
	
case 'entree':
##############
  if ($sinput != '') {
		// Remplacer les codes § par -
		$sinput = str_replace ('§', '-', $sinput);

### S'il y a une lettre dans les 4 premiers caractères, c'est une cote de rangement
  	if (ctype_alpha (substr ($sinput, 0, 1)) OR ctype_alpha (substr ($sinput, 1, 1)) 
  			OR ctype_alpha (substr ($sinput, 2, 1)) OR ctype_alpha (substr ($sinput, 3, 1))) {
  		$coterange = $sinput;

			// Tester cette cote
			$SQLresult = requete ( "SELECT idcollection FROM Collections WHERE coterange LIKE '$coterange%' "  );
			if (!mysqli_num_rows ($SQLresult)) {
				$cote_err = TRUE;			// pas d'objets, erreur
				$barcod = $sinput;
				$audio = 'echec';
			} else {                // réponse, afficher la liste
				// Préparer les paramètres pour le module de recherche
				$SQL_include = "coterange LIKE '$coterange%' ";
				$f_include = TRUE;
				include ("rechercher.php");
			}			
				
		} else {
### Sinon, ce doit être un numéro d'inventaire
			// Séparer idetablisement et nrinv
			$n = strpos ($sinput, K_SEP);
			if ($n != 0) {
				$idetablissement = substr ($sinput, 0, $n);
				$nrinv = substr ($sinput, $n+1);
			}	else {									// sinon, prebdre l'établissement par défaut
				$idetablissement = $dbase['etablissement_defaut'];
				$nrinv = $sinput;
			}
			
			// Tester ce numéro
			$SQLresult = requete ( "SELECT idcollection FROM Collections 
														WHERE idetablissement=$idetablissement AND nrinv LIKE '$nrinv' "  );
			switch (@mysqli_num_rows ($SQLresult)) {
				case 0 :		// numéro inconnu
					$id_err = TRUE;
					$barcod = $sinput;
					$audio = 'echec';
					break;
					
				case 1: 		// un seul objet, afficher la fiche
					$ligne =  mysqli_fetch_assoc ($SQLresult);
					$idcollection = $_SESSION['idcollection'] = $ligne['idcollection'];

					// Se brancher sur l'écran "Consulter"			  
					$_SERVER['PHP_SELF'] = 'consulter.php';
					include ($_SERVER['PHP_SELF']);							// >>>>>>>>>>>>>>>>>> Branchement vers "Consulter"
																											// >>>>>>>> EXIT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				
				default :		// réponses multiples, afficher la liste
					// Préparer les paramètres pour le module de recherche
					$SQL_include = "nrinv LIKE '$sinput%' ";
					$f_include = TRUE;
					include ("rechercher.php");
			}			
  	} 	   
  }
}    

# AFFICHAGE ÉCRAN PRINCIPAL  
#############################

Debut ();

$Xvars['audio']				= $audio;
$Xvars['barcod']			= $barcod;
$Xvars['id_err']			= $id_err;
$Xvars['cote_err']		= $cote_err;

$liste_xml = Xopen ('./XML_modeles/barcod_liste.xml') ;
Xpose ($liste_xml);

Fin ();
?>

