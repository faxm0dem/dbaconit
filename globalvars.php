<?php
# v21				171122	PhD		Ajout table Tnpositions
# v21.1			171129	PhD		Ajout pointeur sélection
# v22				180522	PhD		Ajout table des civilités
# v23				180831	PhD		Ajout droits barcod
# v23.1.01	180903	PhD		Correction droits
# v24				190225	PhD		Ajout tables Batiments et Series, localisation=>etablissement, P_AFF_GALVIRT devient VIGNETTE
# v24.1			190320	PhD		Ajout droit mod_rapport pour administrateurs
# v25				190619	PhD		Ajout table Compteurs, Table Categories devient Domaines, Refonte table profil, 
#														Modif PROFIL_DEF
# v25.1			190722	PhD		PHP7 changé constantes
# v25.4			191123	PhD		Ajoute table Périodes dans $T_tablesliees (oubli)
# v25.5			191227	PhD		Ajouté droit maint_desh
# v25.6			200116	PhD		Ajouté cas vide dans 'T_civil', ajouté P_ORD_IDCOL
# v25.8			200411	PhD		Ajout des noms des fichiers plugin
# v25.10		210208	PhD		Ajout K_SEP
# v25.14		220716	PhD		Adapté $date_fmt et $timestamp_fmt au format fn date (PHP 8.1)
###

######### VERSION LOGICIELLE #############
$DBAconit ='DBAconit V26.1';		// 230223
	 
date_default_timezone_set('Europe/Paris');	
$date_fmt = array ('Y-m-d', 'Y-m-');	// format strftime
$timestamp_fmt = $date_fmt[0] . '  H:i';
$langues = array ('FR', 'EN');

#### TABLE DES DROITS ##########################################################
$tab_droits ["senior_admin"] = array (
				"aff_caller",			// Affichage adresse appellant dans titre
				"aff_complet", 		// Affichage écran "Complet" ET MENU
				"aff_etatfiche",	// Affichage et modif du champ "etatfiche"
				"aff_galerie",		// Affichage des salles masquées des galeries
				"aff_idcol"	,			// Affichage No idcollection
				"aff_req_com",		// Affichage requètes communes
				"aff_req_priv",		// Affichage requètes privées ... et des médias privés
				"aff_resume", 		// Affichage écran "Résumé"
				"backtrace"	,			// Affichage erreur backtrace (si code debug)
				"creer_objet", 		// Création des objets
				"barcod_w",				// Codes barres : fonctions écriture
				"barcod_r",				// Codes barres : fonction lecture
				"maintenance",		// Maintenance
				"maint_restaur",	// Maintenance : restauration 
				"maint_util",			// Maintenance : fonctions utilitaires
				"mod_admin",			// Modification des données administratives
				"mod_export",			// Maintenance : traitement des validations export
				"mod_objet"	, 		// Modification des objets
				"mod_rapport", 		// Modification de la table des rapporteurs
				"mod_req"	,				// Modification des requètes prédéfinies
				"mod_tables",			// Modification des tables
				"rang_galerie",		// Rangement des objets dans les galeries
				"recol_aff",			// Mode récolement : contrôle affichages détaillés
				"recol_desh",			// Mode désherbage : (si recol_aff actif) 
				"recol_verrou",		// Mode désherbage : autorisation d'action sur m10=verrou
				"sauv_auto",			// Sauvegarde automatique
//				"sup_objet",				// Supprimer un objet (table Collections et tous liens) ... assigné par droitperso
				"sup_elt"					// Supprimer un élément d'une table
				);				 
$tab_droits ["administrateur"] = array (		// sans mod_rapport, maint_restaur, recol_desh, recol_verrou
				"aff_caller",
				"aff_complet",
				"aff_etatfiche",
				"aff_galerie",
				"aff_idcol"	,
				"aff_req_com",
				"aff_req_priv",
				"aff_resume",
				"backtrace"	,
				"creer_objet",
				"barcod_w",
				"barcod_r",
				"maintenance",
				"mod_admin",
				"mod_objet"	, 
				"mod_rapport",
				"mod_req"	,
				"mod_tables",
				"rang_galerie",
				"recol_aff",
				"recol_desh", 
				"sauv_auto"
				);				 
$tab_droits ["rapporteur"] = array (
				"aff_complet", 
				"aff_etatfiche",
				"aff_galerie",
				"aff_idcol"	,
				"aff_req_com",
				"aff_req_priv",
				"aff_resume", 
				"barcod_r",
				"creer_objet", 
				"mod_req"	,
				"mod_objet"	,  
				"rang_galerie",
				"recol_aff",
				"recol_desh",
				"sauv_auto"
				);				 
$tab_droits ["expert"] = array (
				"aff_complet",
				"aff_galerie",
				"aff_idcol"	,
				"aff_req_com",
				"aff_resume", 
				); 
$tab_droits ["client_web"] = array (
				"consult"	,		// Consultation
				); 
$tab_droits ["visiteur"] = array (
				"aff_resume", 
				);

#### PARAMETRES DIVERS #########################################################
define ("DB_nbr_niveaux_max", 20);	// Nombre max de niveaux de liens traités
define ('K_SEP', '%');						// caractère séparateur dans No inventaire mixtes
$kfiche 	= array ('transfert' => 2, 'refus' => 3, 'attente' => 4);	// Codes "idetafiche" traité par gestexport
$T_civil = array ('', 'M.', 'Mme', 'Mlle', 'Dr', 'Pr', 'Me', 'MM', 'Mmes', 'MMme');

									
# PARAMETRES IMAGES 
$plmax = 600;					# Taille des photos
$phmax = 600;					# 
$vlmax = 150;					# Taille des vignettes
$vhmax = 150;

# NOM DES FICHIERS PLUGIN
$dbaconit_plus 	= 'dbaconit_plus.css';
$menu_tete_plus = 'menu_tete_plus.xml';
$requete_plus 	= 'requete_plus.php';
$note_recol 		= 'note_recol.xml';

# Pointeurs dans la chaine de caractères binaires $b_profil (à partir de 0)
define ('P_MACHINE', 0);					// sélection de fiches machines
define ('P_DOCUMENT', 1);					// sélection de fiches documents
define ('P_LOGICIEL', 2);					// sélection de fiches logiciels
define ('P_OBJET', 3);						// Sélection de fiches objets
define ('P_ICONO', 4);						// Sélection de fiches iconographie
																// ...en réserve 5,6
define ('P_ORD_IDCOL', 7);				// ordre de tri : index idcollection
define ('P_ORDER_ASC', 8);				// tri ascendant
define ('P_ORD_NRINV', 9);				// ordre de tri : numéro d'inventaire
define ('P_ORD_DATE', 10);				// ordre de tri : date de modification
define ('P_ORD_ETAT', 11);				// ordre de tri : état fiche
define ('P_ORD_COTE', 12);				// ordre de tri : cote de rangement

define ('P_SELECT', 13);					// modification en mode récolement/sélection
define ('P_RECOL', 14);						// affichage et modification en mode récolement
																// ... en réserve 15
define ('P_AFF_COMPLET', 16);			// affichage résumé ou affichage complet
define ('P_AFF_LIENS_NOR', 17);		// affichage des liens non orientés
																// ... en réserve 18, 19
define ('P_PERSO_INV', 20);				// liste personnalisée responsable inventaire
define ('P_PERSO_COLL', 21);			// liste personnalisée correspondants collection
define ('P_PERSO_5', 22);					// liste personnalisée, affichage état 5, etc.
define ('P_PERSO_6', 23);
define ('P_PERSO_7', 24);
define ('P_PERSO_8', 25);
define ('P_PERSO_9', 26);
define ('P_PERSO_10', 27);
																// ... en réserve 28, 29, 30
define ('P_AFF_GALEVIRT', 31);
define ('P_AFF_AUTRNO', 32);			// colonnes à afficher
define ('P_AFF_DESIGN', 33);			// colonnes à afficher ...
define ('P_AFF_MEDIAS', 34);
define ('P_AFF_VIGNETTE', 35);
define ('P_AFF_DIMENSIONS', 36);
define ('P_AFF_FONCTION', 37);
define ('P_AFF_ETAT', 38);
define ('P_AFF_DONATEUR', 39);
define ('P_AFF_BATIMENT', 40);
define ('P_AFF_COTERANGE', 41);
define ('P_AFF_LIEURANGE', 42);
define ('P_AFF_DATE_VALID', 43);
define ('P_AFF_DATEMOD', 44);
define ('P_AFF_CORCOL', 45);
define ('P_AFF_RESPINV', 46);
define ('P_AFF_ETATFICH', 47);
														
define ('PROFIL_DEF_H', 'f8c07de03000'); 		// Profil par défaut
define ('PROFIL_DEF_B', '111110001100000001111101111000000011000000000000'); 		// Profil binaire pour les visiteurs

#### TABLES D'ORDRES ###########################################################
#### pour contrôles dans modifier_table
#
$T_ordre = array (NULL, 'Acquisitions', 'Batiments', 'Codeprogs', 'Compteurs', 'Designations', 'Domaines',
		'Etablissements', 'Etatfiches', 'Etats', 		'Familles', 'Langprogs', 'Langues', 'Etablissements', 'Materiaux',
		'Medias', 'Motscles', 'Mouvements', 'Organismes', 'Parametres', 'Periodes', 'Personnes', 'Productions',
		'Rapporteurs', 'Relations', 'Series', 'Services',	'Supports', 'Rapporteurs', 'Systemes', 'Tmarques', 'Tnpositions',
		'Trecols', 'Typedocs', 'Typemouvts', 'Typeillustrs', 'Validite', 'Series', 'Verrous', 'SQLrequetes');

$T_tablesliees = array (
	'idacquisition'	=> array ("Collections"),
	'iddomaine'		=> array ("Collections"),
  'idcodeprog'		=> array ("Logiciels"),
	'idcompteur'		=> array ("Compteurs"),				// Ce bouclage interdit toute suppression 
	'iddesignation' => array ("Machines"),
	'idetat'				=> array ("Collections"),
	'idetatfiche'		=> array ("Collections"),
	'idfamille'			=> array ("Liens"),
  'idlangprog'		=> array ("Logiciels"),
	'idlangue'			=> array ("Documents","Logiciels"),
	'idetablissement'=> array ("Collections"),
	'idmateriau'		=> array ("Col_Mate"),
	'idmedia' 			=> array ("Col_Med"),
	'idmotcle' 			=> array ("Col_Mot"),
	'idmouvement'		=> array ("Col_Mouv"),
	'idorganisme'		=> array ("Col_Org"),
  'idparametre'		=> array (),										// Effacement libre
	'idperiode'			=> array ("Collections"),
	'idpersonne'		=> array ("Col_Per"),
	'idproduction'	=> array ("Collections"),
	'idrapporteur'	=> array ("SQLRequetes"),
	'idrelation'		=> array ("Col_Org", "Col_Per"),
	'idservice'			=> array ("Machines"),
  'idsupport'			=> array ("Logiciels"),
  'idsqlrequete'	=> array (),										// Effacement libre
	'idsysteme'			=> array ("Logiciels"),
	'idtmarque'			=> array (),										// Effacement libre	
	'idtnposition'	=> array ("Collections"),
	'idtrecol'			=> array ("Collections"),
	'idtypedoc'			=> array ("Documents"),
	'idtypeillustr'	=> array ("Documents"),
	'idtypemouvts'	=> array ("Mouvements")
);

### VARIABLES INITIALES  #######################################################
# Elles sont modifiées après le lancement
$DebutFait = 0;	

### VARIABLES CONCERNANT LE SERVEUR LOCAL ######################################
### elles sont chargées après les variables globales et peuvent les modifier ###
################################################################################

require_once "../dbconfig.php";		// Fichier de configuration. Local à une installation donnée
# dbconfig peut commander un arrêt pour maintenance :
if (@$maintenance) exit ("<h2 style='color:red'>DBAconit<br/>Interruption de service pour maintenance<br/>
													quelques minutes...</h2>");
?>