<?php
# V5.3			050623  HBP		Mise en place de la gestion des transferts XML
# ...
# v13.0			150707	PhD 	Introduction Mysqli
# v13.0.01	150717	PhD		Corrigé message erreur connexion
# V14.2			151214	PhD		Commentaires
# v15				160220	PhD		Basculement HTML5
###
	
####################################################################################
### ATTENTION ### Ce module peut être appelé directement par "dbexport/index.php".
### Dans ce cas, il s'exécute de façon autonome, hors gestion des sessions dbaconit
### et seule la commande de téléchargement est active.
####################################################################################


################################### Initialisations session et en tête page affichée
	require_once('../dbaconit/globalvars.php');
	require_once('../dbaconit/xml_inc.php');
	require_once('../dbaconit/xml_inc_html5.php');
	require_once('../dbaconit/gestexport.inc.php');
	session_start ();
   
	debug (0x20,'$_REQUEST', $_REQUEST); 
	debug (0x10,'$_SESSION', $_SESSION);

	# Noter les droits d'accès - si l'utilisateur s'est inscrit            
	if (@$_SESSION['statut'])  $droits = $tab_droits[$_SESSION['statut']];
	else $droits[] = "";  
	debug (1, "Droits", $droits);

	$refus = array();
	$valide = array();
	$erreurs = array ();

# Traitement des appels par les formulaires #########################################
#1# Appel par le formulaire de validation créé par le module XML de codage#########1#

  if (isset($_REQUEST['envoi'])) {
		debug (1,'cas "envoi" - traiter résultat de validation');
		$db = $_REQUEST['db'];
    $date_tr = $_REQUEST['datetransfert'];
		$fiche = $_REQUEST['fiche'];
		$message = $_REQUEST['textfield'];

# Contrôle de l'existence du fichier de transfert
    if (!file_exists ($dir_export.$fiche.'.htm')) {
			 echo "<h2 align='center'>Le fichier $dir_export$fiche demandé n'existe plus</h2>";
			 exit();
		}

# Connexion à la base
	  $dbase = $Bases[$db];
		if ($dblink = mysqli_connect ($dbase['serveur'], $dbase['user'], $dbase['passe'], $dbase['nom'])) {
				debug (1, "mySQL session ouverte - Base de données " . $dbase['nom'] . " ouverte");
		} else die ('Erreur de connexion avec la base de données - '. mysqli_connect_error ());
      

# Traiter la réponse, séparer les cas valides et les cas refusés
    foreach ($_REQUEST as $n => $v)
			if (($v == 'valide') || ($v == 'refus')) {
				$n = explode (' : ', str_replace('_', ' ', $n));
		 		if (ctype_digit ($n[0])) {
		 		
			 		$req = "Select idetatfiche from Collections where idcollection='$n[0]' and idetatfiche=4;";
          if (!$res = mysqli_query ($dblink, $req))
             $erreurs[] = array ($n[0], $n[1], $n[2], 'problème requête : ' . mysqli_error ($dblink));
   	    	else if (1 == $num = mysqli_num_rows($res)) ${$v}[] = $n;
   	    	
				else
					$erreurs[] = array ($n[0], $n[1], $n[2], 'n\'est pas en transfert');
			} else	$erreurs[] = array ($n[0], $n[1], $n[2], 'idcollection non numerique');
	  }
	  
	  debug (1, '$valide',$valide); 
	  debug (1, '$refus',$refus); 
	  debug (1, '$erreurs',$erreurs);
  
# Début de composition de la page de confirmation ++++++++++++++++++++++++++

		$t = explode ('-', $date_tr);
		$Xvars['date_tr'] = $t[2].'/'.$t[1].'/'.$t[0];	// Mettre la date en français
		$Xvars['db'] = $db;
		$Xvars['droits'] = $droits;
		$Xvars['erreurs'] = $erreurs;
		$Xvars['fiche'] = $fiche;
		$Xvars['message'] = $message;
		$Xvars['refus'] = $refus;
		$Xvars['valide'] = $valide;
	
    ob_start ();
		$liste_xml = Xopen ('../dbaconit/XML_modeles/gestexport_resume.xml') ;
		Xpose	 ($liste_xml);   
    $reponse = ob_get_clean();	
    
# Fin de composition de la page de confirmation +++++++++++++++++++++++++++

# Écriture du fichier résumé de transfert "R_"
    $f = fopen ($dir_export.'R_'.$fiche.'.htm', "w");
    fwrite($f, $reponse);
    fclose($f);
    unlink ("$dir_export$fiche.htm");
     
# On enchaîne sur l'affichage principal
# mais il serait préférable d'ouvrir directement le fichier résumé qui vient d''être créé ???
  }

#####################################################################################
#2# Appel par le traitement principal - ci dessous ################################2#
###   Cas de la demande de transfert fichier ZIP ------------------------------------
	if (isset($_REQUEST['zip'])) {	
		debug (1, "cas demande transfert ZIP");
		$path = dirname (realpath ($_REQUEST['zip']));
		$zip = basename ($_REQUEST['zip']);
		$file = $dir_export.$zip;
		
		if (file_exists ($file)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($file));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			
			// Le fichier ZIP perd son "T_" une fois transféré
			$new_zip = substr ($zip, 2);
			rename ($dir_export.$zip, $dir_export.$new_zip);
			
			exit; // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ON NE PEUT PLUS AFFICHER SUR L'ÉCRAN
		} 
  }

###   Cas de la demande de validation ------------------------------------------------
# Le bouton "valider" charge la page htm de validation du transfert ;
# le bouton envoyer de cette page est traité ci-dessus

### TRAITEMENT PRINCIPAL ########################################################
#################################################################################
# En-tête minimum
	echo "<!DOCTYPE html >";
	echo "<html>";


  debug (1, "Traitement principal");
   
# Ouvrir le dossier dbexport et scanner le contenu
# L'identification des fichiers ZIP et des fiches de transfert htm va être faite 
# par les fonctions XML_zip, XML_transf et XML_result 
   if (is_dir ($dir_export)) {  
   	$tab_files = scandir ($dir_export);
   } else  die ("Erreur de lecture dossier dbexport");
 

####################### Affichage

	$Xvars['dir_export'] = $dir_export;				// adresse relative du dossier dbexport
	$Xvars['droits'] = $droits;								// droits de l'utilisateur
	$Xvars['tab_files'] = $tab_files;					// tableau des fichiers en ordre croissant
	$Xvars['tr_ok'] = (isset($tr_ok)) ? $tr_ok : "";
	$Xvars['tr_nok'] = (isset($tr_nok)) ? $tr_nok : "";
	
	$liste_xml = Xopen ('../dbaconit/XML_modeles/gestexport.xml') ;
	Xpose ($liste_xml);

	echo "</html>";

?>