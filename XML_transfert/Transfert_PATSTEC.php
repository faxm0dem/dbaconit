<?php
# v15.3		20160802	PhD		Adaptation au moteur Xpose, Ajout d'une fonction PostEval remplace TraitementTexte
# v16.0		20170626	PhD		Ajout Typb, Ajout d'un traitement <type>, recodage partiel fr XML_FICHIER_INVENTAIRE_MEDIAS
# v25			20190611	PhD		Tables Localisation -> Etablissements, Categories -> Domaines, sup Vrp, 
#														corrigé XML_FICHIER_INVENTAIRE_MATERIAUX, supprimé ElementFin
# v25.3		20191029	PhD		Requête XML_FICHIER_INVENTAIRE : supprimé appel intempestif à etatfiche. Supprimé Trim_nr
# v25.4		20191206	PhD		Ajout fonction Etat_general,
# v25.6		20200121	PhD		Correction fonction Etat_general, testé str dans posteval
###


########################################################################################## Description ###           
function Description($td_fr, $tl_fr, $d) { 
	
	if ($tl_fr != "")
		return "Titre traduit : ".$tl_fr."\n".$d ;
	elseif ($td_fr != "") 
		return "Titre traduit : ".$td_fr."\n".$d ;
	else return $d;
}

########################################################################################## Description ###           
function Etat_general ($etat) { 
	
	$Tab = array (
	 '(non renseigné)' => '',
   'mauvais, incomplet' => 'Mauvais',
   'mauvais, complet' => 'Mauvais',
   'moyen, incomplet' => 'Médiocre',
   'moyen, complet' => 'Médiocre',  
   'bon, incomplet' => 'Bon',  
   'bon, complet' => 'Bon',    
   'mauvais, à restaurer' =>'Mauvais',  
   'bon, restauré' => 'Très bon',   
   'état neuf, complet' => 'Très bon' 
	);
	return $Tab[$etat];
}

########################################################################################## Fonctionnel ###           
function Fonctionnel($service) { 

	if (($service == 'service partiel') || $service == 'opérationnel') return "1";
	return "0";
}

################################################################################################ Fpstc ###           
function Fpstc($index) { 
# Formate l'index fourni sur 9 chiffres et ajoute un préfixe propre à la base nationale
	global $db;
	
	$x = array ('005-', '018-');		// base ACONIT, base Grenoble

	$str = $x[$db].sprintf("%09d",$index);
	return $str;
}

############################################################################################### Marque ###  
function Marque ($idcollection) {
# Retourne un nom de marque s'il existe dans les relations organismes

	global $tl_orga;

	$tl_orga = Compose_tl_orga ($idcollection);
	return AffNoms ('o', 'Marque');
}

############################################################################################= MotsCles ###           
function MotsCles ($idcollection) {

	$listemots = "";
	$result = requete ("SELECT motcle, cleaconit
      FROM Col_Mot, Motscles
      WHERE Col_Mot.idmotcle = Motscles.idmotcle
        AND Col_Mot.idcollection = $idcollection ");
	if (mysqli_num_rows ($result) != 0) {
		while ($lignemot = mysqli_fetch_assoc ($result)) {
			$st = $lignemot['motcle'];
			if ( $lignemot['cleaconit'] == 'non' )	// Ne pas traiter les mots réservés ACONIT
				$listemots = $listemots.ltrim ($lignemot['motcle'], '-')."\n";				
		}
  }           

	return rtrim ($listemots, '\n');
		
}

############################################################################################### Nonnul ###           
function Nonnul($val) { 
	
	return ($val !=0) ? $val : ""; 
}

############################################################################################ Souschain ###
function Souschaine ($s, $n=0) {
// Sépare les différentes parties d'une chaine

	$t = Explode ("|", $s);
	return Trim ($t[$n]);
}

################################################################################################### Tt ###           
function PostEval ($texte) {   
# Fonction appelée systématiquement à la sortie du module Xvalue d'évaluation de chaine 
# Rétablir les New line égarés après SQL, recoderl'apostrophe std et échapper l'éperluette
#------------------------------------------------------------------------------
	if (is_string ($texte)){
    $texte = str_replace (array("\n", "\r", '&#039', '&'), array ('\\n', '', '&apos', '&amp;'), $texte);   
    									// Attention aux doubles guillemets "\n" !!!  
  }
  return $texte;    
}        


################################################################################################# Typb ###           
   
function Typb ($str) {
# Filtrage des caractères typographiques utilisés pour l'affichage DBAconit
# - les appels gras et italique sont supprimés
# - liens HTML : l'adresse URL est mise entre parenthèses
#------------------------------------------------------------------------------
	$pattern = array ('#\{\{#', '#\{#', '#\}\}#', '#\}#',
										 '#\[\[#', '#\=\=#', '#\]\]#'	);
	$replacement = array ('', '', '', '',
												'(', ')', '' );	
	return preg_replace ($pattern, $replacement, $str);
}	


############################################################################### XML_FICHIER_INVENTAIRE ###           
function XML_FICHIER_INVENTAIRE($loop, $attr, $Xaction) {       

	if ($loop === null) return 'TAG';		// tag de fin
	
	global $Xvars;
  global $objetListe, $requete, $db;              
	static $resultat;

	// tag de début, lire la base
	if ($loop === 0) {
    $objetListe = array ();          

        $resultat = requete(    
        "SELECT * FROM Collections    
          left join Machines on Machines.idmachine=Collections.idmachine    
          left join Documents on Documents.iddocument=Collections.iddocument    
          left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel     
          left join Designations on Designations.iddesignation=Machines.iddesignation    
          left join Services on Services.idservice=Machines.idservice       
          left join Langues on Langues.idlangue=Documents.idlangue   
          left join Typedocs on Typedocs.idtypedoc=Documents.idtypedoc 
          left join Codeprogs on Codeprogs.idcodeprog=Logiciels.idcodeprog    
          left join Langprogs on Langprogs.idlangprog=Logiciels.idlangprog    
          left join Supports on Supports.idsupport=Logiciels.idsupport    
          left join Systemes on Systemes.idsysteme=Logiciels.idsysteme     
        	left join Acquisitions on Acquisitions.idacquisition=Collections.idacquisition    
          left join Domaines on Domaines.iddomaine=Collections.iddomaine    
          left join Etats on Etats.idetat=Collections.idetat
          left join Etablissements on Etablissements.idetablissement=Collections.idetablissement   
          left join Periodes on Periodes.idperiode=Collections.idperiode    
          left join Productions on Productions.idproduction=Collections.idproduction    
          WHERE ".$requete );

        if (mysqli_num_rows ($resultat) == 0)          
           return  'TAG';      
    }            

    $dbdata=mysqli_fetch_assoc($resultat);             
    if ($dbdata == NULL) return;   // EXIT    
		// Préparer le tableau des variables
		$Xvars =  $dbdata;
		$Xvars['db'] = $db;
		

		// objetListe est utilisé pour créer les documents d'accompagnement de l'envoi.
    $objetListe[count($objetListe)] = $dbdata['idcollection']. ' : ' .          
        ($dbdata['titredoc'] ?       
        'Doc : '.$dbdata['titredoc'] :       
        ($dbdata['titrelog'] ? 'Logiciel : '. $dbdata['titrelog'] :       
        'Machine : ' . $dbdata['nom']));  
        
  	if ($loop === 0)  return 'TAG,ACT,LOOP';			// Le tag de début est émis une seule fois à l'ouverture
		else return 'ACT,LOOP';    
}   
     
######################################################################### XML_FICHIER_INVENTAIRE_MATERIAUX ###           
function XML_FICHIER_INVENTAIRE_MATERIAUX ($loop, $attr, $Xaction) {

	if ($loop === null) return 'TAG';		// tag de fin
	
	global $Xvars;
  global $requete;
  static $SQL_result_materiau;
    

	// tag de début, lire la base
	if ($loop === 0) {
		$SQL_result_materiau = requete (
		"SELECT Collections.idcollection, Materiaux.idmateriau, materiau, materiauPSTC
		FROM Collections, Col_Mate, Materiaux
		WHERE Col_Mate.idmateriau = Materiaux.idmateriau
		AND Col_Mate.idcollection = Collections.idcollection 
	    AND ".$requete );
			
		if (mysqli_num_rows ($SQL_result_materiau) == 0) 
			return  'TAG';		// >>>>>>>>>>>>>>>
	}

   // Tant qu'il y a des résultats
	while ($lignemate =  mysqli_fetch_assoc ($SQL_result_materiau)) {
		if ( $lignemate['materiauPSTC'] != '' )	
			if ($lignemate['materiauPSTC'] != '-' )
				$lignemate['materiau'] = $lignemate['materiauPSTC'];	

		$Xvars = $lignemate;
  	if ($loop === 0)  return 'TAG,ACT,LOOP';			// Le tag de début est émis une seule fois à l'ouverture
		else return 'ACT,LOOP';    
	}
   
    return 'EXIT';       
}

############################################################################ XML_FICHIER_INVENTAIRE_MEDIAS ###           
function XML_FICHIER_INVENTAIRE_MEDIAS ($loop, $attr, $Xaction) {

	if ($loop === null) return 'TAG';		// tag de fin
	
	global $Xvars;
	static $mediaresultat, $tab_format, $tab_type;
	global $db, $fichierListe, $requete;       
	
	// tag de début, lire la base
	if ($loop === 0) {
	
		// Deux tables de codage, déclarées "static"
		$tab_format = array ('jpg'=>'JPEG', 'png'=>'PNG', 'pdf'=>'PDF', 'mov'=>'MOV', 'mp3'=>'MP3', 'mp4'=>'MP4');
		$tab_type = array ('jpg'=>'Image', 'png'=>'Image', 'pdf'=>'Document', 'mov'=>'Vidéo', 
												'mp3'=>'Vidéo', 'mp4'=>'Vidéo', 'mp5'=>'Vidéo');

		// Lire la base et appeler tous les liens médias et toutes les infos médias...
   	$mediaresultat = requete (
			'SELECT Collections.idcollection, Medias.idmedia, mediacle, pubreserv, formatmedia, descrimedia 
			FROM Collections, Col_Med, Medias 
			WHERE Col_Med.idcollection=Collections.idcollection 
			AND Col_Med.idmedia=Medias.idmedia
			AND '.$requete );
				
		if (0 == mysqli_num_rows ($mediaresultat)) return 'TAG';   // >>>>>>>>>>>>>
		if (!isset ($fichierListe)) $fichierListe = array();       
	}       

	while (null != ($mediadata = mysqli_fetch_assoc ($mediaresultat))) {  
		$Xvars = $mediadata;
		$idmedia = $mediadata['idmedia'];

		$Xvars['extmedia'] = $extmedia = trim ($mediadata['formatmedia']);
		$Xvars['format_media'] = $tab_format[$extmedia];

		$Xvars['type'] = $tab_type[$extmedia];

		// Ajouter les médias, s'ils existent bien, et si la publication est autorisée...
		// S'il ne reste aucun média disponible, on crée un fichier xml incorrect ! (pas de balise ouvrante)
		if ($mediadata['pubreserv']=='non') {		// Si publication NON réservée
			if (file_exists (AdMedia($mediadata['idmedia'], $db, $extmedia))) {        
				$fichierListe[$idmedia.'.'.$extmedia] = AdMedia($mediadata['idmedia'], $db, $extmedia);  
										// ???? d'où sort idmedia ???
				if ($loop === 0)  return 'TAG,ACT,LOOP';			// Le tag de début est émis une seule fois à l'ouverture
				else return 'ACT,LOOP';    
			} 
		}
	}

	return 'EXIT';       
} 
       
################################################################################################ XML_liens ###  
function XML_liens ($loop, $attr, $Xaction) {
# Pour tous les objets-fils faisant partie de cet export, la fonction relève les liens père-fils. 
# Les liens "famille" et "même lot" ne sont plus exclus 
# S'il reste des pères multiples... ils sont transmis...

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
  static $SQL_result_liens;  
	$idcollection = $attr['idcollection'];  
 
	// tag de début, lire la base
	if ($loop === 0) {
        $SQL_result_liens = requete ("SELECT idcol1
              FROM Liens
              WHERE idcol2 = $idcollection");
           
		if (mysqli_num_rows ($SQL_result_liens)==0) {
			$Xvars['idcol_pere'] ="";
			return 'ACT,LOOP';		// >>>>>>>>>>>>>>>
		}
	}
   
   // Tant qu'il y a des résultats
    while ($ligneliens = mysqli_fetch_assoc ($SQL_result_liens)) {
		$Xvars['idcol_pere'] = $ligneliens['idcol1'];
    	return 'ACT,LOOP';  			// >>>>>>>>>>>>>>>
    }
    
    return 'EXIT';
}

######################################################################################### XML_organ ###           
function XML_organ ($loop, $attr, $Xaction) {       

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	static $SQLresult_orga;
  global $requete;       

	// tag de début, lire la base
	if ($loop === 0) {
   	$SQLresult_orga = requete (
			"SELECT Collections.idcollection, Organismes.idorganisme, onom, relation 
			FROM Collections, Organismes, Col_Org, Relations
			WHERE Organismes.idorganisme=Col_Org.idorganisme 
				AND Relations.idrelation=Col_Org.idrelation
				AND Col_Org.idcollection= Collections.idcollection
				AND ".$requete );
	
	   if (0 == (mysqli_num_rows ($SQLresult_orga)))
  	    	return 'EXIT';		// >>>>>>>>>>>>>>>
	}
   // Tant qu'il y a des résultats
	while ($Xvars =  mysqli_fetch_assoc ($SQLresult_orga)) {
		return 'ACT,LOOP';
	}
   
    return 'EXIT';       
} 
       

########################################################################################## XML_person ###           
function XML_person ($loop, $attr, $Xaction) {       

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	static $SQLresult_perso;
  global $requete;       

	// tag de début, lire la base
	if ($loop === 0) { 	
   	$SQLresult_perso = requete (
   		"SELECT Collections.idcollection, Personnes.idpersonne, pcivil, pnom, pprenom, relation 
   		FROM Collections, Personnes, Col_Per, Relations
			WHERE Personnes.idpersonne=Col_Per.idpersonne 
				AND Relations.idrelation=Col_Per.idrelation
				AND Col_Per.idcollection= Collections.idcollection
	    	AND ".$requete );
	
	   if (0 == (mysqli_num_rows ($SQLresult_perso)))
  	    	return 'EXIT';		// >>>>>>>>>>>>>>>
	}
   // Tant qu'il y a des résultats
	while ($Xvars =  mysqli_fetch_assoc ($SQLresult_perso)) {
		return 'ACT,LOOP';
	}
   
    return 'EXIT';       
} 
       

?>