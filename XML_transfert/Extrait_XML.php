<?php
# v25				190713	PhD		Création
# v25.6			200125	PhD		Supprimé appels à Typc (intégré dans PostEval), ajouté tri par idcollection asc
# v25.8			200410	PhD		Ajouté space devant ORDER
###

require_once ('consulter.inc.php');

################################################################################################### PostEval ###           
function PostEval ($texte)   
# Fonction appelée systématiquement à la sortie du module Xvalue d'évaluation de chaine 
# On ne touche plus les NL, on supprime les LF éventuels, on recode l'apostrophe std et on échappe l'éperluette
#------------------------------------------------------------------------------
{ 
	if (is_string ($texte)){
		$pattern = array ('#\r#', '#&nbsp;#', '%&#039;%', '#&quot;#', '#&gt;#', '#&#');
		$replacement = array ('', ' ', "'", '"', '>', '&amp;' );	
		$texte = preg_replace ($pattern, $replacement, $texte);
	}  
  return $texte;    
}        

############################################################################### XML_boucle_objets ###           
function XML_boucle_objets ($loop, $attr, $Xaction) {       

	if ($loop === null) return;		// SORTIE tag de fin, sans affichage du tag
	
	global $Xvars;
  global $objetListe, $requete, $db, $tl_orga, $tl_perso;              
	static $resultat;

	// tag de début, lire la base
	if ($loop === 0) {
    $objetListe = array ();          

        $resultat = requete(    
        'SELECT * FROM (Collections, Acquisitions, Domaines, Etats, Etablissements, Periodes, Productions)    
          left join Machines on Machines.idmachine=Collections.idmachine    
          left join Documents on Documents.iddocument=Collections.iddocument    
          left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel     
          left join Designations on Designations.iddesignation=Machines.iddesignation    
          left join Etatfiches on Etatfiches.idetatfiche=Collections.idetatfiche          
					left join Services on Services.idservice=Machines.idservice       
          left join Langues on Langues.idlangue=Documents.idlangue   
          left join Typedocs on Typedocs.idtypedoc=Documents.idtypedoc 
          left join Typeillustrs on Typeillustrs.idtypeillustr=Documents.idtypeillustr 
          left join Codeprogs on Codeprogs.idcodeprog=Logiciels.idcodeprog    
          left join Langprogs on Langprogs.idlangprog=Logiciels.idlangprog    
          left join Supports on Supports.idsupport=Logiciels.idsupport    
          left join Systemes on Systemes.idsysteme=Logiciels.idsysteme     
        WHERE Acquisitions.idacquisition=Collections.idacquisition    
          and Domaines.iddomaine=Collections.iddomaine    
          and Etats.idetat=Collections.idetat
          and Etablissements.idetablissement=Collections.idetablissement   
          and Periodes.idperiode=Collections.idperiode    
          and Productions.idproduction=Collections.idproduction    
          and '.$requete.' ORDER BY idcollection');          

        if (mysqli_num_rows ($resultat) == 0)          
           return;      		// SORTIE sur sélection d'objets vide (TAG, ACT par défaut)
    }            

    $dbdata=mysqli_fetch_assoc($resultat);             
    if ($dbdata == NULL) return 'EXIT';   // EXIT  fin de boucle  
    
		// Préparer les tableaux personnes et organismes
		$tl_orga = Compose_tl_orga ($dbdata['idcollection']);
		$tl_perso = Compose_tl_perso ($dbdata['idcollection']);
		
		// Préparer le tableau des variables
		$Xvars =  $dbdata;
		$Xvars['db'] = $db;
		

		// objetListe est utilisé pour créer les documents d'accompagnement de l'envoi.
    $objetListe[count($objetListe)] = $dbdata['idcollection']. ' : ' .          
        ($dbdata['titredoc'] ?       
        'Doc : '.$dbdata['titredoc'] :       
        ($dbdata['titrelog'] ? 'Logiciel : '. $dbdata['titrelog'] :       
        'Machine : ' . $dbdata['nom']));  
        
  	return 'ACT,LOOP';    // SORTIE avec bouclage
}   
     
?>
