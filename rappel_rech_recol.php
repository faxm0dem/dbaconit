<?php 
# v25				190708	PhD		Préparé comptage
# v25.1			190717	PhD		Supprimé f_aff_galevirt
# v25.4			191205	PhD		Ajout des commandes spécifiques à rec_recol_desh, Commentaires, 
# v25.5			191224	PhD		Plus de traitement de pondération provisoire session, revu test récolement
# v25.5.01	200114	PhD		Les fonctions Act_id et Crit_id sont déplacées dans aff_recol_inc
# v25.8			200411	PhD		Ajout custom_css
###
# Ce module est appelé en retour par les écrans xml 'rech_resultat_recol', 'rech_resultat_desh' et 'rech_recol_sel'
# Il traite les modifications demandées par le formulaire et réaffiche le même écran
#########
/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
'act_id'				- POST - filtré NormIN
'crit_id'				- POST - filtré NormIN
'idcollection'	- POST - testé numérique
'idtrecol'			- POST - filtré NormIN
'ligne'					- POST - uniquement testé switch
L'ensemble des données du formulaire est normalisé par NormINPUT dans le cas Sauverecol
------------------------------------------------------------------------------ */

##############################################################################################

$custom_css = "rappel_rech_recol.css";
require_once ('global.js.php');  // avant init
require_once ('init.inc.php');
require_once ('rechercher.inc.php');
require_once ('consulter.inc.php');
if (@$dbase['mode_recolement']) require_once ('aff_recol.inc.php');

## Traitement des entrées :
###########################
	$action = @$_POST['action'];
	$act_id = @$_POST['act_id'];
	$crit_id = @$_POST['crit_id'];

	if ($action == 'Sauverecol')  NormINPUT ();				// Normalisation en bloc formulaire 'mod_recol.xml'
		
  $idcollection = @$_POST['idcollection'];
  if (isset ($idcollection) AND !is_numeric($idcollection)) 	
  	DIE ("*** rappel_rech_recol - Paramètre 'idcollection' faux ! ***"); 
	
	$idtrecol = NormIn ('idtrecol');
	$idtnposition = NormIn ('idtnposition');

	$tm = @$_POST['tm'];
	$pond = @$_POST['pond'];

  if (isset ($_POST['ids'])) $_SESSION['ids'] = $ids = array_values(array_unique ($_POST['ids']));
  elseif (isset ($_SESSION['ids'])) $ids = $_SESSION['ids'];
  else $ids = array ();
   

#################################################################################
### Traitement de l'action demandée
	switch ($action) {
#
	### Sauvegarde des modifications depuis le bloc masqué ############################################
	#=================================================================================== Sauvegarder ===	
		case 'Sauverecol' :
			Sauvobjet ('Recolement', $idcollection);  // traiter aussi les champs coterange,...
			break;

	### Modifications "en masse" dans l'écran 'rec_resultat_sel' #######################################
	#======================================================================================== select ===	
		case 'select' :
			// déjà traité en traitement entrée
			break;

	#====================================================================================== m_trecol ===	
		case 'm_critere' :
			foreach ($ids as $idcollection) {
				Act_id ($idcollection, $idtrecol, $idtnposition);
			}
			break;

	#===================================================================================== m_critere ===	
		case 'm_ponder' :
			foreach ($ids as $idcollection) {
				Crit_id ($idcollection, $tm);
			}
			break;

	}
	
#################################################################################	
#	ACTIONS EN MODE LIGNE : dans ce cas l'idcollection est transmis DANS la valeur du bouton
### cas ligne de l'écran Désherbage	
	if (isset ($crit_id) AND is_numeric ($crit_id)) {
		Crit_id ($crit_id, $tm);
	}
	
### cas ligne de l'écran Sélection
	if (isset ($act_id) AND is_numeric ($act_id)) {
			Act_id ($act_id, $idtrecol, $idtnposition);
	}

#################################################################################
### et réaffichage de la table récolement ou désherbage
	Debut ();
	
	$req_rechercher = $_SESSION['req_rechercher'];
	$SQLresult = requete ($req_rechercher);

	// Charger le résultat de la recherche
	$Xvars['SQLresult'] = $SQLresult;
	$Xvars['db'] =$db;					// Pour pouvoir ajouter le nr de la base dans l'URL

						// Afficher le code de la recherche
						$Xvars['textreq'] = $_SESSION['reaffichage'][0];
						$Xvars['codereq'] = $_SESSION['reaffichage'][1];
						$Xvars['clausereq'] = $_SESSION['reaffichage'][2];
						$Xvars['typs']		= $_SESSION['reaffichage'][3];
			
	// Préparer le comptage machines/documents/logiciels/objets/icongraphie
	$Xvars['compt_c'] = $Xvars['compt_d'] = $Xvars['compt_l']= $Xvars['compt_o']= $Xvars['compt_i'] = 0;

	$Xvars['droits'] = $droits;
	$Xvars['ids'] = $ids;

### (création éventuelle d'une table de structure simplifiée) et AIGUIILLAGE vers le module récolement ou désherbage
	Aiguil_rech_resultat ();
	
	Fin ();
?>
