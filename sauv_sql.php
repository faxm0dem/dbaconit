<?php
# v25				190520	PhD		importexport devient sauv_sql, suprimé slash après dir_save
#	v25.5			200106	PhD		Ajout Xvars droits
# v25.8			200411	PhD		Ajout custom_css
# v25.11.01	210430	PhD		Ajouté extension fichier sql si manquant
###

$f_non_head_html = TRUE; 				// Ceci va bloquer l'ouverture de la page HTML dans init.inc
																// Nécessaire dans le cas export d'un fichier
require_once ('init.inc.php');				// Initialisations, identification..
require_once ('sauv_sql.inc.php');		// Fonctions de création du buffer texte

#################################################################################

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
'origin'				- POST (radio) - test des valeurs connues
'table'					- POST  - Filtré NormIN
'restaur_file'	- POST  - Filtré NormIN
'sql_file'			($_FILES)
------------------------------------------------------------------------------ */

@set_time_limit (10000); // On augmente le timeout

## Traitement des entrées :
###########################

	$action = @$_POST['action'];
	$origin = @$_POST['origin'];

	$table = NormIN ('table');	
	$restaur_file = NormIN ('restaur_file');
	if (strrchr($restaur_file, '.') != '.sql') $restaur_file .= '.sql';

###  Vérifier les droits de maintenance
	if (!in_array ("maintenance", $droits)) {
		erreurMsg ("Vous ne vous êtes pas identifié...");
		include "identification.php";
		exit;
	}
####################################################
#   PREMIERE PARTIE : Restauration = importation   #
####################################################

switch ($action) {
case 'import':
	require_once ('inc_tete.php');  // Ouverture retardée de la page HTML
	Debut ();	

	if ($origin == 'local') $sql_file = $_FILES['sql_file']['tmp_name'];
	else $sql_file = $dir_save.$restaur_file;
debug (1, 'FILE', $sql_file);
	// Vérifier l'existence du fichier, puis le lire en totalité
	if (!$sql_file || ! file_exists ($sql_file) || ($sql_file == $dir_save))
		erreurMsg ("Vous n'avez pas sélectionné de fichier valide");
	else {    
		$file_lines = file ($sql_file);
		$erreur = FALSE;
		$insertion = '';
		$a_sql_query = '';

####### Lecture ligne à ligne
		foreach ($file_lines as $a_line) {
            $a_line = trim ($a_line);
            if (!$a_line || ($a_line[0] == '#') || ($a_line == ';'))	// Sauter les lignes vides
               continue;

            $a_sql_query = trim ($a_sql_query.' '.$a_line);
            
 			// Tester si on commence une instruction INSERT/
			if (substr ($a_sql_query, 0, 11) =="INSERT INTO") {
				// Se rappeler que la commande INSERT ... VALUES n'apparait qu'une fois, 
				// ensuite le fichier comporte une liste de valeurs par ligne. 
				// Il faut ajouter la commande INSERT manquante pour effectuer le chargement
				// ligne par ligne (pas rapide,  mais nécessaire pour les très longues tables...).
				$insertion = $a_sql_query;
				$a_sql_query = '';
				$errvalue = 0;      
			}
			
			// Tester différemment les fins de ligne...
 			if ($insertion) {
 				//mode insertion 
 				$lastchar = substr ($a_sql_query, -1,1);
 				if (($lastchar == ',') OR ($lastchar == ';')) {
 					// remplacer la ',' par un ';'
 					if ($lastchar == ',') $a_sql_query = substr ($a_sql_query, 0,-1).';' ;
					// ajouter la commande INSERT...VALUES
					$a_sql_query = trim ($insertion . ' ' . $a_sql_query);                  
 					$errvalue = 0;
					// -> Envoi de la commande SQL
					if (false === requete ($a_sql_query)) {
						$erreur = TRUE;
						ErreurMsg ('Écriture SQL refusée (1) - abandon');
						break;			// Erreur -> abandon
					}
					$a_sql_query = '';	//OK, effacer la commande...

 					// Tester si c'est la dernière liste de valeurs					
					if ($lastchar == ';') $insertion ='';
 				} else continue;
 			} else {
 				if  (substr ($a_sql_query, -1,1) == ';') {
 					// fin d'instruction
 				} else continue;			// ...charger la suite (jusqu'au ";")
 			}
 				

			// Si on a composé une instruction valide
			if (strlen ($a_sql_query) > 1) {
				$errvalue = 0;
				// Envoi de la commande SQL
				if (false === requete ($a_sql_query)) {
					$erreur = TRUE;
					ErreurMsg ('Écriture SQL refusée (2) - abandon');
					break;			// Erreur -> abandon
				}
				$a_sql_query = '';	//OK, effacer la commande...
			}
		}							// et continuer foreach ligne suivante

			if (!$erreur)
			Message ("La restauration a réussi");
	}
	break;
   	  

##################################################
#   DEUXIEME PARTIE : Sauvegarde = exportation   #
##################################################
case 'export' :
	
	// Definit le nom du fichier et son extension
	$filename = $table ? $table : $dbase['nompublic'];
	$filename = date('Ymd_B_').$filename;

	$ext = 'sql';

	// définit les entêtes pour un fichier
	header ('Content-Type: application/octetstream');
	header ('Content-Disposition: filename="' . "$filename.$ext" . '"');
	header ('Pragma: no-cache');
	header ('Expires: 0');
	
	/** Crée l'export **/
	$num_tables = buffer_export ($table);
	
	if ($num_tables == 0) erreurMsg ("Aucune table n'a été trouvée...");
	else echo $dump_buffer;
	
	/** fin de transmission **/		// NÉCESSAIRE POUR STOPPER L'ÉCRITURE DANS FICHIER
	exit;											// ABANDON DE TOUTE SUITE ICI !!!!
	if (!$erreur) Message ("Sauvegarde terminée");	// MESSAGE NON AFFICHÉ
}    
    
#############################
# AFFICHAGE ÉCRAN PRINCIPAL #  
#############################

	$custom_css = "sauv_sql.css";
	require_once ('inc_tete.php');  // Ouverture retardée de la page HTML
  Debut ();

	$Xvars['dir_save'] = $dir_save;
	$Xvars['droits'] = $droits;
	
	$liste_xml = Xopen ('./XML_modeles/sauv_sql.xml') ;
	Xpose ($liste_xml);

	Fin ();
?>

