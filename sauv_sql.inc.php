<?php
# v10.5			121229	PhD		Révision générale, suppressiontraitement dbvide
# v12.2			140308	PhD		Correction du format VALUES (), ()... 
# v12.8			141220	PhD		Sortir les tables triées par id
# v12.8.04	150406	PhD		Corrigé instruction lecture contenu
# v12.9			150408	PhD		Appel fonct "requete"
# v13.0			150708	PhD 	Introduction Mysqli
# v13.0.01	150714	PhD		Corrigé oubli mysqli_real_escape_string
# v14.2			151214	PhD		Commentaires
# v25.5			191217	PhD		Ajouté une ligne en tête pour sécuriser le code UTF_8
# v25.5.01	200113	PhD		Supprimé la ligne en tête pour sécuriser le code UTF_8
# v25.14		220716	PhD		Remplacé strftime par date (PHP 8.1)
###


#========================================================== buffer_export ===

function buffer_export ($table) {
/* Crée le buffert export contenant l'ensemble du texte SQL
*/
	global $timestamp_fmt, $dbase, $DBAconit;
	global $dump_buffer, $nl;

	// Récupérer le nombre de tables de la base si aucune table n'a été choisie
	if (!$table) {
		$sql = "SHOW TABLES FROM ".$dbase['nom'];
		$result_tables = Requete ($sql);
		$num_tables = @mysqli_num_rows ($result_tables);
		$single = FALSE;
	} else {
		$num_tables = 1;
		$single = TRUE;
	}


	$nl        = "\n";
//	$dump_buffer = "SET NAMES 'utf8' COLLATE 'utf8_general_ci';".$nl;
	$dump_buffer = "";
	
#### On ajoute des commentaires au début
	$timestamp = date ($timestamp_fmt);
	$t = explode ('$', mysqli_get_client_info());
	$mysql_info = $t[0];
	
	$dump_buffer .= '# Export de la base de donnée "' . $dbase['nom'] 
	  .'" de la machine ' . $_SERVER["SERVER_ADDR"] . $nl
	  .'# MySQL ' . $mysql_info . $nl
	  .'# ' . $DBAconit . $nl
	  .'# Date : ' . $timestamp . $nl;
	
	for ($i = 0; $i < $num_tables; $i++) {
		if (!$single) {
			$ttable = mysqli_fetch_row($result_tables);
			$table = $ttable[0];
		} 

#### Structure
		$dump_buffer.= $nl . $nl.
			substr ('#==============================================================================', 
			0, 75-strlen ($table)) . " $table ===$nl"
			. structure_table ($table) . ";$nl";
		
#### Données
		
		$dump_buffer .= $nl ;
		$dump_buffer .= contenu_table ($table);
		
	}
	
	$dump_buffer .= $nl;	
	return $num_tables;
}

#============================================================ contenu_table ===

function contenu_table ($table)  {
# Récupère les contenus d'une table

   global $dblink, $nl;   
   $tmp_buffer = '';
   
   // Lire la table
   $id = Nomid ($table);
   $result = Requete ('SELECT * FROM '.$table.' ORDER BY '.$id);
   
   // sortir si table vide
   if (($result != FALSE) AND (mysqli_num_rows ($result) != 0)) {
 
      // Vérifie si le champ est un entier ou pas
      $field_count = mysqli_num_fields ($result);
      for ($j = 0; $j < $field_count; $j++) {
         $info = mysqli_fetch_field_direct ($result, $j);
         $field_set[$j] = $info->name;
         $type          = $info->type;
         $field_num[$j] = ($type == 'tinyint' || $type == 'smallint' || 
        $type == 'mediumint' || $type == 'int' || $type == 'bigint');
       }

       // Définit le schéma
       $fields        = implode (', ', $field_set);
       $tmp_buffer .=  'INSERT INTO ' . $table . ' (' . $fields . ') VALUES' . $nl;
       $schema_insert = '  (';

       $search  = array ("\x0a","\x0d","\x1a");
       $replace = array ("\\n","\\r","\Z");

       @set_time_limit (300); // 5 Minutes

       while ($row = mysqli_fetch_row ($result)) {
          for ($j = 0; $j < $field_count; $j++)
             $values[] = ($row[$j] == '') ? 'NULL' : ($field_num[$j] ? $row[$j] :
        ("'" . str_replace ($search, $replace, mysqli_real_escape_string ($dblink, $row[$j])) . "'"));

          $insert_line = $schema_insert . implode (',', $values) . ')';
          unset ($values);

          // Concaténation
          $tmp_buffer .=  $insert_line . ',' . $nl;
       }
       
   	   // Remplacer le dernier ',nl' par ';nl'
	   $tmp_buffer = substr ($tmp_buffer, 0, -2).';'.$nl ;
   }

   return $tmp_buffer;
}

#========================================================== structure_table ===

function structure_table ($table) {
 // Récupère les définitions d'une table 
	global $nl;        
	$tmp_buffer = 'DROP TABLE IF EXISTS ' . $table . ';' . $nl;
   
	$result = Requete ('SHOW CREATE TABLE ' . $table);
	if ($result != FALSE && mysqli_num_rows ($result) > 0) {
		$result_show = mysqli_fetch_row ($result);
		$tmp_buffer .= $result_show[1];
		
	} else 
		DIE ("Erreur en demande de structure : " .$table);	//>>>>>> ABANDON SI ERREUR
	return $tmp_buffer;
}

