<?php
# v25.4			191203	PhD		Création à partir de list_npositions
# v25.5			200107	PhD		Ajout totaux par colonne et lien sur chaque nombre
# v25.6			200115	PhD		Ajout totaux non verrouillés
# v25.8			200411	PhD		Ajout custom_css
###


############################################################ XML_list_recol ###
function XML_list_recol ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_trecol;

	// Si tag de début, appeler la liste des positions
	if ($loop === 0) {		
		$SQLresult_trecol = requete (
	 		"SELECT * FROM Trecols ORDER BY idtrecol");
	 		
		// Préparer les totalisateurs
		$Xvars['tot_fiches'] = $Xvars['tot_machines'] = $Xvars['tot_documents'] = $Xvars['tot_logiciels'] = 0;
		$Xvars['tot_nonver'] = $Xvars['tot_ver'] = 0;
 	}
			
	//  Pour chaque critère dominant chercher le nombre d'objets
	while ($ligne = mysqli_fetch_assoc ($SQLresult_trecol)) { 
		$Xvars['ligne'] = $ligne;
		$idtrecol = $ligne['idtrecol'];
				
		// Chercher le nombre global de fiches concernées	
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtrecol = $idtrecol");
		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
		if ($idtrecol != 1) $Xvars['tot_fiches'] += $Xvars['nbr_fiches'];
				// Préparer les paramètres pour l'URL de recherche
				$Xvars['quest'] = Phd_encode("Collections.idtrecol=$idtrecol", session_id ());

		// Chercher le nombre de machines	
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtrecol = $idtrecol
									and (Collections.idmachine!=0)");
		$Xvars['nbr_machines'] = mysqli_num_rows($SQLresult2);
		if ($idtrecol != 1) $Xvars['tot_machines'] += $Xvars['nbr_machines'];
				// URL de recherche
				$Xvars['quest_m'] = Phd_encode("Collections.idtrecol=$idtrecol AND (Collections.idmachine!=0)", session_id ());

		// Chercher le nombre de documents
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtrecol = $idtrecol
									and (Collections.iddocument!=0)");
		$Xvars['nbr_documents'] = mysqli_num_rows($SQLresult2);
		if ($idtrecol != 1) $Xvars['tot_documents'] += $Xvars['nbr_documents'];
				// URL de recherche
				$Xvars['quest_d'] = Phd_encode("Collections.idtrecol=$idtrecol AND (Collections.iddocument!=0)", session_id ());

		// Chercher le nombre de logiciels
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtrecol = $idtrecol
									and (Collections.idlogiciel!=0)");
		$Xvars['nbr_logiciels'] = mysqli_num_rows($SQLresult2);
		if ($idtrecol != 1) $Xvars['tot_logiciels'] += $Xvars['nbr_logiciels'];
				// URL de recherche
				$Xvars['quest_l'] = Phd_encode("Collections.idtrecol=$idtrecol AND (Collections.idlogiciel!=0)", session_id ());

		// Chercher le nombre de fiches non verrouillées
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtrecol = $idtrecol AND m10=0");
		$Xvars['nbr_nonver'] = mysqli_num_rows($SQLresult2);
		if ($idtrecol != 1) $Xvars['tot_nonver'] += $Xvars['nbr_nonver'];
				// URL de recherche
				$Xvars['quest_nv'] = Phd_encode("Collections.idtrecol=$idtrecol AND m10=0", session_id ());

		// Chercher le nombre de fiches verrouillées
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtrecol = $idtrecol AND m10!=0");
		$Xvars['nbr_ver'] = mysqli_num_rows($SQLresult2);
		if ($idtrecol != 1) $Xvars['tot_ver'] += $Xvars['nbr_ver'];
				// URL de recherche
				$Xvars['quest_v'] = Phd_encode("Collections.idtrecol=$idtrecol AND m10!=0", session_id ());

		// Alternance des couleurs de ligne
		if ($idtrecol != 1) $Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';		
		else $Xvars['class'] = 'colfond';

		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

########################################################################################################################
########################################################################################################################

$custom_css = "list_recol.css";
require_once ('init.inc.php');

# Initialisations ##############################

Debut ();

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/list_recol.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>